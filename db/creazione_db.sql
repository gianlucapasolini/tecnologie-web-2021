-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Sat Dec 19 11:59:43 2020 
-- * LUN file: D:\Universita\Tecnologie Web\Progetto\DB\ER_DB.lun 
-- * Schema: SCHEMA_LOGICO/v1 
-- ********************************************* 


-- Database Section
-- ________________ 

create database db_CarShop;
use db_CarShop;


-- Tables Section
-- _____________ 

create table AUTO (
     IdAuto SMALLINT AUTO_INCREMENT not null,
     Modello VARCHAR(60) not null,
     Prezzo_base INT not null,
     Link_immagine varchar(300) not null,
     Descrizione VARCHAR(300),
     Suono VARCHAR(200),
     Link_video VARCHAR(200),
     constraint IDAUTO_ID primary key (IdAuto));

create table IMMAGINE (
     Descrizione VARCHAR(600) not null,
     LinkImmagine VARCHAR(300) not null,
     IdImmagine SMALLINT AUTO_INCREMENT not null,
     IdAuto SMALLINT not null,
constraint IDIMMAGINE primary key (IdImmagine));

create table AUTO_CONFIGURATA (
     IdAutoConfigurata SMALLINT AUTO_INCREMENT not null,
     IdAuto SMALLINT not null,
     IdOrdine SMALLINT null,
     IdMotore SMALLINT not null,
     CF VARCHAR(16) null,
     PrezzoTotale INT null,
     constraint IDAUTOCONFIGURATA_ID primary key (IdAutoConfigurata));

create table CLIENTE (
     CF VARCHAR(16) not null,
     Nome VARCHAR(60) not null,
     Cognome VARCHAR(60) not null,
     Email VARCHAR(140) not null,
     Telefono VARCHAR(16) not null,
     Nome_Utente VARCHAR(80) UNIQUE not null,
     Password CHAR(128) not null,
     Salt CHAR(128) not null,
     Via VARCHAR(200),
     N_Civico INT,
     Citta VARCHAR(100),
     Provincia VARCHAR(100),
     CAP INT,
     constraint IDCLIENTE primary key (CF));

create table VENDITORE (
     P_IVA VARCHAR(11) not null,
     Casa_Asta BOOLEAN not null,
     Email VARCHAR(140) not null,
     Telefono VARCHAR(16) not null,
     Nome_Utente VARCHAR(80) UNIQUE not null,
     Password CHAR(128) not null,
     Salt CHAR(128) not null,
     Link_logo varchar(300) not null,
     Link_ImmCopertina varchar(300) not null,
     Via VARCHAR(200),
     N_Civico INT,
     Citta VARCHAR(100),
     Provincia VARCHAR(100),
     CAP INT,
     constraint IDVENDITORE primary key (P_IVA));

create table CORRIERE (
     P_IVA VARCHAR(11) not null,
     Email VARCHAR(140) not null,
     Telefono VARCHAR(16) not null,
     Nome_Utente VARCHAR(80) UNIQUE not null,
     Password CHAR(128) not null,
     Salt CHAR(128) not null,
     constraint IDCORRIERE primary key (P_IVA));

create table COMMENTO (
     IdAuto SMALLINT not null,
     CF VARCHAR(16) not null,
     NumStelline SMALLINT not null,
     Testo VARCHAR(300) not null,
     constraint IDCommento primary key (IdAuto, CF));

create table AUTO_IN_ASTA (
     IdAsta SMALLINT AUTO_INCREMENT not null,
     P_IVA VARCHAR(11) not null,
     Prezzo_iniziale INT not null,
     Anno_Immatricolazione SMALLINT not null,
     Modello VARCHAR(60) not null,
     Link_Immagine1 varchar(300) not null,
     Link_Immagine2 varchar(300) ,
     Link_Immagine3 varchar(300) ,
     Descrizione VARCHAR(500),
     prezzoAttuale INT,
     Suono VARCHAR(200),
     Link_video VARCHAR(200),
     Venduto BOOLEAN DEFAULT 0,
     Vincitore varchar(16),
     constraint IDAUTO_IN_ASTA primary key (IdAsta));

create table METODO_PAGAMENTO (
     IdMetodoPagamento SMALLINT AUTO_INCREMENT not null,
     CF VARCHAR(16) not null,
     Fatturazione BOOLEAN DEFAULT 0,
     constraint IDMETODO_PAGAMENTO primary key (IdMetodoPagamento));

create table ESTERNI (
     IdAuto SMALLINT not null,
     IdEsterni SMALLINT AUTO_INCREMENT not null,
     Nome VARCHAR(100) not null,
     Descrizione VARCHAR(200) not null,
     Link_Immagine VARCHAR(300) ,
     Prezzo INT not null,
     constraint IDESTERNI primary key (IdEsterni));

create table ESTERNI_SCELTE (
     IdAuto SMALLINT not null,
     IdEsterni SMALLINT not null,
     IdAutoConfigurata SMALLINT not null,
     constraint IDesterniscelte primary key (IdAuto, IdEsterni, IdAutoConfigurata));

create table INTERNI (
     IdAuto SMALLINT not null,
     IdInterni SMALLINT AUTO_INCREMENT not null,
     Nome VARCHAR(100) not null,
     Descrizione VARCHAR(200) not null,
     Link_Immagine VARCHAR(300) ,
     Prezzo INT not null,
     constraint IDINTERNI primary key (IdInterni));

create table INTERNO_SCELTO (
     IdAutoConfigurata SMALLINT not null,
     IdAuto SMALLINT not null,
     IdInterni SMALLINT not null,
     constraint IDinternoscelto primary key (IdAutoConfigurata, IdAuto, IdInterni));

create table Gestisce (
     IdAuto SMALLINT not null,
     P_IVA VARCHAR(11) not null,
     constraint IDGestisce primary key (IdAuto, P_IVA));

create table LISTA_DESIDERI (
     CF VARCHAR(16) not null,
     IdAuto SMALLINT not null,
     constraint IDLISTA_DESIDERI primary key (IdAuto, CF));

create table MOTORE (
     IdAuto SMALLINT not null,
     IdMotore SMALLINT AUTO_INCREMENT not null,
     Nome VARCHAR(100) not null,
     Descrizione VARCHAR(200) not null,
     Prezzo INT not null,
     Cilindrata VARCHAR(20),
     Cavalli VARCHAR(20),
     constraint IDMOTORE primary key (IdMotore));

create table OPTIONAL (
     IdAuto SMALLINT not null,
     IdOptional SMALLINT AUTO_INCREMENT not null,
     Nome VARCHAR(100) not null,
     Descrizione VARCHAR(200) not null,
     Link_Immagine VARCHAR(300) ,
     Prezzo INT not null,
     constraint IDOPTIONAL primary key (IdOptional));

create table OPTIONAL_SCELTO (
     IdAutoConfigurata SMALLINT not null,
     IdAuto SMALLINT not null,
     IdOptional SMALLINT not null,
     constraint IDoptionalscelti primary key (IdAutoConfigurata, IdAuto, IdOptional));

create table NOTIFICHE (
     IdNotifica INT AUTO_INCREMENT not null,
     IdOrdine SMALLINT null,
     IdPrimario VARCHAR(16) not null,
     Messaggio VARCHAR(300) not null,
     Titolo VARCHAR(100) not null,
     Letto BOOLEAN DEFAULT 0,
     constraint IDNotifiche primary key (IdNotifica));

create table ORDINE (
     IdOrdine SMALLINT AUTO_INCREMENT not null,
     IdPagamento SMALLINT,
     IdSpedizione SMALLINT,
     P_IVA VARCHAR(11) null,
     IdStato SMALLINT not null,
     constraint IDORDINE_ID primary key (IdOrdine),
     constraint FKEffettuazione_ID unique (IdPagamento));

create table PAGAMENTO (
     IdPagamento SMALLINT AUTO_INCREMENT not null,
     IdMetodoPagamento SMALLINT not null,
     constraint IDPAGAMENTO_ID primary key (IdPagamento));

create table SPEDIZIONE (
     IdSpedizione SMALLINT AUTO_INCREMENT not null,
     CF VARCHAR(16) not null,
     Via VARCHAR(200) not null,
     N_Civico INT not null,
     Citta VARCHAR(100) not null,
     Provincia VARCHAR(100) not null,
     CAP INT not null,
     constraint IDSPEDIZIONE primary key (IdSpedizione));

create table STATO_ORDINE (
     IdStato SMALLINT AUTO_INCREMENT not null,
     Nome_Stato VARCHAR(80) not null,
     constraint IDSTATO_ORDINE primary key (IdStato));

create table RILANCIO (
     Importo INT not null,
     IdRilancio INT AUTO_INCREMENT not null,
     CF VARCHAR(16) not null,
     IdAsta SMALLINT not null,
     constraint IDRILANCIO primary key (IdRilancio));

create table NOTIFICHE_VENDITORE (
     IdNotifica INT AUTO_INCREMENT not null,
     IdOrdine SMALLINT not null,
     P_IVA VARCHAR(11) not null,
     Messaggio VARCHAR(400) not null,
     Titolo VARCHAR(100) not null,
     Letto BOOLEAN DEFAULT 0,
     constraint IDNotifiche_venditore primary key (IdNotifica));

create table NOTIFICHE_CORRIERE (
     IdNotifica INT AUTO_INCREMENT not null,
     IdOrdine SMALLINT not null,
     P_IVA VARCHAR(11) not null,
     Messaggio VARCHAR(400) not null,
     Titolo VARCHAR(100) not null,
     Letto BOOLEAN DEFAULT 0,
     constraint IDNotifiche_corriere primary key (IdNotifica));

alter table AUTO_CONFIGURATA add constraint FKRicevuta
     foreign key (IdOrdine)
     references ORDINE (IdOrdine);

alter table AUTO_CONFIGURATA add constraint FKconfigura
     foreign key (CF)
     references CLIENTE (CF);

alter table AUTO_CONFIGURATA add constraint FKconfigurata_FK
     foreign key (IdAuto)
     references AUTO (IdAuto);

alter table AUTO_CONFIGURATA add constraint FKconfigura_Motore
     foreign key (IdMotore)
     references MOTORE (IdMotore);

alter table AUTO_IN_ASTA add constraint FKGestisceAste
     foreign key (P_IVA)
     references VENDITORE (P_IVA);

alter table AUTO_IN_ASTA add constraint FKvincitore 
     foreign key (Vincitore)
     references CLIENTE (CF);

alter table METODO_PAGAMENTO add constraint FKha
     foreign key (CF)
     references CLIENTE (CF);

alter table ESTERNI add constraint FKConf_Esterni
     foreign key (IdAuto)
     references AUTO (IdAuto);

alter table ESTERNI_SCELTE add constraint FKest_AUT
     foreign key (IdAutoConfigurata)
     references AUTO_CONFIGURATA (IdAutoConfigurata);

alter table ESTERNI_SCELTE add constraint FKest_EST
     foreign key (IdAuto, IdEsterni)
     references ESTERNI (IdAuto, IdEsterni);

alter table Gestisce add constraint FKGes_VEN
     foreign key (P_IVA)
     references VENDITORE (P_IVA);

alter table Gestisce add constraint FKGes_AUT
     foreign key (IdAuto)
     references AUTO (IdAuto);

alter table INTERNI add constraint FKConf_Interni
     foreign key (IdAuto)
     references AUTO (IdAuto);

alter table INTERNO_SCELTO add constraint FKint_INT
     foreign key (IdAuto, IdInterni)
     references INTERNI (IdAuto, IdInterni);

alter table INTERNO_SCELTO add constraint FKint_AUT
     foreign key (IdAutoConfigurata)
     references AUTO_CONFIGURATA (IdAutoConfigurata);

alter table LISTA_DESIDERI add constraint FKLIS_AUT
     foreign key (IdAuto)
     references AUTO (IdAuto);

alter table LISTA_DESIDERI add constraint FKLIS_CLI
     foreign key (CF)
     references CLIENTE (CF);

alter table MOTORE add constraint FKConf_Motore
     foreign key (IdAuto)
     references AUTO (IdAuto);

alter table Notifiche add constraint FKNot_UTE
     foreign key (IdPrimario)
     references CLIENTE (CF);

alter table Notifiche add constraint FKNot_ORD
     foreign key (IdOrdine)
     references ORDINE (IdOrdine);

alter table OPTIONAL add constraint FKConf_Optional
     foreign key (IdAuto)
     references AUTO (IdAuto);

alter table OPTIONAL_SCELTO add constraint FKopt_OPT
     foreign key (IdAuto, IdOptional)
     references OPTIONAL (IdAuto, IdOptional);

alter table OPTIONAL_SCELTO add constraint FKopt_AUT
     foreign key (IdAutoConfigurata)
     references AUTO_CONFIGURATA (IdAutoConfigurata);

alter table ORDINE add constraint FKEffettuazione_FK
     foreign key (IdPagamento)
     references PAGAMENTO (IdPagamento);

alter table ORDINE add constraint FKPresoinconsegna
     foreign key (P_IVA)
     references CORRIERE (P_IVA);

alter table ORDINE add constraint FKStatoordine
     foreign key (IdStato)
     references STATO_ORDINE (IdStato);

alter table ORDINE add constraint FKspedizione
     foreign key (IdSpedizione)
     references SPEDIZIONE (IdSpedizione);

alter table PAGAMENTO add constraint FKroba
     foreign key (IdMetodoPagamento)
     references METODO_PAGAMENTO (IdMetodoPagamento);

alter table SPEDIZIONE add constraint FKSceglie
     foreign key (CF)
     references CLIENTE (CF);

alter table IMMAGINE add constraint FKAppartiene
     foreign key (IdAuto)
     references AUTO (IdAuto);

alter table COMMENTO add constraint FKCom_CLI
     foreign key (CF)
     references CLIENTE (CF);

alter table COMMENTO add constraint FKCom_AUT
     foreign key (IdAuto)
     references AUTO (IdAuto);

alter table RILANCIO add constraint FKpaga
     foreign key (CF)
     references CLIENTE (CF);

alter table RILANCIO add constraint FKeffettuato
     foreign key (IdAsta)
     references AUTO_IN_ASTA (IdAsta);

alter table NOTIFICHE_VENDITORE add constraint FKNot_VENDITORE
     foreign key (P_IVA)
     references VENDITORE (P_IVA);

alter table NOTIFICHE_VENDITORE add constraint FKNot_ORD_VENDITORE
     foreign key (IdOrdine)
     references ORDINE (IdOrdine);

alter table NOTIFICHE_CORRIERE add constraint FKNot_CORRIERE
     foreign key (P_IVA)
     references CORRIERE (P_IVA);

alter table NOTIFICHE_CORRIERE add constraint FKNot_ORD_CORRIERE
     foreign key (IdOrdine)
     references ORDINE (IdOrdine);
INSERT INTO `stato_ordine` (`IdStato`, `Nome_Stato`) VALUES
	(1, 'Carrello'),
	(2, 'Richiesta Inviata'),
	(3, 'Pronto per spedizione'),
	(4, 'Spedito'),
	(5, 'In consegna'),
	(6, 'Consegnato');

INSERT INTO `corriere` (`P_IVA`, `Email`, `Telefono`, `Nome_Utente`, `Password`, `Salt`) VALUES
('04209680158', 'gianluca.pasolini99@gmail.com', '3665040354', 'DHL', '094c7f5923ba6cf8c0cb3c8ba91f0f603b7ccc037d66d1dd327374ea539eb87842c54963184ba96ed1b53e395a0f20e50fd19c206e2f4902768b7b85312dafb3', 'f7f0b7741e89a94e7a1d924190edaa35bcc677c9ab007cdd5572eb5b47f592988250cc889ef8162f5ab75b710b2cbc308a8e8baaf6b114e5da4db9e372114e0f'),
('04507990150', 'gianluca.pasolini99@gmail.com', '3665040354', 'Bartolini', '4e9790a104bb554f35ca4441e0f8a4e3e263fffc5226f995f33504a6a70ee9a5d88646df8c7a3f5c6b24a716b24a8ce9728cfbe5d88e810ce8cde01ec47d9f5d', '87c36446e4d0ba2788f506a70e6a9e2dfe74b6bc60b094349e19c023b9201883c9d55f42fc3a1ab16bb814495e2d7acc5ad5eb06c9c4dc365428b8e5566033ef'),
('12144660151', 'gianluca.pasolini99@gmail.com', '3665040354', 'GLS', '1cc1ee02d01398aeed2164b5ec199ae9a347aa61a2ee689ed70616d69f312e31ca04d46d7a9d5382840c94fc8bb27e38f00cbb3c4e8dbb315a3c189aabaf22fd', '0cc96a091db4d25e51fa3612115e30cf2028e03b2e04903bc2c18de463c2bedcb403258fc14a83cd66f580ab784844bd49654bba7b4509191231fa392ffe9fdb');
-- passw di tutti gli utenti: GianlucaPasolini2020!

INSERT INTO `venditore` (`P_IVA`, `Casa_Asta`, `Email`, `Telefono`, `Nome_Utente`, `Password`, `Salt`, `Link_logo`, `Link_ImmCopertina`, `Via`, `N_Civico`, `Citta`, `Provincia`, `CAP`) VALUES
('02751850047', 0, 'gianluca.pasolini99@gmail.com', '3665040354', 'Bentley', '9144574ec7c5e4a22316ade2aa4efd3397479562b0c1682b328da8c22adbed4ec47131ffdcf18955ba2a978cf49e18530e3edb95f9686112668c9086186fe7b8', '03817048c944c5c2c76e0de81deba77b21223804997278c7057ddf6737938dfd4749cd0e5023e3fde227803b412a7b3bd4d153c3dd3560ad149201c6e3cda6d3', 'bentley.png', 'copertina_bentley.jpg', 'Corso Stati Uniti', 1, 'Padova', 'Padova', 35127),
('00159560366', 0, 'gianluca.pasolini99@gmail.com', '3665040354', 'Ferrari', '2e6eb31d81af32b9849f82174adc9830f26cd61f2cee0cf6bf0b6813434e056b91be963876096106df4f97fabedc08a68dc488f724a17a0361d2b19e1f66b1fe', '9f5bb3818e76dcf451e79175ff8e5900294b33f61d2c7523573ccd376828603671f6de6b83a22670ede37bdf3e6b9618dca532dfb813b9c24b9ac21857ffaffe', 'ferrari.png', 'copertina_ferrari.jpg', 'Viale Enzo Ferrari', 27, 'Maranello', 'Modena', 41053),
('00591801204', 0, 'gianluca.pasolini99@gmail.com', '3665040354', 'Lamborghini', 'be18920480207a2f6d12d7a1ae443ec211933167b8e804ae752eb5b02dfb299ea985cffb67a69e90d2a81dfe8607acfd2dcc04b3ffb14dea53bb4a33c9d95b6a', '71cbe4d714b34d0eee46346588065bf11dd3abff7718b48e41acfce16f34b2d7267f1f08daa918a0245350867571728b5ad4d5fdf9db6df141a30c75c01924db', 'Lamborghini.svg.png', 'copertina-lamborghini.jpg', 'Via Modena', 12, 'Sant\'Agata Bolognese', 'Bologna', 40019),
('03970540963', 0, 'gianluca.pasolini99@gmail.com', '3665040354', 'RollsRoyce', '8b426c5d0f484c912406d11ecd45e3d152c861b8a61caaab4b5ed9482953a7f762c516933cfcaaeaa7a79c4d5d0c2e13caf1afc637c6502fe62f9fb6b4f7dd2b', 'c19d07f791a2a3beee89ca70396abe82918e20ce7ed8fd0e571b4f86b8c5b20d1bccc94dc32068d48a658605ed7bad4a30143b3bebcd26740b9862c90237720a', 'rolls-royce.png', 'copertina_RollsRoyce.jpg', 'Via Salaria', 1268, 'Roma', 'Roma', 00138),
('07024150968', 0, 'gianluca.pasolini99@gmail.com', '3665040354', 'Tesla', '47d57f1888bd605a252555648453f0dffde71d24850b2163d59be9ae0d17f78c512b3c7710443be88f800469f1effea2e68c7863790765acaff5fdcca2af210a', '025239c4c9ccac2bc45a06e5857a83a310a28c7c6a64f9d71d83866630daeaaac371b0e138cf7d90eb4f7afff87a3bc4a9d59a7c555fef972e2a7a0637c58ed0', 'tesla-logo-marketing-studio.jpg', 'copertina_tesla.jpg', 'Piazza Gae Aulenti', 4, 'Milano', 'Milano', 20154),
('07973780013', 0, 'gianluca.pasolini99@gmail.com', '3665040354', 'Fiat', '2a0306eed509a8f7af3d9970ac629672e0e2767f05dd6768d39b36442cdfe5d94315ed1d54dc8932fb64e86f6f38ae1f0796e1d8e65dfe83f2e4c04fe984893f', '5c3ccdba1392507e8a9f14f56267f863ec570f9d4dbe1cf00174341314a41853955151a7d5b797e3a19a560bc3776bf4f1367b017394f0bcdfa95daed8ab9801', 'Fiat.svg.png', 'copertina_fiat.jpg', 'Via Agnano Agli Astroni', 183, 'Napoli', 'Napoli', 80125),
('06325761002', 0, 'gianluca.pasolini99@gmail.com', '3665040354', 'Mercedes', '205488db637e2e2a4067f82650972aee921fa4c4c595ad572692bedf89ab6ca9c021996da2240f0f0a44edf71747b07662fdde095313372dcfc07b6fb43b20b8', '32f5229ad07b5279da226f4894fb8d47f59eb33dc12d9c038c481bd6a67d407f08c6067e4abebc8a1462013eec0840f61d27ed660aabf8c6067e174573d0f87a', 'logo_mercedes.jpg', 'Copertina_Mercedes.jpg', 'Via Martiri della Libertà', 1, 'Venezia', 'Venezia', 30174);
-- passw di tutti gli utenti: GianlucaPasolini2020!

INSERT INTO `cliente` (`CF`, `Nome`, `Cognome`, `Email`, `Telefono`, `Nome_Utente`, `Password`, `Salt`, `Via`, `N_Civico`, `Citta`, `Provincia`, `CAP`) VALUES
('LGHLCU59C55F205B', 'Lucia', 'Alighieri', 'gianluca.pasolini99@gmail.com', '3665040354', 'LuciaAlighieri', '3c36cbd65043de94d24eacdecdee6724b09b63f6584f920c479ec8ae5f953d682137aa2621e315b9472a79ee8e95c060074c9cc67fa6be38782faf63082858e1', 'b21736d957c57b6c21c87e9d7484a5c7a925b16aea647b84aaeab64a7d10e45d926bce52fa26385d8d0cb37e99fe99f789d355a10855361139e9cd039c2e70b5', NULL, NULL, NULL, NULL, NULL),
('MRRFNC90S27F839O', 'Franco', 'Marrone', 'gianluca.pasolini99@gmail.com', '3665040354', 'FrancoMarrone', 'dca85179b23ebe117246aec5e5034211e3596b56b0e76f84824211dade7585011079c78b1c1b178d883adf5a0b2acc3fe345f1587e53505b92621553b0b3d1be', 'c8cd6b510cc37e9e555a91a5a2589e3c66f412149a4ad5a471f37a43f18beacc0a1c68d22f34efd1d6d7a7ae9ab2ae2a0bb4e1d31460fda5ed1fada5cd8c9b3b', 'Via Bartolomeo Chioccarelli', 41, 'Napoli', 'Napoli', 80133),
('PSCLNS70M10H501C', 'Alfonso', 'Pascoli', 'gianluca.pasolini99@gmail.com', '3665040354', 'AlfonsoPascoli', '3a140606a7c0125f83ebc29ca8621d87570a4c397b66d1bfc0d42316e0677db892ab9c02434163cd46afcdbb97ed0d5412bbd5ad0294a6cc865048fcfd39f35a', '02a25d10cae5ae9bd01a1292a2e9ccf1fba55dd8cd235a3f8f32e28bbba7e5e56951c740b75aa693f983827903b72f874fe78a390a641b41e49ab1226b500600', NULL, NULL, NULL, NULL, NULL),
('PSLGLC99T21H294J', 'Gianluca', 'Pasolini', 'gianluca.pasolini99@gmail.com', '3665040354', 'GianlucaPasolini', '1e0db0cc159edd2fdb1b613af71d727cb90ddf4156639fec2ae80bba6587d53548c759bd4619b72e5d6c866d3bc64c2e1ba4b5ba64d7d0983bc00fa1eaa8d8fa', 'ce1b72a54f75166e880a2e4a071b30c61c828652a3d7d47a5ef009d2265f1b75ba7250f62b142bef08d9280175917e8c7301758d6738c3178d8585da6807082f', 'Via Gino Bonfi', 36, 'Rimini', 'Rimini', 47926);
-- passw di tutti gli utenti: GianlucaPasolini2020!

-- metodo_pagamento
INSERT INTO `metodo_pagamento` (`CF`, `Fatturazione`) VALUES
('PSLGLC99T21H294J', 1),
('PSLGLC99T21H294J', 0),
('LGHLCU59C55F205B', 0),
('MRRFNC90S27F839O', 1),
('PSCLNS70M10H501C', 0);

-- spedizione
INSERT INTO `spedizione` (`CF`, `Via`, `N_Civico`, `Citta`, `Provincia`, `CAP`) VALUES
('PSLGLC99T21H294J', 'Via Alfonso Malatesta', 64, 'Rimini', 'Rimini', 47928),
('PSLGLC99T21H294J', 'Via Sigismondo Malatesta', 96, 'Gabicce', 'Rimini', 47940),
('PSLGLC99T21H294J', 'Via Dativi', 108, 'Domagnano', 'San Marino', 47895),
('LGHLCU59C55F205B', 'Via Marcona', 12, 'Milano', 'Milano', 20129),
('MRRFNC90S27F839O', 'Via Doglie', 39, 'Ercolano', 'Napoli', 80056),
('MRRFNC90S27F839O', 'Via Bartolomeo Chioccarelli', 41, 'Napoli', 'Napoli', 80133),
('PSCLNS70M10H501C', 'Via Martiri di Civitella', 3, 'Arezzo', 'Arezzo', 52100);

-- MI SERVONO LE AUTO
INSERT INTO `auto` (`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) VALUES
(1, 'Pista', 120000, 'FerrariPista.PNG', 'Il V8 più potente nella storia Ferrari', 'Ferrari_Pista_audio.mp3', 'Ferrari_Pista_video.mp4'),
(2, 'Roadster', 50000, '2020-Tesla-Roadster-01.jpg', 'L\'auto più veloce al mondo, in grado di garantire performance, autonomia e accelerazione da record.', NULL, NULL),
(3, '500', 20000, 'autoFiat500.jpeg', 'Auto molto ricercata lato design con particolari accattivanti. Non è un auto che ricerca le prestazioni bensì cerca di dare il massimo di se in città.', NULL, NULL),
(4, 'Continental', 180000, 'bentley-continental-gt.jpg', NULL, NULL, NULL),
(5, 'Huracan Svg', 200000, 'lamborghini_notizia.jpg', 'Macchina ideata e pronta per essere venduta. è molto accattivamente con ottimo potenza e ben costruita.', 'Lamborghini_Huracan_Sound.mp3', 'Lamborghini_Huracan_video.mp4'),
(6, 'Ghost', 240000, 'RollsGhost.PNG', NULL, NULL, NULL),
(7, 'Classe S', 107000, 'mercedes-benz-s-class.jpg', NULL, 'Mercedes_ClasseS_Sound.mp3', 'Mercedes_ClasseS_video.mp4');

INSERT INTO `gestisce` (`IdAuto`, `P_IVA`) VALUES
(1, '00159560366'),
(2, '07024150968'),
(3, '07973780013'),
(4, '02751850047'),
(5, '00591801204'),
(6, '03970540963'),
(7, '06325761002');

INSERT INTO `immagine` (`Descrizione`, `LinkImmagine`, `IdImmagine`, `IdAuto`) VALUES 
('La Nuova Classe S è equipaggiata con i più moderni sistemi di assistenza alla guida, aumentando il comfort e la sensazione di sicurezza di chi si trova a bordo. Le tecnologie per la sicurezza, sia attiva sia passiva, analizzano costantemente ciò che accade in strada, prendendo in considerazione tutte le informazioni disponibili, anche quelle non direttamente visibili al guidatore.', 'mercedes_classeS_Imm1.jpeg', 1, 7),
('Il design pulito di Nuova Classe S si concentra sull\'essenziale: proporzioni perfette negli esterni ed esclusività contemporanea negli interni. Le innovazioni all\'avanguardia di Nuova Classe S s\'inseriscono così in un ambiente straordinario che conquista.', 'mercedes_classeS_Imm2.jpg', 2, 7),
('Lo stile del nuovo modello segue un’idea di minimalismo esteriore definito “Post Opulence”, che rappresenta la rinnovata idea del lusso del marchio britannico. Linee semplici, rispettose dei canoni tradizionali, ridefiniscono l’opulenza della limousine, peraltro esaltata da giochi di luce: 20 diodi a Led collocati nella parte superiore della mascherina illuminano la Pantheon grille, il cui retro è stato sottoposto a un trattamento opacizzante per attenuare e perfezionare il bagliore voluto dai progettisti.', 'RollsRoyce_ghost_Imm1.jpg', 3, 6),
('L’abitacolo riprende la tipica impostazione delle auto di Goodwood, sebbene anche qui sia stato ricercato un certo minimalismo stilistico. Pelli, legni e metalli sono ovviamente di elevata fattura, ma la vera novità è la fascia illuminata che si trova di fronte al sedile del passeggero: riprende il caratteristico motivo astrale del cielo dell’abitacolo circondando con 850 stelle il nome Ghost, grazie all’utilizzo di 152 diodi. Solo la lavorazione di questo elemento dell’abitacolo ha richiesto ben 10 mila ore di lavoro.', 'RollsRoyce_ghost_Imm2.jpg', 4, 6),
('La Continental GT è caratterizzata da linee pulite e superformate e da un corpo largo e basso, che evoca un senso di velocità e presenza. All\'apertura della vettura, una sequenza di illuminazione esterna pre-programmata vi dà il benvenuto. Per completare il suggestivo design interno della vettura, l\'illuminazione interna (montata di serie) può essere configurata secondo le vostre preferenze.', 'Bentley_continental_Imm1.jpg', 5, 4),
('Creata per una guida senza sforzo ed esaltante, la Continental GT è costruita su un telaio innovativo e su un\'architettura elettrica, con un motore W12 da 6,0 litri completamente nuovo e un cambio a doppia frizione a 8 marce, per un cambio di marcia fluido, veloce ed efficiente. Il risultato è un\'accelerazione da 0 a 60 mph in 3,6 secondi (da 0 a 100 km/h in 3,7 secondi) e una fenomenale velocità massima di 207 mph (333 km/h).', 'Bentley_continental_Imm2.jpg', 6, 4),
('Trattandosi di una supercar completamente elettrica, la Roadster sfrutta al massimo le potenzialità offerte dalla progettazione aerodinamica, garantendo efficienza e performance da record.', 'Tesla_Roadster_Imm1.jpg', 7, 2),
('Il tetto in vetro, leggero e rimovibile, può essere comodamente alloggiato nel bagagliaio per vivere l\'esperienza di guida di una decappottabile.', 'Tesla_Roadster_Imm2.jpg', 8, 2),
('Un grande contributo al miglioramento delle prestazioni della Ferrari 488 Pista è da attribuirsi all’approfondito lavoro di ricerca aerodinamica, che si è avvalso delle libertà progettuali concesse da un concept di prodotto focalizzato sull’innovazione e sulla performance senza compromessi.', 'Ferrari_Pista_Imm1.jpg', 9, 1),
('L’obiettivo dello sviluppo dinamico della Ferrari 488 Pista era ottenere una vettura fortemente caratterizzata da prestazioni meccaniche elevate in termini di tempi sul giro e prestazioni da fermo.', 'Ferrari_Pista_Imm2.jpg', 10, 1);

INSERT INTO `motore` (`IdAuto`, `IdMotore`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES
(7, 1, 'S 400 d 4MATIC Sedan', 'Diesel', 10000, '2925', '330'),
(7, 2, 'S 450 4MATIC', 'Benzina', 13000, '2996', '367 CV + 22 CV'),
(7, 3, 'S 500 4MATIC Sedan', 'Benzina', 22000, '4663', '435 CV + 22 CV'),
(6, 4, 'V12 twin-turbo', 'Benzina', 30000, '6499', '570'),
(5, 5, 'V8', 'Benzina', 0, '3999', '480'),
(5, 6, 'V16', 'Benzina', 40000, '5999', '650'),
(4, 7, 'V8 Mulliner', 'Benzina', 0, '3499', '399'),
(4, 8, 'W12', 'Benzina', 45000, '5999', '550'),
(3, 9, '70 KM', 'Elettrico', 0, NULL, '95'),
(3, 10, '87 KM', 'Elettrico', 3000, NULL, '118'),
(2, 11, 'base', 'Elettrico', 0, NULL, '400'),
(2, 12, 'performance', 'Elettrico', 160000, NULL, '1242'),
(1, 13, 'V8 - 90°twin-turbo', 'Benzina', 0, '3902', '720');

INSERT INTO `esterni` (`IdAuto`, `IdEsterni`, `Nome`, `Descrizione`, `Link_Immagine`, `Prezzo`) VALUES
(7, 1, 'nero', 'colore', 'mercedes_classeS_nero.jpg', 0),
(7, 2, 'rosso rubellite', 'colore', 'mercedes_classeS_rossorubellite.jpg', 1208),
(7, 3, 'grigio grafite', 'colore', 'mercedes_classeS_grigiografite.jpg', 1208),
(7, 4, 'verde smeraldo', 'colore', 'mercedes_classeS_verde.jpg', 1208),
(7, 5, '18" a 5 doppie razze', 'cerchi', 'mercedes_classeS_cerchio18.png', 0),
(7, 6, '20" a doppie razze', 'cerchi', 'mercedes_classeS_cerchio20.png', 2750),
(6, 7, '21" Twin Spoke Part', 'cerchi', 'rollsroyce_ghost_cerchio21.jpg', 4000),
(6, 8, '19" Painted Wheels', 'cerchi', 'rollsroyce_ghost_cerchio19.jpg', 500),
(6, 9, 'pure', 'colore', 'rollsroyce_ghost_colorPure.jpg', 0),
(6, 10, 'optic', 'colore', 'rollsroyce_ghost_colorOptic.jpg', 8000),
(5, 11, '20" special edition', 'cerchi', NULL, 0),
(5, 12, 'carbonio stock', 'colore', NULL, 0),
(4, 13, 'Jetstream II', 'colore', 'bentley_continental_colorjatstream2.jpg', 6000),
(4, 14, 'Sequin Blue', 'colore', 'bentley_continental_colorSequinBue.jpg', 2000),
(4, 15, 'Mulliner Driving Specification', 'cerchi', 'bentley_continental_cerchioMulliner.jpg', 0),
(4, 16, '21" Five Tri-Spoke Wheel', 'cerchi', 'bentley_continental_cerchio21.jpg', 2000),
(3, 17, 'Ice WHITE', 'colore', 'fiat_500_colorIceWHITE.jpg', 0),
(3, 18, 'Ocean Green', 'colore', 'fiat_500_colorOceanGreen.jpg', 700),
(3, 19, 'alluminio 15"', 'cerchi', 'fiat_500_cerchialluminio15.png', 0),
(3, 20, 'lega 16"', 'cerchi', 'fiat_500_cerchilega16.png', 400),
(2, 21, 'rosso', 'colore', NULL, 0),
(2, 22, 'nero', 'colore', NULL, 0),
(2, 23, 'bianco', 'colore', NULL, 0),
(1, 24, 'Rosso Corsa', 'colore', 'Ferrari_Pista_coloreRossoCorsa.png', 0),
(1, 25, 'Grigio Titanio', 'colore', 'Ferrari_Pista_coloreGrigioTitanio.png', 3500),
(1, 26, 'Azzurro California', 'colore', 'Ferrari_Pista_coloreAzzurroCalifornia.png', 6000),
(1, 27, 'Standard', 'cerchi', 'Ferrari_Pista_cerchioStandard.png', 0),
(1, 28, '20" Forgiati Oro', 'cerchi', 'Ferrari_Pista_cerchio20Oro.png', 2000),
(1, 29, 'Forgiati grigio corsa', 'cerchi', 'Ferrari_Pista_cerchioForgiatoOpaco.png', 5000);

INSERT INTO `interni` (`IdAuto`, `IdInterni`, `Nome`, `Descrizione`, `Link_Immagine`, `Prezzo`) VALUES
(7, 1, 'pelle black', 'rivestimento', 'mercedes_classeS_rivestimentoNero.jpg', 0),
(7, 2, 'marrone Siena / nero', 'rivestimento', 'mercedes_classeS_marroneNero.jpg', 2800),
(7, 3, 'legno di pioppo antracite', 'inserto', 'mercedes_classeS_insertoPioppo.jpg', 0),
(7, 4, 'legno di noce marrone', 'inserto', 'mercedes_classeS_legnoNoce.jpg', 3904),
(6, 5, 'bianco', 'rivestimento', NULL, 0),
(6, 6, 'nero', 'rivestimento', NULL, 5000),
(6, 7, 'Walnut Burr', 'inserto', 'rollsroyce_ghost_WalnutBurr.jpg', 2000),
(6, 8, 'Open Pore - Paldao', 'inserto','rollsroyce_ghost_Paldao.jpg', 6000),
(5, 9, 'Alcantara', 'rivestimento', NULL, 8000),
(4, 10, 'Porpora', 'rivestimento', 'bentley_continental_internoporpora.jpg', 0),
(4, 11, 'Hotspur', 'rivestimento', 'bentley_continental_internoHotspur.jpg', 4000),
(3, 12, 'Grigio', 'rivestimento', NULL, 0),
(2, 13, 'Bianco', 'rivestimento', NULL, 0),
(2, 14, 'Nero', 'rivestimento', NULL, 0),
(1, 15, 'Alcantara Blu scuro', 'rivestimento', 'Ferrari_Pista_internoBluScuro.png', 0),
(1, 16, 'Alcantara Nero', 'rivestimento', 'Ferrari_Pista_internoNero.png', 1000),
(1, 17, 'Alcantara Cuoio', 'rivestimento', 'Ferrari_Pista_internoCuoio.png', 3000),
(1, 18, 'Serie', 'contagiri', 'Ferrari_Pista_internoContagiriSerie.png', 0),
(1, 19, 'Rosso', 'contagiri', 'Ferrari_Pista_internoContagiriRosso.png', 2000),
(1, 20, 'Giallo', 'contagiri', 'Ferrari_Pista_internoContagiriGiallo.png', 2000);

INSERT INTO `optional` (`IdAuto`, `IdOptional`, `Nome`, `Descrizione`, `Link_Immagine`, `Prezzo`) VALUES
(7, 1, 'Luci Digitali Led', 'luce', 'mercedes_classeS_luce.jpg', 2306),
(7, 2, 'Display del guidatore 3D', 'display', 'mercedes_classeS_Display3D.jpg', 1400),
(6, 3, 'Essensa Spirito Oro', 'Spirito', 'rollsroyce_ghost_GoldSpirit.jpg', 8000),
(6, 4, 'Configurazione Teatro posteriore', 'display', 'rollsroyce_ghost_RearTheatre.jpg', 5000),
(5, 5, 'Voltante multifunzione', 'volante', NULL, 5000),
(5, 6, 'Razzi posteriori', 'turbo', NULL, 40000),
(4, 7, 'dettagli in diamante', 'finitura', 'bentley_continental_optionaldiamante.jpg', 80000),
(4, 8, 'visualizzazzione 360°', 'camera', 'bentley_continental_optionalCamere.jpg', 2000),
(3, 9, 'Easy wallbox', 'ricarica', 'fiat_500_optionalRicarica.jpg', 399),
(3, 10, 'Sensore posteriore e anteriori', 'sensori', NULL, 600),
(3, 11, 'Antifurto perimetrale', 'sicurezza', NULL, 350),
(2, 12, 'Razzi space x', 'performance', NULL, 40000),
(1, 13, 'pianale carbonio con tappetino', 'tappetini', 'Ferrari_Pista_optionalTappetinoCarbonio.png', 10000),
(1, 14, 'sedili racing carbonio', 'sedili', 'Ferrari_Pista_optionalSedileRacing.png', 5000),
(1, 15, 'codino scarico in titanio', 'codini scarico', 'Ferrari_Pista_optionalCodiniTitanio.png', 9000);

-- lista_desideri
INSERT INTO `lista_desideri` (`CF`, `IdAuto`) VALUES 
('PSLGLC99T21H294J', '1'),
('PSLGLC99T21H294J', '4'),
('PSLGLC99T21H294J', '7'),
('LGHLCU59C55F205B', '2'),
('MRRFNC90S27F839O', '3'),
('MRRFNC90S27F839O', '6'),
('PSCLNS70M10H501C', '1'),
('PSCLNS70M10H501C', '3'),
('PSCLNS70M10H501C', '5');

-- MI SERVONO GLI ORDINI
INSERT INTO `pagamento` (`IdPagamento`, `IdMetodoPagamento`) VALUES
(1, 1),
(2, 2),
(5, 3),
(4, 4),
(3, 5);

INSERT INTO `ordine` (`IdOrdine`, `IdPagamento`, `IdSpedizione`, `P_IVA`, `IdStato`) VALUES
(1, 1, 3, '04507990150', 6),
(2, 2, 1, '12144660151', 3),
(3, 3, 7, '04507990150', 4),
(4, 4, 6, '04507990150', 2),
(5, 5, 4, '04209680158', 2);

INSERT INTO `auto_configurata` (`IdAutoConfigurata`, `IdAuto`, `IdOrdine`, `IdMotore`, `CF`, `PrezzoTotale`) VALUES
(1, 1, 1, 13, 'PSLGLC99T21H294J', 151000),
(2, 6, 2, 4, 'PSLGLC99T21H294J', 301000),
(3, 2, 2, 12, 'PSLGLC99T21H294J', 250000),
(4, 5, 2, 6, 'PSLGLC99T21H294J', 288000),
(5, 3, 3, 10, 'PSCLNS70M10H501C', 25449),
(6, 7, 4, 3, 'MRRFNC90S27F839O', 143368),
(7, 3, 4, 9, 'MRRFNC90S27F839O', 21099),
(8, 4, 5, 8, 'LGHLCU59C55F205B', 313000);

INSERT INTO `esterni_scelte` (`IdAuto`, `IdEsterni`, `IdAutoConfigurata`) VALUES
(1, 24, 1),
(1, 29, 1),
(2, 21, 3),
(3, 18, 5),
(3, 18, 7),
(3, 19, 7),
(3, 20, 5),
(4, 14, 8),
(4, 15, 8),
(5, 11, 4),
(5, 12, 4),
(6, 7, 2),
(6, 10, 2),
(7, 4, 6),
(7, 6, 6);

INSERT INTO `interno_scelto` (`IdAutoConfigurata`, `IdAuto`, `IdInterni`) VALUES
(1, 1, 15),
(1, 1, 19),
(2, 6, 5),
(2, 6, 8),
(3, 2, 13),
(4, 5, 9),
(5, 3, 12),
(6, 7, 2),
(6, 7, 4),
(7, 3, 12),
(8, 4, 11);

INSERT INTO `optional_scelto` (`IdAutoConfigurata`, `IdAuto`, `IdOptional`) VALUES
(1, 1, 13),
(1, 1, 14),
(1, 1, 15),
(2, 6, 3),
(2, 6, 4),
(3, 2, 12),
(4, 5, 6),
(5, 3, 9),
(5, 3, 10),
(5, 3, 11),
(6, 7, 1),
(6, 7, 2),
(7, 3, 9),
(8, 4, 7),
(8, 4, 8);

INSERT INTO `notifiche` (`IdNotifica`, `IdOrdine`, `IdPrimario`, `Messaggio`, `Titolo`, `Letto`) VALUES
(1, 1, 'PSLGLC99T21H294J', 'Buongiorno GianlucaPasolini, il tuo pagamento di 151000 euro per l\'ordine 1 è andato a buon fine, nei prossimi giorni riceverà notifiche dal corriere riguardo la spedizione. Grazie.', 'Aggiornamento stato Ordine Pagamento - Cliente', 1),
(2, 2, 'PSLGLC99T21H294J', 'Buongiorno GianlucaPasolini, il tuo pagamento di 839000 euro per l\'ordine 2 è andato a buon fine, nei prossimi giorni riceverà notifiche dal corriere riguardo la spedizione. Grazie.', 'Aggiornamento stato Ordine Pagamento - Cliente', 1),
(3, 3, 'PSCLNS70M10H501C', 'Buongiorno AlfonsoPascoli, il tuo pagamento di 25449 euro per l\'ordine 3 è andato a buon fine, nei prossimi giorni riceverà notifiche dal corriere riguardo la spedizione. Grazie.', 'Aggiornamento stato Ordine Pagamento - Cliente', 1),
(4, 4, 'MRRFNC90S27F839O', 'Buongiorno FrancoMarrone, il tuo pagamento di 164467 euro per l\'ordine 4 è andato a buon fine, nei prossimi giorni riceverà notifiche dal corriere riguardo la spedizione. Grazie.', 'Aggiornamento stato Ordine Pagamento - Cliente', 0),
(5, 5, 'LGHLCU59C55F205B', 'Buongiorno LuciaAlighieri, il tuo pagamento di 313000 euro per l\'ordine 5 è andato a buon fine, nei prossimi giorni riceverà notifiche dal corriere riguardo la spedizione. Grazie.', 'Aggiornamento stato Ordine Pagamento - Cliente', 0),
(6, 1, 'PSLGLC99T21H294J', 'Buongiorno GianlucaPasolini, l\'ordine numero: 1 da lei effettuato risulta essere PRONTO PER LA SPEDIZIONE, prossimamente le verrà riferito che sarà spedito. Grazie.', 'Aggiornamento stato Ordine - Cliente', 0),
(7, 1, 'PSLGLC99T21H294J', 'Buongiorno GianlucaPasolini, l\'ordine numero: 1 da lei effettuato risulta essere SPEDITO, riceverà l\'ordine al più presto e verrà informato sul giorno di consegna. Grazie.', 'Aggiornamento stato Ordine - Cliente', 0),
(8, 1, 'PSLGLC99T21H294J', 'Buongiorno GianlucaPasolini, l\'ordine numero: 1 da lei effettuato risulta essere IN CONSEGNA, riceverà l\'ordine in giornata. Grazie.', 'Aggiornamento stato Ordine - Cliente', 0),
(9, 1, 'PSLGLC99T21H294J', 'Buongiorno GianlucaPasolini, l\'ordine numero: 1 da lei effettuato risulta essere CONSEGNATO, per eventuali problemi non esiti a contattarci e a lasciare una valutazione nella sezione dedicata. Grazie.', 'Aggiornamento stato Ordine - Cliente', 0),
(10, 3, 'PSCLNS70M10H501C', 'Buongiorno AlfonsoPascoli, l\'ordine numero: 3 da lei effettuato risulta essere PRONTO PER LA SPEDIZIONE, prossimamente le verrà riferito che sarà spedito. Grazie.', 'Aggiornamento stato Ordine - Cliente', 0),
(11, 3, 'PSCLNS70M10H501C', 'Buongiorno AlfonsoPascoli, l\'ordine numero: 3 da lei effettuato risulta essere SPEDITO, riceverà l\'ordine al più presto e verrà informato sul giorno di consegna. Grazie.', 'Aggiornamento stato Ordine - Cliente', 0),
(12, 2, 'PSLGLC99T21H294J', 'Buongiorno GianlucaPasolini, l\'ordine numero: 2 da lei effettuato risulta essere PRONTO PER LA SPEDIZIONE, prossimamente le verrà riferito che sarà spedito. Grazie.', 'Aggiornamento stato Ordine - Cliente', 0);

INSERT INTO `notifiche_corriere` (`IdNotifica`, `IdOrdine`, `P_IVA`, `Messaggio`, `Titolo`, `Letto`) VALUES
(1, 1, '04507990150', 'Buongiorno Bartolini, un cliente ha creato l\'ordine numero: 1 e ha richiesto lei come corriere. Il PAGAMENTO è già stato EFFETTUATO. La preghiamo di accedere al suo account e aggiornare lo stato dell\'ordine a PRONTO PER LA SPEDIZIONE per avvisare i venditori. Successivamente dovrà ritirare le auto e aggiornare lo stato ordine di conseguenza. Per eventuali problemi non esiti a contattare l\'assisten', 'Aggiunta ordine da ritirare e consegnare - Corriere', 0),
(2, 2, '12144660151', 'Buongiorno GLS, un cliente ha creato l\'ordine numero: 2 e ha richiesto lei come corriere. Il PAGAMENTO è già stato EFFETTUATO. La preghiamo di accedere al suo account e aggiornare lo stato dell\'ordine a PRONTO PER LA SPEDIZIONE per avvisare i venditori. Successivamente dovrà ritirare le auto e aggiornare lo stato ordine di conseguenza. Per eventuali problemi non esiti a contattare l\'assistenza di ', 'Aggiunta ordine da ritirare e consegnare - Corriere', 0),
(3, 3, '04507990150', 'Buongiorno Bartolini, un cliente ha creato l\'ordine numero: 3 e ha richiesto lei come corriere. Il PAGAMENTO è già stato EFFETTUATO. La preghiamo di accedere al suo account e aggiornare lo stato dell\'ordine a PRONTO PER LA SPEDIZIONE per avvisare i venditori. Successivamente dovrà ritirare le auto e aggiornare lo stato ordine di conseguenza. Per eventuali problemi non esiti a contattare l\'assisten', 'Aggiunta ordine da ritirare e consegnare - Corriere', 0),
(4, 4, '04507990150', 'Buongiorno Bartolini, un cliente ha creato l\'ordine numero: 4 e ha richiesto lei come corriere. Il PAGAMENTO è già stato EFFETTUATO. La preghiamo di accedere al suo account e aggiornare lo stato dell\'ordine a PRONTO PER LA SPEDIZIONE per avvisare i venditori. Successivamente dovrà ritirare le auto e aggiornare lo stato ordine di conseguenza. Per eventuali problemi non esiti a contattare l\'assisten', 'Aggiunta ordine da ritirare e consegnare - Corriere', 0),
(5, 5, '04209680158', 'Buongiorno DHL, un cliente ha creato l\'ordine numero: 5 e ha richiesto lei come corriere. Il PAGAMENTO è già stato EFFETTUATO. La preghiamo di accedere al suo account e aggiornare lo stato dell\'ordine a PRONTO PER LA SPEDIZIONE per avvisare i venditori. Successivamente dovrà ritirare le auto e aggiornare lo stato ordine di conseguenza. Per eventuali problemi non esiti a contattare l\'assistenza di ', 'Aggiunta ordine da ritirare e consegnare - Corriere', 0),
(6, 1, '04507990150', 'Buongiorno Bartolini, le confermiamo che l\'ordine numero: 1 è stato CONSEGNATO correttamente, per eventuali problemi non esiti a contattare l\'assistenza di Car Shop. Grazie.', 'Conferma consegna ordine effettuato - Corriere', 0);

INSERT INTO `notifiche_venditore` (`IdNotifica`, `IdOrdine`, `P_IVA`, `Messaggio`, `Titolo`, `Letto`) VALUES
(1, 1, '00159560366', 'Buongiorno Ferrari, è stato creato l\'ordine numero: 1 da un cliente e lo ha già pagato. Il corriere è PRONTO PER LA SPEDIZIONE passerà a ritirare il veicolo nei prossimi giorni. Grazie.', 'Auto ordinata e pagata, il corriere passerà a ritirarla - Venditore', 0),
(2, 1, '00159560366', 'Buongiorno Ferrari, l\'ordine numero: 1 effettuato dal suo cliente risulta essere CONSEGNATO, per eventuali problemi non esiti a contattare l\'assistenza di Car Shop. Grazie.', 'Conferma consegna ordine ricevuto - Venditore', 0),
(3, 3, '07973780013', 'Buongiorno Fiat, è stato creato l\'ordine numero: 3 da un cliente e lo ha già pagato. Il corriere è PRONTO PER LA SPEDIZIONE passerà a ritirare il veicolo nei prossimi giorni. Grazie.', 'Auto ordinata e pagata, il corriere passerà a ritirarla - Venditore', 0),
(4, 2, '00591801204', 'Buongiorno Lamborghini, è stato creato l\'ordine numero: 2 da un cliente e lo ha già pagato. Il corriere è PRONTO PER LA SPEDIZIONE passerà a ritirare il veicolo nei prossimi giorni. Grazie.', 'Auto ordinata e pagata, il corriere passerà a ritirarla - Venditore', 0),
(5, 2, '03970540963', 'Buongiorno RollsRoyce, è stato creato l\'ordine numero: 2 da un cliente e lo ha già pagato. Il corriere è PRONTO PER LA SPEDIZIONE passerà a ritirare il veicolo nei prossimi giorni. Grazie.', 'Auto ordinata e pagata, il corriere passerà a ritirarla - Venditore', 0),
(6, 2, '07024150968', 'Buongiorno Tesla, è stato creato l\'ordine numero: 2 da un cliente e lo ha già pagato. Il corriere è PRONTO PER LA SPEDIZIONE passerà a ritirare il veicolo nei prossimi giorni. Grazie.', 'Auto ordinata e pagata, il corriere passerà a ritirarla - Venditore', 0);


-- parte integrativa

-- inserimento aste
INSERT INTO `venditore` (`P_IVA`, `Casa_Asta`, `Email`, `Telefono`, `Nome_Utente`, `Password`, `Salt`, `Link_logo`, `Link_ImmCopertina`, `Via`, `N_Civico`, `Citta`, `Provincia`, `CAP`) VALUES
("23608161206",1,"superdani.d@gmail.com","6984114979","PandolfiniAste", "f981c233338c2e84afff10949ff3c33d438aa2a7edc3cce561448984a4375ea7dd4fab7e619e5ad79a9abae7e095652381c628f43585ef20568d62de1cc6e795", "728b66d0d129b55c279fb119c6527e92f85690c5d9671a812706748cfa49998241ce734c29c07567fa6ddae45356dec768f4c7a29fad01c467e7bd6ac210eb90", "logoPandolfini.jpg", "pandolfiniCopertina.jpg", NULL, NULL, NULL, NULL, NULL);
/* password: PandolfiniAste123!! */

INSERT INTO `venditore` (`P_IVA`, `Casa_Asta`, `Email`, `Telefono`, `Nome_Utente`, `Password`, `Salt`, `Link_logo`, `Link_ImmCopertina`, `Via`, `N_Civico`, `Citta`, `Provincia`, `CAP`) VALUES
("70306540017",1,"superdani.d@gmail.com","5121209051","AdesaAste", "a00815c45edbf382c08e9ee7b81baf0a5bbcc3a0f901ef3539813664d4e1347799e3a70a35c19446bebf17690ebdb6a96b498e1e31df6fec523a7146160f7757", "bcc1b689514aefa8ad3176fd40ca3fe6af3d93185241165445c2368e4ad8573b9d9b8f46fd545dbffae143876c82e08e67a49f7346e27c2a6672c835dfdc86ce", "logoAdesa.png", "adesaCopertina.png", NULL, NULL, NULL, NULL, NULL);
/* password: AdesaAste123! */

INSERT INTO `venditore` (`P_IVA`, `Casa_Asta`, `Email`, `Telefono`, `Nome_Utente`, `Password`, `Salt`, `Link_logo`, `Link_ImmCopertina`, `Via`, `N_Civico`, `Citta`, `Provincia`, `CAP`) VALUES
("40167090337",1,"superdani.d@gmail.com","1835910575","AutoEpocaAste", "60be0b9681b03372fe67dfebec07d5d95ef980d2ff97611cfd386d61cb5e315d374483f911c8d23a8e0b13575446e34c6054830dcc7063f91fc89220194919f", "58341a3a66689cdbde8389c0b2e72e5866a35853b3aee91055097e13a8c535616988cc35b8a776528ed54413e2e136329c73d6486e5325883b70cfee89a9fa7d", "logoAutoEpoca.jpg", "copertinaAutoEpoca.jpg", NULL, NULL, NULL, NULL, NULL);
/* password: AutoEpocaAste123! */

INSERT INTO `venditore` (`P_IVA`, `Casa_Asta`, `Email`, `Telefono`, `Nome_Utente`, `Password`, `Salt`, `Link_logo`, `Link_ImmCopertina`, `Via`, `N_Civico`, `Citta`, `Provincia`, `CAP`) VALUES
("75561910177",1,"superdani.d@gmail.com","0690695862","SothebyAste", "4176aa01812d6b80e8e95780d170b9c57646259811f9081c9afdb358eba43e9369f8cd2ea3ec27092f7fd1f9417788e38990f32dd96e7351ccfba9014af47474", "a5897a980aa7e4f99201247f83176da5f9a5da7e21211a6d14965e463f2c15af99f2c15ebeef691f3ae2bb3eec8efbf40c47d35131d7e2d9e3128bb31ee3b7b4", "logoSotheby.jpg", "copertinaSotheby.jpg", NULL, NULL, NULL, NULL, NULL);
/* password: SothebyAste123! */

/*----------------------------------------------------------------------------------*/
/*PANDOLFINI*/
INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("23608161206", 20000, 1980, "BMW M3 E30", "Pandolfini_BmwM3_Imm1.jpg", "Pandolfini_BmwM3_Imm2.jpg", "Pandolfini_BmwM3_Imm3.jpg", "La sigla BMW E30 identifica la seconda generazione della Serie 3, un'autovettura di fascia medio-alta prodotta dal 1982 al 1992 dalla casa automobilistica tedesca BMW. Al suo debutto, la E30 era disponibile generalmente in quattro motorizzazioni, tutte a benzina: 316, 318i, 320i, 323i.", 20000, "Pandolfini_BmwM3_Sound.mp3", "Pandolfini_BmwM3_Video.mp4", 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("23608161206", 20000, 1980, "Datsun 280z", "Pandolfini_Datsun280z_Imm1.jpg", "Pandolfini_Datsun280z_Imm2.jpg", "Pandolfini_Datsun280z_Imm3.jpg", "Presentata nel 1969, era una coupé a due posti, dotata di MacPherson anteriore e posteriore derivato da vetture di serie. Il motore era derivato dalla 510 Sedan con l'aggiunta di due cilindri ottenendo così l'esuberante 6 cilindri in linea 2,4 litri SOHC da 150 cv.
Per le proporzioni della sua linea si era preso come riferimento la Ferrari 250 GTO, mentre per il lungo cofano l'ispirazione era arrivata dalla Jaguar E-Type.", 20000, "Pandolfini_Datsun280z_Sound.mp3", NULL, 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("23608161206", 50000, 1980, "Ferrari 250GTO", "ferrari1.jpg", "ferrari2.jpeg", "ferrari3.jpg", "La Ferrari 250 GTO è sia un'automobile stradale sia una da corsa prodotta dalla Ferrari agli inizi degli anni sessanta. Viene considerata la Ferrari per eccellenza ed è tuttora una delle automobili più conosciute ed apprezzate di tutti i tempi. Il numero, 250, sta per la cilindrata di ciascun cilindro in centimetri cubi del motore V12 3000 cc di cilindrata. GTO sta per Gran Turismo Omologata.", 50000, "Pandolfini_Ferrari250GTO_Sound.mp3", "Pandolfini_Ferrari250GTO_Video.mp4", 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("23608161206", 10000, 1960, "Porsche 911 Targa", "Pandolfini_Porsche911Targa_Imm1.jpg", "Pandolfini_Porsche911Targa_Imm2.jpg", "Pandolfini_Porsche911Targa_Imm3.jpg", "La Porsche 911 è un'autovettura sportiva prodotta dalla Porsche a partire dal 1963. Nel 1966 venne lanciata anche la 911 S che, grazie ad una serie di modifiche all'albero motore, profilo dei pistoni, valvole maggiorate, raggiungeva una potenza di 160 CV. Su tutti i modelli si passò a carburatori Weber. Esternamente la S si riconosceva per i cerchi in lega Fuchs. Sempre nel 1966 le coupé (standard e S) vennero affiancate dalle versioni Targa, con tetto rigido asportabile.", 10000, "Pandolfini_Porsche911_Sound.mp3", "Pandolfini_Porsche911_Video.mp4", 0, NULL);

/*ADESA*/
INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("70306540017", 40000, 1960, "Ford GT40", "Adesa_FordGT40_Imm1.jpg", "Adesa_FordGT40_Imm2.jpg", "Adesa_FordGT40_Imm3.jpg", "La Ford GT40 è una famosa vettura sportiva prodotta dalla Ford dal 1964 al 1969. Pensata come rivale della Ferrari nelle gare di durata, vinse per quattro volte di seguito, dal 1966 al 1969, la 24 Ore di Le Mans. La Ford GT è un omaggio a questa vettura. La nuova sportiva della Ford venne presentata all'Auto Show di New York del 1964: il modello iniziale era dotato di un V8 di 4,2 litri derivato dalla serie.", 40000, "Adesa_FordGT40_Sound.mp3", NULL, 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("70306540017", 60000, 1960, "Mercedes 300SL", "Adesa_Mercedes300SL_Imm1.jpg", "Adesa_Mercedes300SL_Imm2.jpg", "Adesa_Mercedes300SL_Imm3.jpg", "La sigla W198 identifica la Mercedes-Benz 300 SL, un'autovettura sportiva di lusso prodotta dal 1954 al 1957 come coupé (nella cui configurazione era nota anche con il soprannome Gullwing cioè Ali di Gabbiano) e dal 1957 al 1963 come roadster dalla Casa tedesca Mercedes-Benz. La 300 SL coupé, internazionalmente nota come Gullwing (cioè Ali di gabbiano, in Italia) per la caratteristica apertura delle due portiere incernierate sopra il tetto.", 60000,"Adesa_Mercedes300SL_Sound.mp3", NULL, 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("70306540017", 9000, 1960, "Nissan Skyline 2000GTR", "Adesa_Nissan2000GTR_Imm1.jpg", "Adesa_Nissan2000GTR_Imm2.jpg", "Adesa_Nissan2000GTR_Imm3.jpg", "La Nissan Skyline GT-R è un'autovettura prodotta dalla casa automobilistica nipponica Nissan a partire dal 1969 sino al 1973 e in seguito dal 1989 al 2002 quando è stata sostituita dalla Nissan GT-R. Fa parte della serie Skyline, nome utilizzato dalla Nissan sin dal 1957 e comprendente vari tipi di automobile di classe media. La prima serie in gran parte presentava l'impiego del propulsore S20 DOHC a 24 valvole gestito da un cambio a cinque rapporti.", 9000, "Adesa_Nissan2000GTR_Sound.mp3", "Adesa_Nissan2000GTR_Video.mp4", 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("70306540017", 12000, 1960, "Nissan Skyline GTR R34", "Adesa_NissanR34_Imm1.jpg", "Adesa_NissanR34_Imm2.jpg", "Adesa_NissanR34_Imm3.jpg", "La quinta generazione del modello, presentata nel gennaio del 1999, curiosamente presentava una diminuzione degli ingombri esterni, passati da una lunghezza di 4.6Il motore che equipaggia la R34, l'RB26DETT, sei cilindri in linea di 2568 cmcubici sovralimentato mediante l'ausilio di due turbocompressori, riusciva a erogare 340 CV con una coppia di 384 Nm.75 mm della R33 ai 4.600 attuali, aumentando di 5 millimetri in larghezza, dai 1.780 ai 1.785 mm.", 12000, "Adesa_NissanR34_Sound.mp3", "Adesa_NissanR34_Video.mp4", 0, NULL);

/*AutoEpoca*/
INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("40167090337", 60000, 1960, "AstonMartin DB6", "AutoEpoca_AstonMartinDB6_Imm1.jpg", "AutoEpoca_AstonMartinDB6_Imm2.jpg", "AutoEpoca_AstonMartinDB6_Imm3.jpg", "La Aston Martin DB6 è un'autovettura Gran Turismo prodotta dal 1965 al 1971 per sostituire il precedente modello DB5. La vettura era la naturale evoluzione della DB5 ma rispetto ad essa i cambiamenti furono importanti. Il passo venne allungato di diversi centimetri per poter ospitare altri 2 posti, di fortuna, e la coda venne totalmente ridisegnata. Nel febbraio del 1965 vennero svolti dei test in galleria del vento per valutare l'efficienza aerodinamica della DB5.", 60000, "AutoEpoca_AstonMartinDB6_Sound.mp3", "AutoEpoca_AstonMartinDB6_Video.mp4", 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("40167090337", 60000, 1960, "Bentley MkVI", "AutoEpoca_BentleyMkVI_Imm1.jpg", "AutoEpoca_BentleyMkVI_Imm2.jpg", "AutoEpoca_BentleyMkVI_Imm3.jpg", "La Mark VI è una autovettura costruita dalla Bentley dal 1946 al 1952. Il motore era il B60, figlio di quella famiglia di motori modulari a 4, 6 e 8 cilindri (B40-B60-B80) che erano nati per esigenze belliche, per la movimentazione di camion, anfibi e gruppi elettrogeni. Il vecchio 6 cilindri valvole in testa fu rimpiazzato da questa buona unità dotata però della sola valvola di aspirazione in testa e dello scarico laterale.", 60000, NULL, NULL, 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("40167090337", 20000, 1972, "Ford GranTorino", "AutoEpoca_FordGranTorino_Imm1.jpg", "AutoEpoca_FordGranTorino_Imm2.jpg", "AutoEpoca_FordGranTorino_Imm3.jpg", "La Ford Torino è una autovettura prodotta dalla Ford Motor Company tra il 1968 e il 1976 per il mercato nordamericano. Questo modello sostituì la Ford Fairlane, nome che comunque venne mantenuto e che identificava il modello base che aveva interni differenti dalla Torino. Il nome Torino deriva dal fatto che gli statunitensi consideravano questa città, sede della FIAT e della Lancia, come la Detroit d'Italia.", 20000, NULL, NULL, 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("40167090337", 10000, 1979, "Lancia Delta", "AutoEpoca_LanciaDelta_Imm1.jpg", "AutoEpoca_LanciaDelta_Imm2.jpg", "AutoEpoca_LanciaDelta_Imm3.jpg", "La prima serie della Delta nasce nel 1979 dalla matita di Giorgetto Giugiaro, ed è chiamata a raccogliere l'eredità della Beta. È una moderna due volumi ben proporzionata, dalle linee pulite e squadrate, che rompe con i precedenti canoni stilistici della casa. I successi sportivi conseguiti dalle sue versioni rally ne hanno fatto una vettura di successo e ne hanno allungato la carriera, che ha attraversato un intero decennio e si è conclusa solo nel 1993 con l'arrivo della seconda serie.", 10000, "AutoEpoca_LanciaDelta_Sound.mp3", "AutoEpoca_LanciaDelta_Video.mp4", 0, NULL);

/*SOTHEBY*/
INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("75561910177", 2000, 1948, "Citroen 2CV", "Sotheby_Citroen2CV_Imm1.jpg", "Sotheby_Citroen2CV_Imm2.png", "Sotheby_Citroen2CV_Imm3.jpg", "La Citroen 2 CV (deux chevaux - francese, letteralmente due cavalli, dalla valutazione dei cavalli fiscali in Francia) è un'autovettura utilitaria prodotta dalla Casa automobilistica francese Citroën dal 1948 al 27 luglio 1990 e nota in Francia come Dodoche.  La 2CV rappresentò la principale attrazione della kermesse, suscitando grande interesse nel pubblico che si accalcava allo stand Citroen.", 2000, NULL, NULL, 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("75561910177", 20000, 1960, "Jaguar E-Type", "Sotheby_JaguarEtype_Imm1.jpg", "Sotheby_JaguarEtype_Imm2.jpg", "Sotheby_JaguarEtype_Imm3.jpg", "La Jaguar E-Type, conosciuta anche come XK-E o anche XKE (anche se era questo il suo nome iniziale), fu prodotta dalla casa automobilistica britannica Jaguar dal 1961 al 1975. La E-Type fu una vettura rivoluzionaria per la progettazione, le caratteristiche di guida e l'estetica; era in anticipo sui tempi. Il suo prezzo era più basso di quello delle vetture pari classe della concorrenza e questo aiutò le vendite che, nei 14 anni nei quali rimase in produzione, arrivarono a 70.000 vetture.", 20000, NULL, NULL, 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("75561910177", 2000, 1944, "Jeep CJ5", "Sotheby_JeepCJ5_Imm1.jpg", "Sotheby_JeepCJ5_Imm2.jpg", "Sotheby_JeepCJ5_Imm3.jpg", "La Jeep CJ (Civilian Jeep) è una vettura fuoristrada costruita dalla Willys-Overland, marchio successivamente diventato Jeep, dal 1944 al 1986. È nata come versione commerciale della MB, veicolo utilizzato largamente dall'esercito americano durante la seconda guerra mondiale. Con una lenta evoluzione è rimasta in produzione in diverse varianti e con vari allestimenti, tra cui si ricordano la Renegade, Laredo e Golden Eagle.", 2000, NULL, NULL, 0, NULL);

INSERT INTO `auto_in_asta`(`P_IVA`, `Prezzo_iniziale`, `Anno_Immatricolazione`, `Modello`, `Link_Immagine1`, `Link_Immagine2`, `Link_Immagine3`, `Descrizione`, `prezzoAttuale`, `Suono`, `Link_video`, `Venduto`, `Vincitore`) VALUES 
("75561910177", 20000, 1964, "Shelby Cobra", "Sotheby_ShelbyCobra_Imm1.jpg", "Sotheby_ShelbyCobra_Imm2.jpg", "Sotheby_ShelbyCobra_Imm3.png", "La Cobra è un'autovettura sportiva roadster prodotta dalla AC Cars e dalla Shelby Automobiles dal 1962. Nel 1964, per tentare di opporsi allo strapotere della Ferrari, venne montato sulla Cobra un nuovo motore di grossa cilindrata, il Ford type 427 Side Oiler da 7 litri, sviluppato originariamente per le gare NASCAR. Rispetto al precedente motore 289 che erogava 350 CV, il Side Oiler ne erogava 500.", 20000, "Sotheby_ShelbyCobra_Sound.mp3", "Sotheby_ShelbyCobra_Video.mp4", 0, NULL);


-- agginta venditori e auto dedicate
/***************************************************************/
/*Porsche123!*/
INSERT INTO `venditore`(`P_IVA`, `Casa_Asta`, `Email`, `Telefono`, `Nome_Utente`, `Password`, `Salt`, `Link_logo`, `Link_ImmCopertina`, `Via`, `N_Civico`, `Citta`, `Provincia`, `CAP`) VALUES 
("49291620760", 0, "superdani.d@gmail.com", "5149762192", "Porsche", "61531e81a3a57cb5d27fe84fb379d4c5684facc76583f8d3c059ed65bd064b1bc1a63ce13f4b46360861dfed1eae3403d7ea3534817f78ed8d96e230dc8346c1", "b5602f02259405486b198dc7ac0450c8f2532baa7045bfcb17dc7fd4a0828d3ec34c59e27c07a4ca3902742d40e36c6d6a847a29a775e4b145ed5179cda4f4d9", "LogoPorsche.png", "Porsche_Taycan_Copertina.jpeg", "Via Gelasio Adamoli", 347, "Genova", "Genova", 16138);

INSERT INTO `auto`(`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) 
VALUES (35, "Taycan", 200000, "Porsche_Taycan_Imm1.jpg", "La nuova Porsche Taycan porta l'elettrico verso una nuova rivoluzione: la velocità raggiungerà nuovi confini", NULL, NULL);

INSERT INTO `gestisce`(`IdAuto`, `P_IVA`) VALUES (35,"49291620760");

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("La Porsche Taycan è una vettura costruita a partire dal 2019 dalla casa automobilistica tedesca Porsche. Presentata per la prima volta al salone dell'automobile di Francoforte del settembre 2019, è la prima vettura elettrica ad avere la ricarica in corrente continua da 270 kW a 800 Volt.","Porsche_Taycan_Imm2.jpg",35);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Il nome Taycan è una parola macedonia turca composta da tay e can (dove la prima significa giovane cavallo e la seconda anima e vita), che si traduce in spirito di un cavallo vivace; il nome è un richiamo alla cavallina simbolo del marchio Porsche.","Porsche_Taycan_Imm4.jpg",35);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("All'interno sono presenti quattro display digitali, tra cui uno da 16,8 pollici curvo posto dietro il volante davanti al guidatore che funge da quadro strumento. Un secondo schermo da 10,9 pollici è posto a destra del quadro strumenti, che svolge la funzione di sistema dell'infotainment.","Porsche_Taycan_Imm3.jpeg",35);

INSERT INTO `esterni`(`IdAuto`, `Nome`, `Descrizione`, `Link_Immagine`, `Prezzo`) VALUES 
(35, "Cerchio a razze strette", "cerchio", "Porsche_cerchio1.png", 5000);

INSERT INTO `esterni`(`IdAuto`, `Nome`, `Descrizione`, `Link_Immagine`, `Prezzo`) VALUES 
(35, "Cerchio a razze larghe", "cerchio", "Porsche_cerchio2.png", 5000);

INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(35,"Motore elettrico con 560KW","Motore elettrico 370km di autonomia",10000,NULL,530);

INSERT INTO `optional`(`IdAuto`, `Nome`, `Descrizione`, `Link_Immagine`, `Prezzo`) VALUES 
(35,"Mappature motore","Disponibilit 3 mappature per erogazione potenza: EcoDrive, Sport, Comfort",NULL,5000);

INSERT INTO `auto`(`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) VALUES 
(36,"918",900000,"Porsche_918_Imm1.jpg","Auto che emana il senso di velocità, di sportività, di leggerezza e al contempo stesso di equilibrio con la natura","Porsche_918_Sound.mp3",NULL);

INSERT INTO `gestisce`(`IdAuto`, `P_IVA`) VALUES (36,"49291620760");

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES 
("La Porsche 918 Spyder è un'automobile a motore centrale progettata e costruita da Porsche, presentata al pubblico come concept car in occasione del salone dell'automobile di Ginevra nel marzo 2010 e, in veste definitiva, nel 2013 ad Atlanta", "Porsche_918_Imm4.jpg", 36);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES 
("La 918 è una vettura ibrida a trazione integrale, combina la spinta fornita da un motore V8 endotermico collocato in posizione posteriore centrale e di 2 motori elettrici posti rispettivamente uno sull'asse anteriore e uno sull'asse posteriore. Il motore V8 a ciclo Otto è uno sviluppo del propulsore da corsa utilizzato sullo Sport Prototipo Porsche RS Spyder di classe LMP2.
", "Porsche_918_Imm2.jpg", 36);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES 
("Il motore elettrico posteriore garantisce 115 kW (156 CV), mentre quello sull'asse anteriore da 95 kW (129 CV), offrendo quindi altri 210 kW (285 CV), per una potenza totale in combinata di 893 CV e di 1275 Nm di coppia massima.
", "Porsche_918_Imm3.jpg", 36);

/***************************************************************/
/*Nissan123!*/
INSERT INTO `venditore`(`P_IVA`, `Casa_Asta`, `Email`, `Telefono`, `Nome_Utente`, `Password`, `Salt`, `Link_logo`, `Link_ImmCopertina`, `Via`, `N_Civico`, `Citta`, `Provincia`, `CAP`) VALUES 
("58778860443", 0, "superdani.d@gmail.com", "2250567164", "Nissan", "6b68e3c35c87469440030bce81e6c45fe63c8f293bfaea9a25468c7a64ae987c81f1f7769885244d69422253cf215341e944a15ad4333a887ab7becc373ebacb", "2d2247701ced07ecadb0032d3aa8cd9903a4555d51ad8c01930fc58a10619d93e4f58f3f0ac5a8bf373645d8eace52bb27d46a7fb4d156cae4f39d6823ac842c", "LogoNissan.png", "Nissan_Copertina.jpg", "Viale Italia", 24, "Rimini", "Rimini", 47900);

INSERT INTO `auto`(`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) 
VALUES (37, "GTR", 100000, "Nissan_GTR_Imm1.jpg", "Questa macchina è l'essenza della brutalità, della potenza, della cattiveria. Il Mostro è arrivato", "Nissan_GTR_Sound.mp3", "Nissan_GTR_video.mp4");

INSERT INTO `gestisce`(`IdAuto`, `P_IVA`) VALUES (37,"58778860443");

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("La Nissan GT-R (R35) è una vettura coupé sportiva progettata da Nissan Motor, erede della Skyline GT-R (R34). Nel 2001 Nissan iniziò lo sviluppo della nuova generazione di GT-R presentando a Tokyo il primo concept di quella che sarebbe stata la futura auto giapponese di serie più potente mai costruita. Nel 2005 viene esposto il secondo prototipo, e due anni dopo presentata la versione pronta alla commercializzazione.","Nissan_GTR_Imm2.jpg",37);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("La vettura è spinta da un motore V6, montato in posizione anteriore longitudinale, il monoblocco e le testate sono realizzate in alluminio, ha una cilindrata di 3.799 cm cubici, dispone di accorgimenti quali il rivestimento al plasma dei cilindri, la distribuzione è a 4 valvole per cilindro azionate da doppio albero a camme per bancata, è sovralimentato da due turbocompressori IHI, eroga una potenza massima di 480 CV. L'intero blocco motore è assemblato a mano.","Nissan_GTR_Imm3.jpg",37);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Una menzione speciale va al computer di bordo che presenta un ampio schermo, chiamato Multi Function Display. La grafica è stata curata dalla Polyphony Digital, la stessa creatrice della saga Gran Turismo. Si possono visualizzare 11 schermate, 4 impostabili dal conducente.","Nissan_GTR_Imm4.jpg",37);

INSERT INTO `esterni`(`IdAuto`, `Nome`, `Descrizione`, `Link_Immagine`, `Prezzo`) VALUES 
(37, "Ammortizzatori Bilstein DampTronic ottimizzati per NISMO", "ammortizzatori", NULL, 5000);

INSERT INTO `interni`(`IdAuto`, `Nome`, `Descrizione`, `Link_Immagine`, `Prezzo`) VALUES 
(37, "Sedili in pelle rossa", "sedili", "Nissan_GTR_SediliPelle.jpg", 300);

INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(37,"Motore biturbo","Motore 3.8 V6 biturbo a 24 valvole",0,3800,570);

/*------------------------------------------------------*/

INSERT INTO `auto`(`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) 
VALUES (38, "370Z", 30000, "Nissan_370z.jpg", "La 370Z è l'auto perfetta per la città senza rinunciare a divertimento e sportività", NULL, NULL);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Il motore 3.7 V6 denominato VQ37VHR è totalmente nuovo e in alluminio, leggermente più leggero della precedente unità 3.5, sfrutta la tecnologia VVEL a doppia fasatura variabile delle valvole, la coppia è pari a 370 Nm a 5200 giri al minuto e dispone di 331 cavalli (243 kW) che permettono alla 370Z coupé di toccare i 250 km/h, per un'accelerazione da 0-100 km/h in 5,7 secondi, e di emettere 249 grammi di anidride carbonica ogni chilometro nel ciclo misto. Il consumo è pari a 9,5 km/l nel ciclo medio. Lo scarico possiede uno schema a Y.","Nissan_370z_Imm1.jpg",38);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Il design è un'evoluzione della precedente 350Z leggermente più arrotondato e caratterizzato da inedite nervature nelle fiancate e nel frontale; sono stati completamente ridisegnati i fanali anteriori e posteriori che possiedono entrambi il medesimo stile a boomerang e tecnologia luminosa a Led.
","Nissan_370z_Imm2.jpg",38);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("La coupé viene proposta in Italia in due allestimenti: Lev 1 e Lev 2. La dotazione di entrambe le versioni comprende il clima manuale, il controllo elettronico di stabilità, l'autoradio CD e MP3 con ingresso Aux, i fari bixeno, i poggiatesta attivi, controllo della trazione e ruotino di scorta oltre ai cerchi in lega da 18.
","Nissan_370z_Imm3.jpg",38);

INSERT INTO `gestisce`(`IdAuto`, `P_IVA`) VALUES (38,"58778860443");


/************/

INSERT INTO `auto`(`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) 
VALUES (39, "SLS AMG", 180000, "Mercedes_SLS_Copertina.jpg", "La nuova SLS AMG ricorda la stupenda Mercedes Gull Wing: il fascino rimane", "Mercedes_SLS_Sound.mp3", NULL);

INSERT INTO `gestisce`(`IdAuto`, `P_IVA`) VALUES (39,"06325761002");

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Esteticamente la SLS AMG presenta dei richiami stilistici alla progenitrice di oltre 55 anni prima, e non solo per l'affascinante soluzione delle portiere ad ali di gabbiano incernierate sul tetto proprio come nella vecchia 300 SL, ma più in generale per le forme e le proporzioni dell'intero corpo vettura, con il lungo cofano anteriore, la grande calandra tagliata in due da una barra orizzontale con il grande logo della Casa nel mezzo, l'abitacolo fortemente arretrato, i due grandi sfoghi d'aria laterali che si aprono subito dietro il parafango anteriore, la coda corta e tondeggiante.","Mercedes_SLS_Imm1.jpg",39);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Il propulsore utilizzato dalla SLS AMG è il motore M159, un'evoluzione del V8 M156 già utilizzato su altri modelli ad alte prestazioni della Casa. Si tratta di un 6,2 litri di cilindrata (anche se la Casa costruttrice e la stampa dichiarano sempre che si tratta di un 6,3 litri).","Mercedes_SLS_Imm2.jpg",39);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("L'elevato prezzo di listino della SLS AMG comprende un equipaggiamento di prim'ordine, la vettura dispone di un unico livello di allestimento, propone nella dotazione di serie: ABS, ESP, ASR, BAS, cinture di sicurezza a tre punti, otto airbag, Hill Holder, climatizzatore automatico, cielo in alcantara, fari bi-xeno autorientabili, tergilavafari, vetri elettrici, avviamento Keyless-Go, interni in pelle ed alluminio, pedaliera sportiva in acciaio traforato, sistema multimediale COMAND APS con navigatore, lettore CD/DVD e sistema di comando vocale Voice Tronic, computer di bordo.","Mercedes_SLS_Imm3.jpg",39);

/****************************/
INSERT INTO `auto`(`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) 
VALUES (40, "Portofino", 140000, "Ferrari_Portofino_Copertina.jpg", "Con la Portofino, porti l'eleganza e la velocità dove vuoi", NULL, NULL);

INSERT INTO `gestisce`(`IdAuto`, `P_IVA`) VALUES (40,"00159560366");

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Le vetture gran turismo in Ferrari, come già avvenuto con la California, vengono chiamate con un nome piuttosto che con una sigla. La Portofino prende il nome di una delle località più suggestive della costa ligure, che raccoglie i principi di eleganza, piacere e bellezza tipici dei luoghi di villeggiatura italiani","Ferrari_Portofino_Imm1.jpg",40);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Il motore è il V8 biturbo Ferrari F154 BE da 3855 cm cubici disposto longitudinalmente all'anteriore, che fornisce una potenza di 600 CV (441 kW) a 7500 giri/min, equivalenti a 156 CV/l. Gli ingegneri sono riusciti ad ottenere un incremento di 40 cavalli rispetto alla California T grazie all'introduzione di nuovi componenti meccanici specifici e a una taratura dedicata dei software di gestione motore.","Ferrari_Portofino_Imm2.jpg",40);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("L'interno è pensato per essere molto semplice e puro, la bellezza artigianale tipica degli interni di Ferrari è associata ad un'interfaccia molto evoluta ereditata dalla GTC4 Lusso. La dotazione di sistemi di controllo HMI (acronimo di Human Machine Interface) della Portofino prevede un quadro strumenti dotato di doppio display TFT con al centro l'immancabile contagiri analogico in asse con il volante multifunzione dotato del manettino che, su questa Ferrari, ha tre posizioni.","Ferrari_Portofino_Imm3.jpg",40);

/***************************/
INSERT INTO `auto`(`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) 
VALUES (41, "Urus", 200000, "Lamborghini_Urus_Copertina.jpg", "Il primissimo SUV marchiato Lamborghini, per un auto adatta ad ogni terreno (e anche alla pista)", NULL, NULL);

INSERT INTO `gestisce`(`IdAuto`, `P_IVA`) VALUES (41,"00591801204");

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Il nome, come da tradizione Lamborghini, deriva dal mondo delle corride: l'uro (Bos taurus primigenius) era una specie bovina selvaggia europea, estintasi nel tardo medioevo, ritenuta all'origine degli attuali bovini domestici e nota anche col nome di aurochs.","Lamborghini_Urus_Imm1.jpg",41);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Il motore della Urus è un V8 4.0 litri biturbo derivato da quello dell'Audi RS6: sarà il primo motore turbo della Lamborghini e sarà in grado di sviluppare una potenza di 650 CV e una coppia di 850 Nm.","Lamborghini_Urus_Imm2.jpg",41);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Per contenere il peso della vettura, gli ingegneri della casa automobilistica hanno dovuto lavorare sui materiali: sia nella carrozzeria che nell'abitacolo della vettura, nel quale possono trovare spazio fino a cinque passeggeri, si fa largo uso delle fibre di carbonio.","Lamborghini_Urus_Imm3.jpg",41);

/**************************/
INSERT INTO `auto`(`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) 
VALUES (42, "Bentayga", 300000, "Bentley_Bentayga_Copertina.jpg", "Porta l'eleganza agli estremi dell'avventura con la nuovissima Bentayga", NULL, "Bentley_Bentayga_Video.mp4");

INSERT INTO `gestisce`(`IdAuto`, `P_IVA`) VALUES (42,"02751850047");

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("La Bentley Bentayga è un SUV di lusso prodotto dalla casa automobilistica inglese Bentley a partire dal 2016. Senza contare la fuoriserie Dominator del 1996, si tratta del primo SUV realizzato dalla casa britannica, e prende il nome da una particolare formazione rocciosa nelle Isole Canarie, la Roque Bentayga.","Bentley_Bentayga_Imm1.jpg",42);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Il telaio si basa sulla piattaforma del gruppo Volkswagen chiamata MLB, la stessa piattaforma utilizzata nella seconda generazione della Audi Q7 e per la futura terza generazione di Porsche Cayenne e Volkswagen Touareg ma l'80% dei suoi componenti sono unici e esclusivi realizzati appositamente per questo modello.","Bentley_Bentayga_Imm2.jpg",42);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Monta un motore 6.0 litri 12 cilindri a W, sovralimentato da due turbocompressori, capace di 608 CV e 900 Nm di coppia che spingono l'auto da 0 a 100 km/h in 3,9 secondi e fino a 301 km/h di punta; al momento dell'uscita è stato dichiarato come il SUV più veloce in produzione, nonché il SUV più costoso sul mercato.","Bentley_Bentayga_Imm3.jpg",42);

/****************************/
INSERT INTO `auto`(`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) 
VALUES (43, "Model S", 100000, "Tesla_ModelS_Copertina.jpg", "La modernissima e futuristica auto marchiata Tesla è arrivata, lasciato proiettare in un nuovo futuro con la Model S", NULL, NULL);

INSERT INTO `gestisce`(`IdAuto`, `P_IVA`) VALUES (43,"07024150968");

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("È la prima berlina elettrica di lusso della storia a essere prodotta in serie. Nel corso degli anni è stata proposta con batterie di varie capienze (da 40 a 100 kWh) e di conseguenza con differenti livelli di autonomia (da 224 a 629 km, secondo lo standard statunitense EPA).","Tesla_ModelS_Imm1.jpg",43);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("L'idea della Model S nasce dalla volontà dell'azienda di creare la prima berlina con motore elettrico alimentata esclusivamente tramite batterie ricaricabili destinata a una produzione in serie. Ciò ha reso progettisti e designer liberi dai vincoli imposti da motore termico, organi di trasmissione e di scarico nel loro lavoro.","Tesla_ModelS_Imm2.jpg",43);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("La Model S è dotata del sistema di guida assistita di livello 3 denominato Autopilot, che consente al mezzo di operare in alcune circostanze senza l'ausilio del guidatore, grazie alla componentistica hardware di 1 radar, 8 telecamere e 12 sensori a ultrasuoni.","Tesla_ModelS_Imm3.jpg",43);

/*********************/

INSERT INTO `auto`(`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) 
VALUES (44, "Phantom", 500000, "RR_Phantom_Copertina.jpg", "Signori e signore, vi presento l'auto per eccellenza del Gentleman!
", NULL, NULL);

INSERT INTO `gestisce`(`IdAuto`, `P_IVA`) VALUES (44,"03970540963");

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Anche se un 15% dei componenti è condiviso con la BMW Serie 7 l'auto, a partire dal nome, mantiene il tradizionale stile e carattere della casa britannica, poiché l'idea di fondo era di creare un prodotto che comunque non somigliasse ai modelli BMW.","RR_Phantom_Imm1.jpg",44);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("La scocca d'alluminio è costruita in uno stabilimento a Dingolfing, in Germania, mentre il motore V12 viene fabbricato a Monaco di Baviera. L'assemblaggio finale, effettuato integralmente a mano, si svolge a Goodwood, nel West Sussex.","RR_Phantom_Imm2.jpg",44);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Le portiere posteriori sono ad apertura inversa, un design chiamato dal marchio coach doors. Pulsanti sui montanti posteriori permettono di chiudere automaticamente gli sportelli.","RR_Phantom_Imm3.jpg",44);

/*****************************/

INSERT INTO `auto`(`IdAuto`, `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`, `Suono`, `Link_video`) 
VALUES (45, "124 Spyder", 30000, "Fiat_124_Copertina.jpg", "Scatta tra le strade di montagna con maestria (e con il tettuccio aperto) con la nuova Fiat 124 Spyder", NULL, NULL);

INSERT INTO `gestisce`(`IdAuto`, `P_IVA`) VALUES (45,"07973780013");

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Il nome della vettura e alcuni dettagli richiamano il design esterno della Fiat 124 Sport Spider del 1966 disegnata da Pininfarina. Essa riporta la FIAT nel mercato delle spider a 10 anni di distanza dalla fine della produzione della Barchetta.","Fiat_124_Imm1.jpg",45);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("Gli interni vengono condivisi con la Mazda MX-5; la plancia è sottile, con inserti in nero lucido o argento, con un ampio cruscotto; sempre nella plancia troviamo il sistema infotainment con schermo touch screen da 7 pollici e navigatore 3D, lettore MP3, due prese USB e un ingresso AUX, connessione Bluetooth e telecamera posteriore.
","Fiat_124_Imm2.jpg",45);

INSERT INTO `immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) VALUES ("La 124 Spider è spinta da un motore Fiat di 1,4 litri MultiAir turbo. Per il mercato europeo il motore ha una potenza di 140 CV e 240 Nm di coppia, mentre la versione per i mercati del Nord America produce 160 CV con 249 Nm di coppia.","Fiat_124_Imm3.jpg",45);

/************************************/
INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(38,"Motore VQ37VHR","3.7L V6 VVEL con 370Nm",0,"3499","331");

INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(39,"Motore M159","6.2L V8 turbo",0,"5999","631");

INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(40,"Motore F154BE","3.9L V8 biturbo fino a 7500 giri/min",0,"3799","600");

INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(41,"Motore AudiRS6","4.0L V8 biturbo 850Nm",0,"3999","650");

INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(42,"Motore Bentayga V","4.0 V8 biturbo 900Nm", 0,"3999","542");

INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(42,"Motore Bentayga W","6.0L W12 biturbo 900Nm",10000,"5499","608");

INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(43,"Motore DualMotor","Motore elettrico 796CV",0,NULL,"796");

INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(44,"Motore BMW-N73","6.7L V12",0,"5999","563");

INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(45,"Motore MultiAir Turbo","1.4L 240Nm benzina",0,"1599","140");

INSERT INTO `motore`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) VALUES 
(36,"Motore P918","V8 endotermico, 2 elettici",0,"4600","893");


/**********************Rilanci***********************/
INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(20500,"PSLGLC99T21H294J",1);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(20550,"PSLGLC99T21H294J",1);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(20600,"PSLGLC99T21H294J",1);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(20800,"PSLGLC99T21H294J",1);


INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(20300,"PSLGLC99T21H294J",2);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(20600,"PSLGLC99T21H294J",2);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(20680,"PSLGLC99T21H294J",2);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(20700,"PSLGLC99T21H294J",2);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(50300,"PSLGLC99T21H294J",3);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(50500,"PSLGLC99T21H294J",3);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(50560,"PSLGLC99T21H294J",3);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(50590,"PSLGLC99T21H294J",3);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(10100,"PSLGLC99T21H294J",4);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(10180,"PSLGLC99T21H294J",4);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(10200,"PSLGLC99T21H294J",4);

INSERT INTO `rilancio`(`Importo`, `CF`, `IdAsta`) VALUES 
(10300,"PSLGLC99T21H294J",4);

UPDATE `auto_in_asta` SET `Venduto`=1, `prezzoAttuale`=20800,`Vincitore`="PSLGLC99T21H294J" WHERE IdAsta = 1;
UPDATE `auto_in_asta` SET `Venduto`=1, `prezzoAttuale`=20700,`Vincitore`="PSLGLC99T21H294J" WHERE IdAsta = 2;
UPDATE `auto_in_asta` SET `Venduto`=1, `prezzoAttuale`=50590,`Vincitore`="PSLGLC99T21H294J" WHERE IdAsta = 3;
UPDATE `auto_in_asta` SET `Venduto`=1, `prezzoAttuale`=10300,`Vincitore`="PSLGLC99T21H294J" WHERE IdAsta = 4;


/* ----------------------------------------------------------- */

-- Recensioni
INSERT INTO `commento` (`IdAuto`, `CF`, `NumStelline`, `Testo`) VALUES
(1, 'MRRFNC90S27F839O', 5, 'Beh fare una recensione di una Ferrari mi pare superfluo. è semplicemente perfetta.'),
(1, 'PSLGLC99T21H294J', 4, 'Ovviamente essendo una Ferrari avevo veramente molte pretese prima dell\'acquisto, devo ammettere che le ha rispettate tutte. Unico difetto è la spedizione un po lenta.'),
(2, 'MRRFNC90S27F839O', 5, 'Auto super attesa!!!. da comprare!!!'),
(2, 'PSCLNS70M10H501C', 4, 'Nutro veramente molte aspettative da questa auto, sopratutto da TESLA. Piccolo probelma è che ci sarà ancora abbastanza da aspoettare per le prime consegne.'),
(3, 'LGHLCU59C55F205B', 5, 'Devo dire che questa auto è molto accessibile e rispetta tutti i miei criteri, mi ritengo molto soddisfatta.'),
(3, 'MRRFNC90S27F839O', 1, 'Auto a buon prezzo, ma devo dire finiture pessime e non prestante. Sono deluso.'),
(3, 'PSLGLC99T21H294J', 2, 'Auto senza infamia e senza lode. Riesce ad essere maneggevole, ma non è comoda e bella.'),
(4, 'PSCLNS70M10H501C', 2, 'Questa auto mi ha deluso. Non rispetta tutti i canoni ideali di auto e a questo prezzo preferisco spendere qualcosa di più e optare sulla Rolls Royce che è decisamente migliore.'),
(4, 'PSLGLC99T21H294J', 4, 'Auto che lascia il segno. Quando passerete con questa sicuramente tutti si gireranno a guardarla. La versione con v12 è veramente potente e con ottima accelerazione per il prezzo.'),
(5, 'LGHLCU59C55F205B', 5, 'Nonostante il prezzo alto non posso trovare aspetti negativi a questa auto. Era da tempo che cercavo un auto sportiva che potesse soddisfarmi.'),
(5, 'PSCLNS70M10H501C', 4, 'Mi pare veramente bellissima: per me è la meglio riuscita esteticamente tra le varie concorrenti. Invece purtroppo non posso sapere se vada meglio o peggio delle rivali...'),
(5, 'PSLGLC99T21H294J', 4, 'beh che dire è una Lamborghini. Vorrei sottolineare che è un modello di punta e con assetto corsa!!'),
(6, 'PSCLNS70M10H501C', 4, 'Auto veramente importante e di lusso. A mio parere il prezzo risulta un po eccessivo.'),
(6, 'PSLGLC99T21H294J', 5, 'Nonostante il prezzo sia molto elevato non ho riscontrato alcun difetto a questa auto. è semplicemente perfetta e molto comoda.'),
(7, 'LGHLCU59C55F205B', 4, 'Auto molto interessante per qualità prezzo. è tedesca quindi è piena di optional. La consiglio.'),
(7, 'MRRFNC90S27F839O', 3, 'Auto molto costosa, ma non rispecchia totalmente il prezzo perchè ha dei difetti. L\'auto risulta difficile da manovrare. Il costo è eccessivo.'),
(7, 'PSCLNS70M10H501C', 5, 'Sicuramente l\'auto più promettente del mercato ha linee filanti ed è molto comoda. Ottima auto di rappresentanza.'),
(7, 'PSLGLC99T21H294J', 4, 'Questa auto è definibile in un solo modo: Rappresentanza. Questa auto soddsfa tutti i livelli di comfort interni ma non è molto aggressiva esternamente.'),
(35, 'PSCLNS70M10H501C', 4, 'Sicuramente auto interessante dovrei approfondire meglio la sostenibilità.'),
(35, 'PSLGLC99T21H294J', 5, 'Decisamente macchina molto raffinata, potenze e soprattutto elettrica!! Compimenti la consiglio a tutti.'),
(36, 'PSLGLC99T21H294J', 4, 'Altro stile di macchina sempre molto bella e prestante, ma i consumi salgono alle stelle.'),
(37, 'PSLGLC99T21H294J', 4, 'Beh che dire una delle auto sportive di fascia medio-alta più belle in assoluto.'),
(38, 'MRRFNC90S27F839O', 2, 'sicuramente auto sportiva economica, ma non molto prestante e con ottime performance'),
(38, 'PSLGLC99T21H294J', 3, 'Ottima macchina sportiva a basso prezzo. Però ha alcuni difetti come potenza ed estetica.'),
(40, 'PSLGLC99T21H294J', 5, 'Sono un appassionato del marchio Ferrari e posso affermare con assoluta certezza che questo è uno dei modelli meglio riuscito. Vi consiglio di comprarla è sicuramente divertente e grintosa.');

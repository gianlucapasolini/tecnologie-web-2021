<?php
require_once 'gianluca_bootstrap.php';

if(isUserLoggedIn() && $_SESSION["tipo"] == "CLIENTE"){
	if(isset($_POST["listSpedizioni"]) && isset($_POST["via"]) && isset($_POST["N_Civico"]) && isset($_POST["Citta"]) && isset($_POST["Provincia"]) && isset($_POST["CAP"]) && isset($_POST["submit"])){
		if($_POST["submit"] == "Aggiungi"){
			//aggiungi
			$result_insert = $dbh_gianluca->insertSpedizione($_SESSION["CF"], $_POST["via"], $_POST["N_Civico"], $_POST["Citta"], $_POST["Provincia"], $_POST["CAP"]);
			if($result_insert==""){
	            $templateParams["erroreSpedizione"] = "Aggiunto correttamente";
	        }
	        else{
	            $templateParams["erroreSpedizione"] = "Errore in aggiunta!";
	        }
		} elseif ($_POST["submit"] == "Aggiorna") {
			//aggiorna
			$result_update = $dbh_gianluca->updateSpedizione($_POST["listSpedizioni"], $_POST["via"], $_POST["N_Civico"], $_POST["Citta"], $_POST["Provincia"], $_POST["CAP"]);
			if($result_update){
	            $templateParams["erroreSpedizione"] = "Aggiornamento Effettuato";
	        }
	        else{
	            $templateParams["erroreSpedizione"] = "Errore Aggiornamento!";
	        }
		}
	}

	$templateParams["titolo"] = "Car Shop - Spedizioni";
	$templateParams["titoloPagina"] = "Spedizioni";
	$templateParams["nome"] = "form_spedizione.php";
	$templateParams["js"][0] = "./js/spedizione.js";
	$templateParams["css"][0] = "./css/gianluca_style.css";
	$templateParams["spedizioni"] = $dbh_gianluca->getMySpedizioni($_SESSION["CF"]);
	if(isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "CLIENTE"){
		$templateParams["numNotifiche"] = $dbh_gianluca->getNumeroNotifiche($_SESSION["CF"]);
	}
}
else{
    header("location: login.php");
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>
<?php
require_once 'gianluca_bootstrap.php';

if(isset($_POST["email"]) && isset($_POST["nomeutente"])){
    if(isset($_POST["cf"]) && $_POST["cf"] != ""){
	    $resultFakeLogin = $dbh_gianluca->loginToChangePassword($_POST["options_utente"], $_POST["cf"], $_POST["email"], $_POST["nomeutente"]);
	} else if(isset($_POST["P_IVA"]) && $_POST["P_IVA"] != ""){
		$resultFakeLogin = $dbh_gianluca->loginToChangePassword($_POST["options_utente"], $_POST["P_IVA"], $_POST["email"], $_POST["nomeutente"]);
	}
	if(count($resultFakeLogin) != 0){
		$resultFakeLogin[0]["tipo"] = strtoupper($_POST["options_utente"]);
	    registerTemporaryUserChangePass($resultFakeLogin[0]);
	    header("location: nuovaPassword.php");
	} else {
		$templateParams["errorePassPersa"] = "Utente non trovato!";
	}
}

if(isUserLoggedIn()){
    header("location: nuovaPassword.php");
}
else{
    $templateParams["titolo"] = "Car Shop - Password Persa";
	$templateParams["titoloPagina"] = "Password Persa";
	$templateParams["nome"] = "passwordPersa_form.php";
	$templateParams["js"][0] = "./js/passwordPersa.js";
	$templateParams["css"][0] = "./css/gianluca_style.css";
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>
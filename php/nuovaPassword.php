<?php
require_once 'gianluca_bootstrap.php';

if(isUserLoggedIn()){
	if(isset($_POST["password"]) && isset($_POST["Conf_password"]) && $_POST["password"] === $_POST["Conf_password"]){
		$salt = hash('sha3-512', uniqid(mt_rand(1, mt_getrandmax()), true));
		$SaltPassword="";
		for ($i=0; $i < 128; $i++) { 
			$SaltPassword.=$salt[$i].$_POST["password"][$i];
		}
		$password = hash("sha3-512", $SaltPassword);
        $err = $dbh_gianluca->updatePassword($_SESSION["tipo"], $_SESSION["Nome_Utente"], $password, $salt);
		if($err){
            session_unset();
			header("location: login.php");
        }
        else{
            $templateParams["erroreCambioPassword"] = "Errore aggiornamento Password!";
        }	
	}
	$templateParams["titolo"] = "Car Shop - Nuova Password";
	$templateParams["titoloPagina"] = "Nuova Password";
	$templateParams["nome"] = "nuova_password.php";
	$templateParams["js"][0] = "./js/nuovaPassword.js";
	$templateParams["js"][1] = "./js/SecurityPassword.js";
	$templateParams["css"][0] = "./css/gianluca_style.css";
}
else{
    header("location: login.php");
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>
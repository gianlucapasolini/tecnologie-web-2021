<?php 

function getMessageFromStatus($IdStato, $Nome_Utente, $IdOrdine)
{
    switch ($IdStato) {
        case 3:
            //PRONTO PER LA SPEDIZIONE
            $messaggio = "Buongiorno ".$Nome_Utente.", l'ordine numero: ".$IdOrdine." da lei effettuato risulta essere PRONTO PER LA SPEDIZIONE, prossimamente le verrà riferito che sarà spedito. Grazie.";
            break;
        case 4:
            //SPEDITO
            $messaggio = "Buongiorno ".$Nome_Utente.", l'ordine numero: ".$IdOrdine." da lei effettuato risulta essere SPEDITO, riceverà l'ordine al più presto e verrà informato sul giorno di consegna. Grazie.";
            break;
        case 5:
            //In Consegna
            $messaggio = "Buongiorno ".$Nome_Utente.", l'ordine numero: ".$IdOrdine." da lei effettuato risulta essere IN CONSEGNA, riceverà l'ordine in giornata. Grazie.";
            break;
        case 6:
            //CONSEGNATO
            $messaggio = "Buongiorno ".$Nome_Utente.", l'ordine numero: ".$IdOrdine." da lei effettuato risulta essere CONSEGNATO, per eventuali problemi non esiti a contattarci e a lasciare una valutazione nella sezione dedicata. Grazie.";
            break;
        
        default:
            $messaggio = "ERRORE STATO SCONOSCIUTO!";
            break;
    }
    return $messaggio;  
}

function getMessageFromStatusForVenditore($IdStato, $Nome_Utente, $IdOrdine)
{
    switch ($IdStato) {
        case 3:
            //PRONTO PER LA SPEDIZIONE
            $messaggio = "Buongiorno ".$Nome_Utente.", è stato creato l'ordine numero: ".$IdOrdine." da un cliente e lo ha già pagato. Il corriere è PRONTO PER LA SPEDIZIONE passerà a ritirare il veicolo nei prossimi giorni. Grazie.";
            break;
        case 6:
            //CONSEGNATO
            $messaggio = "Buongiorno ".$Nome_Utente.", l'ordine numero: ".$IdOrdine." effettuato dal suo cliente risulta essere CONSEGNATO, per eventuali problemi non esiti a contattare l'assistenza di Car Shop. Grazie.";
            break;
        
        default:
            $messaggio = "ERRORE STATO SCONOSCIUTO!";
            break;
    }
    return $messaggio;  
}

function getMessageFromStatusForCORRIERE($IdStato, $Nome_Utente, $IdOrdine)
{
    switch ($IdStato) {
        case 2:
            //RICHIESTA INVIATA
            $messaggio = "Buongiorno ".$Nome_Utente.", un cliente ha creato l'ordine numero: ".$IdOrdine." e ha richiesto lei come corriere. Il PAGAMENTO è già stato EFFETTUATO. La preghiamo di accedere al suo account e aggiornare lo stato dell'ordine a PRONTO PER LA SPEDIZIONE per avvisare i venditori. Successivamente dovrà ritirare le auto e aggiornare lo stato ordine di conseguenza. Per eventuali problemi non esiti a contattare l'assistenza di Car Shop. Grazie.";
            break;
        case 6:
            //CONSEGNATO
            $messaggio = "Buongiorno ".$Nome_Utente.", le confermiamo che l'ordine numero: ".$IdOrdine." è stato CONSEGNATO correttamente, per eventuali problemi non esiti a contattare l'assistenza di Car Shop. Grazie.";
            break;
        
        default:
            $messaggio = "ERRORE STATO SCONOSCIUTO!";
            break;
    }
    return $messaggio;  
}

function sec_session_start() {
    $session_name = 'sec_session_id';
    $secure = false; // Imposta il parametro a true se vuoi usare il protocollo 'https'.
    $httponly = true;
    ini_set('session.use_only_cookies', 1);
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
    session_name($session_name);
    session_start();
    session_regenerate_id();
}

?>
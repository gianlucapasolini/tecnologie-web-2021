<?php
require_once '../dilo_bootstrap.php';
require_once 'prof-functions.php';
header("Content-type: application/json");

if(isset($_GET['id']) && isset($_GET['prezzo'])){
	$val = $dbh_dilo->getPrezzoAttuale((int)$_GET['id']);
	echo json_encode($val);

	exit;
}

if (isset($_GET['id']) && !isset($_GET['rilancio']))
{
	/*$data = $dbh_dilo->getRilanci($_GET["id"];

	$jsonarray= array();
	$i = 0;
	foreach($data as $dato){
		$jsonarray[$i] = $dato;
		$i++;
	}

	echo json_encode($jsonarray);*/
	$jsonarray = array();
	$elements = $dbh_dilo->getRilanci((int)$_GET['id']);
	foreach($elements as $el){
		array_push($jsonarray, $el["Importo"]);
	}
	echo json_encode($jsonarray);

	exit;
} else {
	$msg="ok";
	if (isset($_GET['id']) && isset($_GET['rilancio']) && isset($_GET['CF'])){
		$err = $dbh_dilo->inserisciRilancio((int)$_GET['id'], (int)$_GET['rilancio'], $_GET['CF']);
		if($err==""){
			$err = $dbh_dilo->setPrezzoAttuale((int)$_GET['id'], (int)$_GET['rilancio']);
			
			$partecipanti = $dbh_dilo->getPartecipanti((int)$_GET['id']);
			$asta = $dbh_dilo->getAsta((int)$_GET['id']);
			foreach($partecipanti as $partecipante){
				$desc = "Effettuato un nuovo rilancio di ".$_GET['rilancio']." € per l'asta ".$asta[0]["Modello"];
				$dbh_dilo->inserisciNotifica($partecipante['CF'], $desc, "Nuovo rilancio");
				
				sendEmail($partecipante["Email"], $desc, "Nuovo rilancio");
			}

            $msg = "ok";
        }
        else{
            $msg = "fail";
        }
		echo json_encode($msg);

		exit;
	}
}
?>
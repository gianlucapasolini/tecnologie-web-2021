<?php
require_once 'brighi_bootstrap.php';


if(isUserLoggedIn() && $_SESSION["tipo"] != "CLIENTE"){
    header("location: index.php");
}
if(!isset($_GET["idAuto"]) && !isset($_GET["config"]) && !isset($_POST["modello"])){
		header("location: index.php");
} else {
    $templateParams["titolo"] = "Car Shop - Configuratore";
    $templateParams["titoloPagina"] = "Configuratore";
    if(isUserLoggedIn() && $_SESSION["CF"] != ""){
        $templateParams["numNotifiche"] = $dbh_brighi->getNumeroNotifiche($_SESSION["CF"]);
    }
    
     if(isset($_POST["modello"])){
        if(count($dbh_brighi->getAuto($_POST["modello"]))==0){
            header("location: index.php");
        }
        $templateParams["interniTitle"] = $dbh_brighi->getTitleInterni($_POST["modello"]);
        $templateParams["esterniTitle"] = $dbh_brighi->getTitleEsterni($_POST["modello"]);
        $error="";
        $ok=true;
        if(!isset($_POST["motore-motore"])){
            $error=$error."<li>non selezionato motore</li>";
            $ok=false;
        }
        $templateParams["interniTitle"] = $dbh_brighi->getTitleInterni($_POST["modello"]);
        foreach ($templateParams["interniTitle"] as $value) {
            if(!isset($_POST["interni-".$value["Descrizione"]])){
                $error=$error."<li>non selezionato ".$value["Descrizione"]." in interni</li>";
                $ok=false;
            }
        }
        $templateParams["esterniTilte"] = $dbh_brighi->getTitleEsterni($_POST["modello"]);
        foreach ($templateParams["esterniTitle"] as $value) {
           
            if(!isset($_POST["esterni-".$value["Descrizione"]])){
                $error=$error."<li>non selezionato ".$value["Descrizione"]." in esterni</li>";
                $ok=false;
            }
        }
        if($ok){
            $templateParams["nome"] = "template/recap.php";
            $user=null;
            $config;
            $motore;
            $modello;
            $interni=array();
            $esterni=array();
            $optional=array();
            $tot=0;
            foreach ($_POST as $key => $value) {
                if($key!="modello" && $key!="config" && $key!="optional"){
                    $pezzi = explode("-", $key);
                    $diviso=$pezzi[0];
                    $tipo=$pezzi[1];
                    if($diviso=="motore"){
                        $motore=$dbh_brighi->getMotore($value, $_POST["modello"]);
                    }else if($diviso=="esterni"){
                        $esterni[]=$dbh_brighi->getEsterno($value, $_POST["modello"]);
                    }else if($diviso=="interni"){
                        $interni[]=$dbh_brighi->getInterno($value, $_POST["modello"]);
                    }
                }
                else{
                    if($key=="modello"){
                        $modello=$dbh_brighi->getAuto($_POST["modello"]);
                    }else if($key=="optional"){
                        foreach ($value as $el) 
                        {
                            $optional[]=$dbh_brighi->getOptional($el, $_POST["modello"]);
                        }
                    }else if($key="config"){
                        $config=$value;
                    }
                }
            }
            $totE=0;
            $totI=0;
            $totO=0;
            foreach ($optional as $el) {
                $totE+=$el[0]["Prezzo"];
            }
            foreach ($interni as $el) {
                $totI+=$el[0]["Prezzo"];
            }
            foreach ($esterni as $el) {
                $totO+=$el[0]["Prezzo"];
            }
            $tot+=$modello[0]["Prezzo_base"]+$motore[0]["Prezzo"]+$totO+$totE+$totI;
            
            if(isUserLoggedIn() && $_SESSION["CF"] != ""){
                $user=$_SESSION["CF"];
            }
            if(isset($_POST["config"])){
                $config= $dbh_brighi->insertConfig($modello[0]["IdAuto"], $motore[0]["IdMotore"], $user, $tot, (int)$_POST["config"]);
            }
            else
            {
                $config= $dbh_brighi->insertConfig($modello[0]["IdAuto"], $motore[0]["IdMotore"],$user,$tot);
            }
            foreach ($optional as $el) {
                $dbh_brighi->insertOptional($config, $el[0]["IdOptional"], $modello[0]["IdAuto"]);
            }
            foreach ($esterni as $el) {
                $dbh_brighi->insertEsterni($config, $el[0]["IdEsterni"], $modello[0]["IdAuto"]);
            }
            foreach ($interni as $el) {
                $dbh_brighi->insertInterni($config, $el[0]["IdInterni"], $modello[0]["IdAuto"]);
            }
        }
        else
        {
            $templateParams["nome"] = "template/config.php";
            $templateParams["selectOptional"] = array();
            $templateParams["selectInterni"] = array();
            $templateParams["selectEsterni"] = array();
            
            $templateParams["auto"] = $dbh_brighi->getAuto($_POST["modello"]);
            $templateParams["interni"] = $dbh_brighi->getInterni($templateParams["auto"][0]["IdAuto"]);
            $templateParams["esterni"] = $dbh_brighi->getEsterni($templateParams["auto"][0]["IdAuto"]);
            $templateParams["optional"] = $dbh_brighi->getOptionals($templateParams["auto"][0]["IdAuto"]);
            $templateParams["motori"] = $dbh_brighi->getMotori($templateParams["auto"][0]["IdAuto"]);
            $templateParams["prejs"][0] = "./js/configuratore.js";
        }
    }
    else
    { 
        if(isset($_GET["config"]))
        {
            $templateParams["auto"] = $dbh_brighi->getConfig($_GET["config"]);
            if(count($templateParams["auto"])==1){
                $templateParams["selectInterni"] = array();
                foreach($dbh_brighi->getInterniScelti($_GET["config"]) as $el){
                    $templateParams["selectInterni"][]=$el["IdInterni"];
                }
                $templateParams["selectEsterni"] = array();
                foreach($dbh_brighi->getEsterniScelti($_GET["config"]) as $el){
                    $templateParams["selectEsterni"][]=$el["IdEsterni"];
                }
                $templateParams["selectOptional"] = array();
                foreach($dbh_brighi->getOptionalScelti($_GET["config"]) as $el){
                    $templateParams["selectOptional"][]=$el["IdOptional"];
                }
            }
            else{
                header("location: index.php");
            }
        }
        else if(!isset($_GET["idAuto"])){
            header("location: index.php");
        }
        else{
            $templateParams["auto"] = $dbh_brighi->getAuto($_GET["idAuto"]);
        }
        if(count($templateParams["auto"])==0){
            header("location: index.php");
        }
        $templateParams["nome"] = "template/config.php";
        $templateParams["interni"] = $dbh_brighi->getInterni($templateParams["auto"][0]["IdAuto"]);
        $templateParams["esterni"] = $dbh_brighi->getEsterni($templateParams["auto"][0]["IdAuto"]);
        $templateParams["optional"] = $dbh_brighi->getOptionals($templateParams["auto"][0]["IdAuto"]);
        $templateParams["motori"] = $dbh_brighi->getMotori($templateParams["auto"][0]["IdAuto"]);
        $templateParams["interniTitle"] = $dbh_brighi->getTitleInterni($templateParams["auto"][0]["IdAuto"]);
        $templateParams["esterniTitle"] = $dbh_brighi->getTitleEsterni($templateParams["auto"][0]["IdAuto"]);
        $templateParams["prejs"][0] = "./js/configuratore.js";
    }
    $templateParams["css"][0] = "./css/configuratore.css";
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>
<?php
require_once 'dilo_bootstrap.php';
require_once 'utils/prof-functions.php';
//fare in POST e converire il prezzo
if(isset($_POST["nomeAuto"]) && isset($_POST["descrizione"]) && isset($_FILES["immagine1"]) && isset($_FILES["immagine2"]) && isset($_FILES["immagine3"]) && isset($_POST["dataImm"]) && isset($_POST["prezzo"])){
    //inserisco l'auto e imposto il messaggio
    list($imm1_result, $msg_imm1) = uploadImage(UPLOAD_DIR, $_FILES["immagine1"]);
    list($imm2_result, $msg_imm2) = uploadImage(UPLOAD_DIR, $_FILES["immagine2"]);
    list($imm3_result, $msg_imm3) = uploadImage(UPLOAD_DIR, $_FILES["immagine3"]);
    $suono = NULL;
    $video = NULL;
    if(isset($_FILES["suono"])){
        list($suono_result, $msg_suono) = uploadSound(UPLOAD_DIR, $_FILES["suono"]);
        if($suono_result != 0){
            $suono = $msg_suono;
        }
    }
    if(isset($_FILES["video"])){
        list($video_result, $msg_video) = uploadVideo(UPLOAD_DIR, $_FILES["video"]);
        if($video_result != 0){
            $video = $msg_video;
        }
    }
    if($imm1_result != 0 && $imm2_result != 0 && $imm3_result != 0){
        $err =$dbh_dilo->inserisciAutoInAsta($_SESSION["Nome_Utente"], (int)$_POST["prezzo"], $_POST["dataImm"], $_POST["nomeAuto"], $msg_imm1, $msg_imm2, $msg_imm3, $_POST["descrizione"], $suono, $video);
        if($err==""){
            $msg = "Inserimento avvenuto con successo!";
            $color = "success";
        }
        else{
            $color = "failed";
            $msg = "Errore in inserimento Venditore!";
        }
    } else {
        $msg = "ERRORE! ".$msg_imm1." - ".$$msg_imm2." - ".$msg_imm3;
    }
    $templateParams["msg"] = $msg;
    $templateParams["color"] = $color;
    $templateParams["titoloPagina"] = "Gestione aste";
    $templateParams["nome"] = "template/aste_gestore.php";
    $templateParams["aste"] = $dbh_dilo->getAsteByIVA($_SESSION["Nome_Utente"]);
} else {
    if(isset($_GET["resoconto"])){
        $templateParams["titoloPagina"] = "Gestione aste";
        $templateParams["nome"] = "template/resoconto_aste.php";
        $templateParams["asteChiuse"] = $dbh_dilo->getAsteChiuseByIVA($_SESSION["Nome_Utente"]);
    } else {
        if(isset($_SESSION["tipo"]) && !isset($_GET["tipologia"])){
            if($_SESSION["tipo"] == "VENDITORE"){
                if(isset($_SESSION["Nome_Utente"])){
                    $templateParams["titoloPagina"] = "Gestione aste";
                    $templateParams["nome"] = "template/aste_gestore.php";
                    $templateParams["aste"] = $dbh_dilo->getAsteByIVA($_SESSION["Nome_Utente"]);
                }
            } else {
                $templateParams["titoloPagina"] = "Menu aste";
                $templateParams["nome"] = "template/aste_menu.php";
                $templateParams["asteRandom"] = $dbh_dilo->getRandomAste(3);
                $templateParams["caseAsta"] = $dbh_dilo->getCaseAsta();
            }
            
        } else {
            if(isset($_GET["tipologia"])){
                if($_GET["tipologia"] == "ricerca" && isset($_GET["casaAsta"])){
                    $templateParams["titoloPagina"] = "Ricerca aste";
                    $templateParams["nome"] = "template/aste_elenco.php";
                    $templateParams["aste"] = $dbh_dilo->getFiltroCasaAste($_GET["casaAsta"]);
                } else {
                    if($_GET["tipologia"] == "selezione" && isset($_GET["asta"])){
                        $asta = $dbh_dilo->getAsta($_GET["asta"]);
                        $templateParams["titoloPagina"] = $asta[0]["Modello"];
                        $templateParams["nome"] = "template/aste_page.php";
                        $templateParams["js"] = ["js/gestione_asta.js"];
                        $templateParams["modal"] = "Aste_page_modal.php";
                        $templateParams["section"] = "Aste_page_carousel.php";
                        $templateParams["infoAsta"] = $dbh_dilo->getAsta($_GET["asta"]);
                    } else {
                        if($_GET["tipologia"] == "Inserimento"){
                            $templateParams["titoloPagina"] = "Inserimento aste";
                            $templateParams["nome"] = "template/aste_inserimento.php";
                        } else {
                            if($_GET["tipologia"] == "Eliminazione"){
                                $dbh_dilo->chiudiAsta((int)$_GET["id"]);
                                $dbh_dilo->setVincitore((int)$_GET["id"]);
                                $partecipanti = $dbh_dilo->getPartecipanti((int)$_GET["id"]);
                                $asta = $dbh_dilo->getAsta((int)$_GET["id"]);
                                foreach($partecipanti as $partecipante){
                                    $desc = "Avviso di chiusura dell'asta modello ".$asta[0]["Modello"];
                                    $dbh_dilo->inserisciNotifica($partecipante['CF'], $desc, "Chiusura asta");
                                    sendEmail($partecipante['Email'], $desc, "Chiusura asta");
                                }
        
                                $templateParams["titoloPagina"] = "Gestione aste";
                                $templateParams["nome"] = "template/aste_gestore.php";
                                $templateParams["aste"] = $dbh_dilo->getAsteByIVA($_SESSION["Nome_Utente"]);
                                
                            } else {
                                if($_GET["tipologia"] == "ricercaAvanzata"){
                                    $templateParams["titoloPagina"] = "Ricerca aste";
                                    $templateParams["nome"] = "template/aste_elenco.php";
                                    $casaAsta = " ";
                                    $battuta = 0;
                                    $modello = " ";
                                    $data = " ";
                                    if(isset($_GET["casaAsta"])) {
                                        if($_GET["casaAsta"] != ""){
                                            $casaAsta = $_GET["casaAsta"];
                                        }
                                    }
                                    if(isset($_GET["checkBattuta"])) {
                                        if((int)$_GET["checkBattuta"] == 1){
                                            $battuta = 1;
                                        }
                                    }
                                    if(isset($_GET["modello"])) {
                                        if($_GET["modello"] == " "){
                                            $modello = $_GET["modello"];
                                        }
                                    }
                                    if(isset($_GET["data"])) {
                                        if($_GET["data"] == " "){
                                            $data = $_GET["data"];
                                        }
                                    }
                                    $templateParams["aste"] = $dbh_dilo->getFiltroAvanzato($casaAsta, $battuta, $modello, $data);
                                }
                            }
                        }
                    }
                }
            } else {
                $templateParams["titoloPagina"] = "Aste menu";
                $templateParams["nome"] = "template/aste_menu.php";
                $templateParams["asteRandom"] = $dbh_dilo->getRandomAste(3);
                $templateParams["caseAsta"] = $dbh_dilo->getCaseAsta();
            }
            
        }
    }

}
if(isset($_SESSION["CF"])){
    $numNotificheNonLette = $dbh_dilo->getNumeroNotifiche($_SESSION["CF"]);
    $templateParams["numNotifiche"] = $numNotificheNonLette;
}

$templateParams["css"] = ["css/Dilo_style.css"];
$templateParams["titolo"] = "CarShop - Aste";
require 'template/struttura.php';
?>
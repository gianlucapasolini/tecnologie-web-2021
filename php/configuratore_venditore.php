<?php
require_once 'brighi_bootstrap.php';

if(!(isUserLoggedIn() && isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "VENDITORE" && $_SESSION["Casa_Asta"] === 0)){
        header("location: index.php");
}
$templateParams["titolo"] = "Car Shop - Aggiungi";
$templateParams["titoloPagina"] = "Aggiungi";
$templateParams["marca"] = $dbh_brighi->getVenditore($_SESSION["Nome_Utente"])[0];
if(isset($_GET["modello"]) || isset($_POST["modello"])){
    if(isset($_GET["modello"]) ){
        $templateParams["auto"]=$_GET["modello"];
    }
    else{
        if($_POST["tipo"]=="auto"){
            if(trim($_POST["modello"]) != "")
            {
                if (is_numeric($_POST["prezzo"]) && $_POST["prezzo"]>4000 && $_POST["prezzo"]<10000000 ){
                    if($_FILES["copertina"]["name"] != "" ){
                        list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["copertina"]);
                        if($result){
                            if(trim($_POST["descrizione"])==""){
                                $_POST["descrizione"]=null;
                            }
                            $templateParams["actual"]= "update";
                            $templateParams["auto"]=$dbh_brighi->insertNewCar($_POST["modello"], $_POST["prezzo"], $msg, $_POST["descrizione"]);
                            $dbh_brighi->insertCar($templateParams["auto"], $templateParams["marca"]["P_IVA"]);
                            if($_FILES["video"]["name"] != "" ){
                                list($result, $msg) = uploadVideo(UPLOAD_DIR, $_FILES["video"]);
                                if($result){
                                    $dbh_brighi->updateVideo($templateParams["auto"], $msg);
                                } else {
                                    $templateParams["erroreSendVideo"] = "Errore caricamento video! riprova ";
                                }
                            }
                            if($_FILES["suono"]["name"] != "" ){
                                list($result, $msg) = uploadSound(UPLOAD_DIR, $_FILES["suono"]);
                                if($result){
                                    $dbh_brighi->updateAudio($templateParams["auto"], $msg);
                                } else {
                                    $templateParams["erroreSendAudio"] = "Errore caricamento audio! riprova ";
                                }
                            }
                        } else {
                            $templateParams["erroreSendImage"] = "Errore caricamento coperitina! riprova ";
                        }
                    }
                    else{
                        $templateParams["erroreSendImage"] = "Errore! inserire copertina ";
                    }
                }
                else{
                    $templateParams["errorePrezzo"] = "Errore! inserire un numero tra 4000 e 10000000";
                }
            }
            else{
                $templateParams["erroreModello"] = "Errore! inserire nome modello ";
            }
        }
        elseif($_POST["tipo"]=="update") {
            $templateParams["actual"]= $_POST["tipo"];
            $templateParams["auto"] = $dbh_brighi->getAuto($_POST["auto"])[0];
            $templateParams["aggiornamenti"]=array();
            foreach($_POST as $key=>$value){
                switch($key){
                    case "modello":
                    {
                        if(trim($value)!=""){
                            if($value!=$templateParams["auto"]["Modello"]){
                                $templateParams["auto"]["Modello"]=$value;
                                $templateParams["aggiornamenti"][]=$key;
                            }
                        }
                        else{
                            $templateParams["erroreModello"] = "Errore! inserire nome modello ";
                        }
                        break;
                    }
                    case "prezzo":
                    {
                        if (is_numeric($value) && $value>4000 && $value<10000000 ){
                                if( $value!=$templateParams["auto"]["Prezzo_base"]){
                                    $templateParams["auto"]["Prezzo_base"]=$value;
                                    $templateParams["aggiornamenti"][]=$key;
                                }
                        }
                        else{
                            $templateParams["errorePrezzo"] = "Errore! inserire un numero tra 4000 e 10000000";
                        }
                        break;
                    }
                    case "descrizione":
                    {
                        if(trim($value)!="" ){
                            if($value!=$templateParams["auto"]["Descrizione"]){
                                $templateParams["auto"]["Descrizione"]=$value;
                                $templateParams["aggiornamenti"][]=$key;
                            }
                        }
                        else{
                            $templateParams["auto"]["Descrizione"]=null;
                        }
                        break;
                    }
                }
            }
            if(isset($_FILES)){
                foreach($_FILES as $key=>$value){
                    if($_FILES[$key]["name"] != "" ){
                        switch($key){
                            case "copertina":
                            {
                                list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["copertina"]);
                                if($result){
                                    $templateParams["auto"]["Link_immagine"]=$msg;
                                    $templateParams["aggiornamenti"][]=$key;
                                } else {
                                    $templateParams["erroreSendImage"] = "Errore caricamento immagine! riprova ";
                                }
                                break;
                            }
                            case "suono":
                            {
                                list($result, $msg) = uploadSound(UPLOAD_DIR, $_FILES["suono"]);
                                if($result){
                                    $templateParams["auto"]["Suono"]=$msg;
                                    $templateParams["aggiornamenti"][]=$key;
                                } else {
                                    $templateParams["erroreSendAudio"] = "Errore caricamento audio! riprova ";
                                }
                                break;
                            }
                            case "video":
                            {
                                list($result, $msg) = uploadVideo(UPLOAD_DIR, $_FILES["video"]);
                                if($result){
                                    $templateParams["auto"]["Link_video"]=$msg;
                                    $templateParams["aggiornamenti"][]=$key;
                                } else {
                                    $templateParams["erroreSendVideo"] = "Errore caricamento video! riprova ";
                                }
                                break;
                            }
                        }
                    }
                }
            }
            $dbh_brighi->updateCar($templateParams["auto"]["IdAuto"], 
            $templateParams["auto"]["Modello"], $templateParams["auto"]["Prezzo_base"], $templateParams["auto"]["Link_immagine"], 
            $templateParams["auto"]["Descrizione"], $templateParams["auto"]["Link_video"], $templateParams["auto"]["Suono"]);
            $templateParams["auto"]=$templateParams["auto"]["IdAuto"];
        }
        elseif($_POST["tipo"]=="motore"){
            $templateParams["actual"]= $_POST["tipo"];
            $templateParams["auto"] = $_POST["modello"];
            $ok=true;
            if(trim($_POST["descrizione"])==""){
                $ok=false;
                $templateParams["erroreMDescrizione"]="Errore, descrizione richiesta";
            }
            if(trim($_POST["cavalli"])==""){
                $_POST["cavalli"]=null;
            }
            else
            {
                if (!is_numeric($_POST["cavalli"])){
                    $ok=false;
                    $templateParams["erroreMCavalli"]="Errore, inserire cavalli  con valore tra 0 e 10000";
                }
                else{
                    if($_POST["cavalli"]<0 || $_POST["cavalli"]> 10000){
                        $ok=false;
                        $templateParams["erroreMCavalli"]="Errore, inserire cavalli  con valore tra 0 e 10000";
                    }
                }
            }
            if(trim($_POST["cilindrata"])==""){
                $_POST["cilindrata"]=null;
            }
            else
            {
                if (!is_numeric($_POST["cilindrata"])){
                    $ok=false;
                    $templateParams["erroreMCilindrata"]="Errore, inserire una cilindrata numerica tra 0 e 10000";
                }
                else{
                    if($_POST["cilindrata"]<0 || $_POST["cilindrata"]> 10000){
                        $ok=false;
                        $templateParams["erroreMCilindrata"]="Errore, inserire una cilindrata numerica tra 0 e 10000";
                    }
                }
            }
            if(trim($_POST["nome"])==""){
                $ok=false;
                $templateParams["erroreMNome"]="Errore, inserire nome";
            }
            if (!is_numeric($_POST["prezzo"])){
                $ok=false;
                $templateParams["erroreMPrezzo"]="Errore, inserire una prezzo numerica tra 0 e 10000";
            }
            else{
                if($_POST["prezzo"]<0 || $_POST["prezzo"]> 10000){
                    $ok=false;
                    $templateParams["erroreMPrezzo"]="Errore, inserire una prezzo numerica tra 0 e 10000";
                }
            }
            if($ok){
                $dbh_brighi->insertNewMotore($_POST["modello"],$_POST["nome"], $_POST["descrizione"], $_POST["prezzo"], $_POST["cavalli"], $_POST["cilindrata"]);
            }
        }
        elseif($_POST["tipo"]=="interni"){
            $templateParams["actual"]= $_POST["tipo"];
            $templateParams["auto"] = $_POST["modello"];
            $ok=true;
            if(trim($_POST["tipologia"])==""){
                $ok=false;
                $templateParams["erroreIDescrizione"] = "Errore! inserire tipo";
            }
            if(trim($_POST["nome"])==""){
                $ok=false;
                $templateParams["erroreINome"] = "Errore! inserire nome";
            }
            if($_FILES["immagine"]["name"] != "" ){
                list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["immagine"]);
                if($result){
                    $_POST["immagine"]=$msg;
                }
                else {
                    $templateParams["erroreIImage"] = "Errore caricamento coperitina! riprova ";
                }
            }
            else {
                $_POST["immagine"]=null;
            }
            if (!is_numeric($_POST["prezzo"])){
                $ok=false;
                $templateParams["erroreIPrezzo"] = "Errore! inserire prezzo";
            }
            else{
                if($_POST["prezzo"]<0 || $_POST["prezzo"]> 10000){
                    $ok=false;
                    $templateParams["erroreIPrezzo"] = "Errore! inserire prezzo";
                }
            }
            if($ok){
                $dbh_brighi->insertNewInterno($_POST["modello"], $_POST["nome"], $_POST["tipologia"], $_POST["immagine"], $_POST["prezzo"]);
            }
        }
        elseif($_POST["tipo"]=="esterni"){
            $templateParams["actual"]= $_POST["tipo"];
            $templateParams["auto"] = $_POST["modello"];
            $ok=true;
            if(trim($_POST["tipologia"])==""){
                $ok=false;
                $templateParams["erroreEDescrizione"] = "Errore! inserire tipo";
            }
            if(trim($_POST["nome"])==""){
                $ok=false;
                $templateParams["erroreENome"] = "Errore! inserire nome";
            }
            if($_FILES["immagine"]["name"] != "" ){
                list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["immagine"]);
                if($result){
                    $_POST["immagine"]=$msg;
                }
                else {
                    $templateParams["erroreEImage"] = "Errore caricamento immagine! riprova ";
                }
            }
            else {
                $_POST["immagine"]=null;
            }
            if (!is_numeric($_POST["prezzo"])){
                $ok=false;
                $templateParams["erroreEPrezzo"] = "Errore! inserire prezzo";
            }
            else{
                if($_POST["prezzo"]<0 || $_POST["prezzo"]> 10000){
                    $ok=false;
                    $templateParams["erroreEPrezzo"] = "Errore! inserire prezzo";
                }
            }
            if($ok){
                $dbh_brighi->insertNewEsterno($_POST["modello"], $_POST["nome"], $_POST["tipologia"], $_POST["immagine"], $_POST["prezzo"]);
            }
        }
        elseif($_POST["tipo"]=="optional"){
            $templateParams["actual"]= $_POST["tipo"];
            $templateParams["auto"] = $_POST["modello"];
            $ok=true;
            if(trim($_POST["descrizione"])==""){
                $ok=false;
                $templateParams["erroreODescrizione"] = "Errore! inserire descrizione";
            }
            if(trim($_POST["nome"])==""){
                $ok=false;
                $templateParams["erroreONome"] = "Errore! inserire nome";
            }
            if($_FILES["immagine"]["name"] != "" ){
                list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["immagine"]);
                if($result){
                    $_POST["immagine"]=$msg;
                }
                else {
                    $templateParams["erroreOImage"] = "Errore caricamento immagine! riprova ";
                }
            }
            else {
                $_POST["immagine"]=null;
            }
            if (!is_numeric($_POST["prezzo"])){
                $ok=false;
                $templateParams["erroreOPrezzo"] = "Errore! inserire prezzo";
            }
            else{
                if($_POST["prezzo"]<0 || $_POST["prezzo"]> 10000){
                    $ok=false;
                    $templateParams["erroreOPrezzo"] = "Errore! inserire prezzo";
                }
            }
            if($ok){
                $dbh_brighi->insertNewOptional($_POST["modello"], $_POST["nome"], $_POST["descrizione"], $_POST["immagine"], $_POST["prezzo"]);
            }
        }
        elseif($_POST["tipo"]=="immagini"){
            $templateParams["actual"]= "update";
            $templateParams["auto"] = $_POST["modello"];
            $ok=true;
            if(trim($_POST["descrizione"])==""){
                $ok=false;
                $templateParams["erroreImDescrizione"] = "Errore! inserire descrizione ";
            }
            else{
                if($_FILES["immagine"]["name"] != "" ){
                    list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["immagine"]);
                    if($result){
                        $_POST["immagine"]=$msg;
                    }
                    else {
                        $templateParams["erroreImImage"] = "Errore caricamento immagine! riprova ";
                        $ok=false;
                    }
                }
                else {
                    $templateParams["erroreImImage"] = "Errore caricamento immagine! riprova ";
                    $ok=false;
                }
            }
            if($ok){
                $dbh_brighi->insertImmagine($_POST["modello"], $_POST["descrizione"], $_POST["immagine"]);
                $templateParams["aggiornamenti"][]="nuova immagine con descrizione";
            }
        }
    }
    if(isset($templateParams["auto"])){
        
        $idAuto=$templateParams["auto"];
        $templateParams["auto"] = $dbh_brighi->getAuto($templateParams["auto"])[0];
        if(count($dbh_brighi->gestisce($templateParams["auto"]["IdAuto"],$templateParams["marca"]["P_IVA"]))==0){
            header("location: index.php");
        }
        $templateParams["interniTitle"] = $dbh_brighi->getTitleInterni($idAuto);
        $templateParams["esterniTitle"] = $dbh_brighi->getTitleEsterni($idAuto);
        $templateParams["interni"] = $dbh_brighi->getInterni($idAuto);
        $templateParams["esterni"] = $dbh_brighi->getEsterni($idAuto);
        $templateParams["optional"] = $dbh_brighi->getOptionals($idAuto);
        $templateParams["motori"] = $dbh_brighi->getMotori($idAuto);
    }
}
$templateParams["nome"] = "template/configuratore_venditore.php";
$templateParams["css"][0] = "./css/configuratore_venditore.css";
$templateParams["prejs"][0] = "./js/configuratore_venditore.js";

//require 'template/base_____.php';
require 'template/struttura.php';
?>
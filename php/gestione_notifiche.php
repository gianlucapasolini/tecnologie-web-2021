<?php
require_once 'dilo_bootstrap.php';

//Base Template
if(isset($_GET["idNotifica"])){
    if($_SESSION["tipo"] == "VENDITORE"){
        $dbh_dilo->setNotificaLettaVenditore($_GET["idNotifica"]);
        $templateParams["msg"] = "Notifica segnata come letta";
    } else {
        if($_SESSION["tipo"] == "CORRIERE"){
            $dbh_dilo->setNotificaLettaCorriere($_GET["idNotifica"]);
            $templateParams["msg"] = "Notifica segnata come letta";
        } else {
            $dbh_dilo->setNotificaLetta($_GET["idNotifica"]);
            $templateParams["msg"] = "Notifica segnata come letta";
        }
    }
}
$templateParams["titolo"] = "Notifiche";
$templateParams["titoloPagina"] = "Notifiche";
$templateParams["nome"] = "template/Notifiche.php";

if(isset($_SESSION["tipo"])){
    if($_SESSION["tipo"] == "VENDITORE"){
        $utente = $dbh_dilo->getPIVA($_SESSION["Nome_Utente"]);
        $numNotificheLette = $dbh_dilo->getNumeroNotificheVenditore($utente,1);
        $numNotificheNonLette = $dbh_dilo->getNumeroNotificheVenditore($utente);
        if($numNotificheLette != 0 || $numNotificheNonLette != 0){
            if(isset($_GET["filtra"])){
                if($_GET["filtra"] == "Lette"){
                    $templateParams["Notifiche"] = $dbh_dilo->getNotificheFiltrateVenditore($utente, 1);
                } else {
                    if($_GET["filtra"] == "Non lette"){
                        $templateParams["Notifiche"] = $dbh_dilo->getNotificheFiltrateVenditore($utente, 0);
                    } else {
                        if($_GET["filtra"] == "Tutte"){
                            $templateParams["Notifiche"] = $dbh_dilo->getNotificheVenditore($utente);
                        } else {
                            $templateParams["Notifiche"] = $dbh_dilo->getNotificheVenditore($utente);
                        }
                    }
                }
            } else {
                $templateParams["Notifiche"] = $dbh_dilo->getNotificheVenditore($utente);
            }
        } else {
            $templateParams["msg"] = "Non ci sono notifiche";
        }

    } else {
        if($_SESSION["tipo"] == "CORRIERE"){
            $utente = $dbh_dilo->getPIVACorriere($_SESSION["Nome_Utente"]);
            $numNotificheLette = $dbh_dilo->getNumeroNotificheCorriere($utente,1);
            $numNotificheNonLette = $dbh_dilo->getNumeroNotificheCorriere($utente);
            if($numNotificheLette != 0 || $numNotificheNonLette != 0){
                if(isset($_GET["filtra"])){
                    if($_GET["filtra"] == "Lette"){
                        $templateParams["Notifiche"] = $dbh_dilo->getNotificheFiltrateCorriere($utente, 1);
                    } else {
                        if($_GET["filtra"] == "Non lette"){
                            $templateParams["Notifiche"] = $dbh_dilo->getNotificheFiltrateCorriere($utente, 0);
                        } else {
                            if($_GET["filtra"] == "Tutte"){
                                $templateParams["Notifiche"] = $dbh_dilo->getNotificheCorriere($utente);
                            } else {
                                $templateParams["Notifiche"] = $dbh_dilo->getNotifiche($utente);
                            }
                        }
                    }
                } else {
                    $templateParams["Notifiche"] = $dbh_dilo->getNotificheCorriere($utente);
                }
            } else {
                $templateParams["msg"] = "Non ci sono notifiche";
            }
        } else {
            if(isset($_SESSION["CF"])){
                $numNotificheLette = $dbh_dilo->getNumeroNotifiche($_SESSION["CF"],1);
                $numNotificheNonLette = $dbh_dilo->getNumeroNotifiche($_SESSION["CF"]);
                $templateParams["numNotifiche"] = $numNotificheNonLette;
                if($numNotificheLette != 0 || $numNotificheNonLette != 0){
                    if(isset($_GET["filtra"])){
                        if($_GET["filtra"] == "Lette"){
                            $templateParams["Notifiche"] = $dbh_dilo->getNotificheFiltrate($_SESSION["CF"], 1);
                        } else {
                            if($_GET["filtra"] == "Non lette"){
                                $templateParams["Notifiche"] = $dbh_dilo->getNotificheFiltrate($_SESSION["CF"], 0);
                            } else {
                                if($_GET["filtra"] == "Tutte"){
                                    $templateParams["Notifiche"] = $dbh_dilo->getNotifiche($_SESSION["CF"]);
                                } else {
                                    $templateParams["Notifiche"] = $dbh_dilo->getNotifiche($_SESSION["CF"]);
                                }
                            }
                        }
                    } else {
                        $templateParams["Notifiche"] = $dbh_dilo->getNotifiche($_SESSION["CF"]);
                    }
                } else {
                    $templateParams["msg"] = "Non ci sono notifiche";
                }
            } else {
                $templateParams["msg"] = "Non ci sono notifiche";
            }
        }
    }
}


$templateParams["css"] = ["css/Dilo_style.css"];
require 'template/struttura.php';
/*
navbar desktop
$_SESSION["Ripristino"] = "1" devo portare qualsiasi pagina a nuova password
*/
?>
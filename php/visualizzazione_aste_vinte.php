<?php
require_once 'dilo_bootstrap.php';
require_once 'utils/prof-functions.php';
if(isset($_SESSION["CF"])){
    $numNotificheNonLette = $dbh_dilo->getNumeroNotifiche($_SESSION["CF"]);
    $templateParams["numNotifiche"] = $numNotificheNonLette;
    $templateParams["aste"] = $dbh_dilo->getAsteVinte($_SESSION["CF"]);
}

$templateParams["css"] = ["css/Dilo_style.css"];
$templateParams["titolo"] = "CarShop - Aste vinte";
$templateParams["titoloPagina"] = "Aste vinte";
$templateParams["nome"] = "template/aste_vinte.php";
require 'template/struttura.php';
?>
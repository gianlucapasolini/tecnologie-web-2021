<?php
require_once 'dilo_bootstrap.php';
//gestisco selezione modello e visualizzazione pagina
//Base Template

if(isset($_GET["commentoInviato"])){
    if(isset($_SESSION["CF"])){
        $dbh_dilo->inserisciCommento($_SESSION["CF"], $_GET["idAuto"], $_GET["commentoInviato"], $_GET["numStelline"]);   
    }
}

$templateParams["titoloPagina"] = "Auto";
if(isset($_GET["idAuto"]) && !isset($_GET["commento"])){
    $auto = $dbh_dilo->getAuto($_GET["idAuto"]);
    $count = count($auto);
    if($count != 0){
        if(isset($_SESSION["CF"])){
            $commentoEsistente = $dbh_dilo->getCommento($_SESSION["CF"], $_GET["idAuto"]);
            if(count($commentoEsistente) != 0){
                $templateParams["haCommentato"] = "si";
            } else {
                $templateParams["haCommentato"] = "no";
            }
        }
        
        $totale = $dbh_dilo->getNumCommenti($_GET["idAuto"]);
        
        $templateParams["auto"] = $auto[0];
        $templateParams["totaleCommenti"] = $totale;
        if($totale == 0){
            $templateParams["NoComments"] = 1;
        } else {
            $stelline = $dbh_dilo->getNumCommentiFiltro($_GET["idAuto"],5);
            $templateParams["5stelline"]["num"] = $stelline;
            $templateParams["5stelline"]["perc"] = ($stelline*100)/$totale;
            $templateParams["5stelline"]["commenti"] = $dbh_dilo->getCommenti($_GET["idAuto"],5);
            $stelline = $dbh_dilo->getNumCommentiFiltro($_GET["idAuto"],4);
            $templateParams["4stelline"]["num"] = $stelline;
            $templateParams["4stelline"]["perc"] = ($stelline*100)/$totale;
            $templateParams["4stelline"]["commenti"] = $dbh_dilo->getCommenti($_GET["idAuto"],4);
            $stelline = $dbh_dilo->getNumCommentiFiltro($_GET["idAuto"],3);
            $templateParams["3stelline"]["num"] = $stelline;
            $templateParams["3stelline"]["perc"] = ($stelline*100)/$totale;
            $templateParams["3stelline"]["commenti"] = $dbh_dilo->getCommenti($_GET["idAuto"],3);
            $stelline = $dbh_dilo->getNumCommentiFiltro($_GET["idAuto"],2);
            $templateParams["2stelline"]["num"] = $stelline;
            $templateParams["2stelline"]["perc"] = ($stelline*100)/$totale;
            $templateParams["2stelline"]["commenti"] = $dbh_dilo->getCommenti($_GET["idAuto"],2);
            $stelline = $dbh_dilo->getNumCommentiFiltro($_GET["idAuto"],1);
            $templateParams["1stelline"]["num"] = $stelline;
            $templateParams["1stelline"]["perc"] = ($stelline*100)/$totale;
            $templateParams["1stelline"]["commenti"] = $dbh_dilo->getCommenti($_GET["idAuto"],1);
        }
        $templateParams["immagini"] = $dbh_dilo->getImmagini($_GET["idAuto"]);
        $templateParams["titoloPagina"] = $auto[0]["Modello"];
        $templateParams["nome"] = "template/singola_auto.php";
    } else {
        header("Location: index.php");
    }
} else {
    if(isset($_GET["commento"]) && isset($_GET["idAuto"])){
        $templateParams["nome"] = "template/singola_auto_commento.php";
        $templateParams["idAuto"] = $_GET["idAuto"];
    } else {
        header("Location: index.php");
    }
}

if(isset($_SESSION["CF"])){
    $numNotificheNonLette = $dbh_dilo->getNumeroNotifiche($_SESSION["CF"]);
    $templateParams["numNotifiche"] = $numNotificheNonLette;
}
$templateParams["titolo"] = "CarShop - Auto singola";

$templateParams["css"] = ["css/Dilo_style.css"];
require 'template/struttura.php';
?>
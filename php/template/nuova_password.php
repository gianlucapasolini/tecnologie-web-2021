		<div class="container-fluid p-0 overflow-hidden">
			<div class="row">
				<div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4 mx-auto">
				    <form action="#" method="POST" class="border m-4 p-4">
				        <h2 class="text-center pb-4">Creala nuova</h2>
				        <?php if(isset($templateParams["erroreCambioPassword"])): ?>
			            <p><?php echo $templateParams["erroreCambioPassword"]; ?></p>
			            <?php endif; ?>
				        <div class="form-group row" id="Field_Password">
				            <label for="password" class="col-12">Password:</label>
				            <div class="col-1"></div>
				            <div class="input-group col-10 px-0">
					            <input type="password" id="password" name="password" class="form-control" placeholder="Password" aria-describedby="passwordHelpBlock" maxlength="30" pattern="(?=.*\d)(?=.*[!@#$%^&*])(?=.*?[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,30}" required/>
					            <div class="input-group-prepend">
		                            <button type="button" id="showPassword" class="btn btn-light rounded">
		                            	<em aria-hidden="true" class="fas fa-eye" title="mostra password"></em>
		                            </button>
		                        </div>
	                    	</div>
							<div id="passwordHelpBlock" class="col-12 p-0 bg-light mt-3">
								<h3 class="p-2 text-center">Password contiene:</h3>
								<div class="col-12 p-0 pl-4">
									<p id="letter" class="invalid">Una lettera <strong>minuscola</strong> </p>
									<p id="capital" class="invalid">Una lettera <strong>MAIUSCOLA</strong> </p>
									<p id="number" class="invalid">Una <strong>numero</strong></p>
									<p id="specChar" class="invalid"><strong>Carattere speciale</strong> (!@#$%^&*)</p>
									<p id="length" class="invalid"><strong>8 - 30 caratteri</strong></p>
								</div>
							</div>
				        </div>
				        <div class="form-group row" id="Field_ConfPassowrd">
				            <label for="Conf_password" class="col-12">Conferma Password:</label>
				            <div class="col-1"></div>
				            <div class="input-group col-10 px-0">
					            <input type="password" id="Conf_password" name="Conf_password" class="form-control" placeholder="Password" aria-describedby="passwordHelpBlockCheck" pattern=".{8,30}" title="8 - 30 caratteri" maxlength="30" required/>
					            <div class="input-group-prepend">
		                            <button type="button" id="showConfPassword" class="btn btn-light rounded">
		                                <em aria-hidden="true" class="fas fa-eye" title="mostra conferma password"></em>
		                            </button>
		                        </div>
		                    </div>
				            <div id="passwordHelpBlockCheck" class="form-text text-danger">
							  Le password devono corrispondere.
							</div>
				        </div>
				        <div class="form-group text-right">
				            <input type="submit" name="submit" value="Salva Nuova Password" class="btn btn-primary"/>
				        </div>
				    </form>
				</div>
			</div>
		</div>
<div class="container horizontal_list mt-5">
    
    <?php
    foreach($templateParams["aste"] as $asta):
    ?>
        <div class="card horizontal_list_item" style="width: 18rem;">
            <img src="upload/<?php echo $asta["Link_Immagine1"]; ?>" class="card-img-top" alt="Asta Modello <?php echo $asta["Modello"]; ?>"/>
            <div class="card-body">
                <h5 class="card-title"><?php echo $asta["Modello"]; ?></h5>
                <p class="card-text horizontal_list_text"> <?php echo $asta["prezzoAttuale"]; ?> €</p>
                <form method="GET" action="aste.php">
                    <input type="hidden" name="tipologia" value="selezione"/>
                    <input type="hidden" name="asta" value="<?php echo $asta["IdAsta"]; ?>"/>
                    <input type="submit" class="btn btn-primary" value="Apri" aria-label="Apri asta <?php echo $asta["Modello"]; ?>"/>
                </form>
            </div>
        </div>
    <?php
    endforeach;
    ?>    
</div>
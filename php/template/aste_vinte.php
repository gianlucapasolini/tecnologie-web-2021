<div class="container-fluid mx-auto resizeContainer">
    <?php if(isset($templateParams["aste"])): ?>
        <h2> Resoconto delle aste </h2>
        <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Asta</th>
                <th scope="col">Prezzo finale</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; foreach($templateParams["aste"] as $asta): ?>
                <tr>
                    <th scope="row"><?php echo $i; $i++;?></th>
                    <td><?php echo $asta["Modello"]; ?></td>
                    <td><?php echo $asta["prezzoAttuale"]; ?></td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    <?php else: ?>
        <h2> Non ci sono aste vinte </h2>
    <?php endif; ?>
</div>
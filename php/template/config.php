<header>
    <img src="<?=UPLOAD_DIR.$templateParams["auto"][0]["Link_immagine"]?>" alt="<?=$templateParams["auto"][0]["Modello"]?>" />
</header>
<form action="configuratore.php" method="post" id="form">
    <input type="hidden" id="modello" name="modello" value="<?=$templateParams["auto"][0]["IdAuto"]?>">
    <?php if(isset($templateParams["auto"][0]["IdAutoConfigurata"]) && $templateParams["auto"][0]["IdOrdine"]==null):?>
        <input type="hidden" id="config" name="config" value="<?=$templateParams["auto"][0]["IdAutoConfigurata"] ?>"/>
    <?php endif;?>
        <ul class="nav nav-pills nav-justified" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="pills-motore-tab" data-toggle="pill" href="#pills-motore" role="tab"
                    aria-controls="pills-motore" aria-selected="true">Motore</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="pills-esterni-tab" data-toggle="pill" href="#pills-esterni" role="tab"
                    aria-controls="pills-esterni" aria-selected="false">Esterni</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="pills-interni-tab" data-toggle="pill" href="#pills-interni" role="tab"
                    aria-controls="pills-interni" aria-selected="false">Interni</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="pills-optional-tab" data-toggle="pill" href="#pills-optional" role="tab"
                    aria-controls="pills-optional" aria-selected="false">Optional</a>
            </li>
        </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-motore" role="tabpanel" aria-labelledby="pills-motore-tab">
            <div id="motore-tipo">
                <fieldset>
                    <legend>Motore</legend>
                    <ul>
                    <?php $n=0;
                        foreach($templateParams["motori"] as $motore): ?>
                        <li>
                            <input type="radio" id="m-<?=$motore["IdMotore"]?>" name="motore-motore" value="<?=$motore["IdMotore"]?>" 
                            <?php if(isset($_POST["motore-motore"])):
                                    if($motore["IdMotore"]==$_POST["motore-motore"]):?>
                                        checked
                            <?php  endif;
                                elseif(isset( $templateParams["auto"][0]["IdMotore"])):
                                    if($motore["IdMotore"]==$templateParams["auto"][0]["IdMotore"]):?>
                                    checked
                            <?php endif;
                            elseif($n==0):?>
                                checked
                            <?php  
                                $n++;
                            endif;?>/>
                            <label for="m-<?=$motore["IdMotore"]?>">
                                <span><?=$motore["Nome"]?></span>
                                <span><?=$motore["Descrizione"]?></span>
                                <?php if($motore["Cilindrata"]!=null):?>
                                <span>Cilindrata: <?=$motore["Cilindrata"]?></span>
                                <?php endif;?>
                                <?php if($motore["Cavalli"]!=null):?>
                                <span>Cavalli: <?=$motore["Cavalli"]?></span>
                                <?php endif;?>
                                <span><em><?=$motore["Prezzo"]?></em>€</span>
                            </label>
                        </li>
                        <?php endforeach;?>
                    </ul>
                </fieldset>
            </div>
        </div>
        <div class="tab-pane fade" id="pills-esterni" role="tabpanel" aria-labelledby="pills-esterni-tab">
            <h2>Esterni</h2>
            <?php if(count($templateParams["esterni"])>0):?>
                <?php foreach($templateParams["esterniTitle"] as $title): ?>
                <div id="<?=$title["Descrizione"]?>-tipo">
                    <fieldset>
                        <legend><?=$title["Descrizione"]?></legend>
                        <ul>
                            <?php $n=0;
                            foreach($templateParams["esterni"] as $elemento): ?>
                                <?php if($elemento["Descrizione"]==$title["Descrizione"]):?>
                                <li>
                                    <input type="radio" id="e-<?=$elemento["IdEsterni"]?>" name="esterni-<?=$elemento["Descrizione"]?>" value="<?=$elemento["IdEsterni"]?>" 
                                    <?php if(isset($_POST["esterni-".$elemento["Descrizione"]])): ?>
                                        <?php if($elemento["IdEsterni"]==$_POST["esterni-".$elemento["Descrizione"]]):?>
                                            checked
                                        <?php endif;
                                    elseif(isset( $templateParams["selectEsterni"])):
                                        if( in_array($elemento["IdEsterni"], $templateParams["selectEsterni"])):?>
                                        checked
                                        <?php endif;
                                    elseif($n==0): ?>
                                        checked
                                    <?php 
                                    $n++;
                                    endif;?>/>
                                    <label for="e-<?=$elemento["IdEsterni"]?>">
                                            <?php if($elemento["Link_Immagine"]!=null): ?>
                                                <img src="<?=UPLOAD_DIR.$elemento["Link_Immagine"]?>" alt=""/>
                                            <?php endif; ?>
                                            <span><?=$elemento["Nome"]?></span>
                                            <span><em><?=$elemento["Prezzo"]?></em>€</span>
                                    </label>
                                </li>
                                <?php endif; ?>
                            <?php endforeach;?>
                        </ul>
                    </fieldset>
                </div>
                <?php endforeach;?>
            <?php else: ?>
                <p> esterni non disponibili</p>
            <?php endif; ?>
        </div>

        <div class="tab-pane fade" id="pills-interni" role="tabpanel" aria-labelledby="pills-interni-tab">
            <h2>Interni</h2>
            <?php if(count($templateParams["esterni"])>0):?>
                <?php foreach($templateParams["interniTitle"] as $title): ?>
                <div id="<?=$title["Descrizione"]?>-tipo">
                    <fieldset>
                        <legend><?=$title["Descrizione"]?></legend>
                        <ul>
                            <?php $n=0;
                            foreach($templateParams["interni"] as $elemento): ?>
                            <?php if($elemento["Descrizione"]==$title["Descrizione"]):?>
                                <li>
                                    <input type="radio" id="i-<?=$elemento["IdInterni"]?>" name="interni-<?=$elemento["Descrizione"]?>" value="<?=$elemento["IdInterni"]?>" 
                                    <?php if(isset($_POST["interni-".$elemento["Descrizione"]])): ?>
                                        <?php if($elemento["IdInterni"]==$_POST["interni-".$elemento["Descrizione"]]):?>
                                            checked
                                        <?php endif;
                                    elseif(isset( $templateParams["selectInterni"])):
                                        if( in_array($elemento["IdInterni"], $templateParams["selectInterni"])):?>
                                        checked
                                        <?php endif;
                                    elseif($n==0): ?>
                                        checked
                                    <?php
                                        $n++;
                                    endif;
                                    ?>/>
                                    <label for="i-<?=$elemento["IdInterni"]?>">
                                    
                                            <?php if($elemento["Link_Immagine"]!= null): ?>
                                                <img src="<?=UPLOAD_DIR.$elemento["Link_Immagine"]?>" alt=""/>
                                            <?php endif; ?>
                                            <span><?=$elemento["Nome"]?></span>
                                            <span><em><?=$elemento["Prezzo"]?></em>€</span>
                                
                                    </label>
                                </li>
                                <?php endif; ?>
                            <?php endforeach;?>
                        </ul>
                    </fieldset>
                </div>
                <?php endforeach;?>
            <?php else: ?>
                <p> interni non disponibili</p>
            <?php endif; ?>
        </div>
        <div class="tab-pane fade" id="pills-optional" role="tabpanel" aria-labelledby="pills-optional-tab">
            <div id="optional-tipo">
                <fieldset>
                    <legend>Optional</legend>
                    <?php if(count($templateParams["optional"])>0):?>
                    <ul>
                        <?php foreach($templateParams["optional"] as $elemento):?>
                        <li>
                            <input type="checkbox" id="o-<?=$elemento["IdOptional"]?>" name="optional[]" value="<?=$elemento["IdOptional"]?>"
                            <?php if(isset($_POST["optioanl"]) && $_POST["optional"].contains($elemento["IdOptional"])):?>checked<?php endif;?>
                            <?php  if(isset( $templateParams["selectOptional"]) && in_array($elemento["IdOptional"], $templateParams["selectOptional"])):?>checked<?php endif;?>/>
                            <label for="o-<?=$elemento["IdOptional"]?>">
                               
                                    <?php if($elemento["Link_Immagine"]!=null): ?>
                                        <img src="<?=UPLOAD_DIR.$elemento["Link_Immagine"]?>" alt=""/>
                                    <?php endif; ?>
                                    <span><?=$elemento["Nome"]?></span>
                                    <span><?=$elemento["Descrizione"]?></span>
                                    <span><em><?=$elemento["Prezzo"]?></em>€</span>
                               
                            </label>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php else: ?>
                        <p> optional non disponibili</p>
                    <?php endif;?>
                </fieldset>
            </div>
        </div>
    </div>
    <aside>
        <table>
            <tr>
                <th id="selezione">Selezione</th>
                <th id="prezzo">Prezzo</th>
            </tr>
            <tr>
                <th id="base" headers="selezione"><?=$templateParams["auto"][0]["Modello"]?></th>
                <th id="prezzo_base" headers="prezzo base"><em><?=$templateParams["auto"][0]["Prezzo_base"]?></em>€
                </th>
            </tr>
            <tr>
                <th id="motore" headers="selezione">Motore
                </th>
                <th id="prezzo_motore" headers="prezzo motore">
                    <em>0</em>€
                </th>
            </tr>
            <tr>
                <th id="esterni" headers="selezione">Esterni
                </th>
                <th id="prezzoEsterni" headers="prezzo esterni">
                    <em>0</em>€
                </th>
            </tr>
            <tr>
                <th id="interni" headers="selezione">Interni
                </th>
                <th id="prezzoInterni" headers="prezzo interni">
                    <em>0</em>€
                </th>
            </tr>
            <tr>
                <th id="optional" headers="selezione">Optional
                </th>
                <th id="prezzoOptional" headers="prezzo optional">
                    <em>0</em>€
                </th>
            </tr>
            <tr>
                <th id="totale" headers="selezione">Totale
                </th>
                <th id="prezzoTotale" headers="prezzo totale">
                    <em><?=$templateParams["auto"][0]["Prezzo_base"]?></em>€
                </th>
            </tr>
        </table>
        <input type="submit" value="Conferma" name="btnSubmit" />
        <?php if(isset($error) && $error!=""):?>
            <ul class="error">
                <?=$error?>
            </ul>
        <?php endif; ?>
    </aside>
</form>
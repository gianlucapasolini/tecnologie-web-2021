<div id="recap">
    <table>
        <tr>
            <th id="selezione">Selezione</th>
            <th id="prezzo">Prezzo</th>
        </tr>
        <tr>
            <th id="base" headers="selezione"><?=$modello[0]["Modello"]?></th>
            <th id="prezzo_base" headers="prezzo base"><em><?=$modello[0]["Prezzo_base"]?></em>€
            </th>
        </tr>
        <tr>
            <th id="motore" headers="selezione">Motore
            </th>
            <th id="prezzo_motore" headers="prezzo motore">
                <em><?=$motore[0]["Prezzo"]?></em>€
            </th>
        </tr>
        <tr>
            <th id="<?=$motore[0]["IdMotore"]?>" headers="selezione motore"><?=$motore[0]["Nome"]?>
            </th>
            <td id="prezzo_<?=$motore[0]["IdMotore"]?>" headers="prezzo <?=$motore[0]["IdMotore"]?>">
                <em><?=$motore[0]["Prezzo"]?></em>€
            </td>
        </tr>
        <tr>
            <th id="esterni" headers="selezione">Esterni
            </th>
            <th id="prezzo_esterni" headers="prezzo esterni">
                <em><?=$totE?></em>€
            </th>
        </tr>
        <?php foreach ($esterni as $el):?>
            <tr>
            <th id="<?=$el[0]["IdEsterni"]?>" headers="selezione"><?=$el[0]["Descrizione"]." ".$el[0]["Nome"]?>
            </th>
            <td id="prezzo_<?=$el[0]["IdEsterni"]?>" headers="prezzo <?=$el[0]["IdEsterni"]?> prezzo_esterni">
                <em><?=$el[0]["Prezzo"]?></em>€
            </td>
        </tr>
        <?php endforeach;?>
        <tr>
            <th id="interni" headers="selezione">Interni
            </th>
            <th id="prezzo_interni" headers="prezzo interni">
                <em><?=$totI?></em>€
            </th>
        </tr>
        <?php foreach ($interni as $el):?>
            <tr>
            <th id="<?=$el[0]["IdInterni"]?>" headers="selezione"><?=$el[0]["Descrizione"]." ".$el[0]["Nome"]?>
            </th>
            <td id="prezzo_<?=$el[0]["IdInterni"]?>" headers="prezzo <?=$el[0]["IdInterni"]?> prezzo_interni">
                <em><?=$el[0]["Prezzo"]?></em>€
            </td>
        </tr>
        <?php endforeach;?>
        <tr>
            <th id="optional" headers="selezione">Optional
            </th>
            <th id="prezzo_optional" headers="prezzo optional">
                <em><?=$totO?></em>€
            </th>
        </tr>
        <?php foreach ($optional as $el):?>
            <tr>
            <th id="<?=$el[0]["IdOptional"]?>" headers="selezione"><?=$el[0]["Nome"]?>
            </th>
            <td id="prezzo_<?=$el[0]["IdOptional"]?>" headers="prezzo <?=$el[0]["IdOptional"]?> prezzo_optional">
                <em><?=$el[0]["Prezzo"]?></em>€
            </td>
        </tr>
        <?php endforeach;?>
        <tr>
            <th id="totale" headers="selezione">Totale
            </th>
            <th id="prezzoTotale" headers="prezzo totale">
                <em><?=$tot?></em>€
            </th>
        </tr>
    </table>
    <form action="configuratore.php" method="get">
        <input type="hidden" name="config" value="<?=$config?>"/>
        <input type="submit" value="Modifica" name="btnSubmit" />
    </form>
    <form action="gestione_carrello.php" method="get">
        <input type="hidden" name="idAuto" value="<?=$config?>"/>
        <input type="submit" value="Aggiungi al carrello" name="btnSubmit" />
    </form>
</div>
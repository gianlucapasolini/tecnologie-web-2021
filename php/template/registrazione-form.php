		<div class="container-fluid p-0 overflow-hidden">
			<div class="row">
				<div class="col-12 col-sm-10 col-lg-8 col-xl-6 mx-auto">
				    <form action="#" method="POST" class="border m-4 p-4" enctype="multipart/form-data">
				        <h2 class="text-center pb-4">Car Shop</h2>
				        <?php if(isset($templateParams["erroreRegistrazione"])): ?>
			            <p><?php echo $templateParams["erroreRegistrazione"]; ?></p>
			            <?php endif; ?>
			            <div class="form-group row">
				        	<div class="col-12">
					        	<fieldset>
					        		<legend>Tipologia account:</legend>
									<div class="col-12 btn-group btn-group-toggle" data-toggle="buttons">
										<label class="btn btn-primary active" for="cliente">
											<input type="radio" name="options_utente" id="cliente" value="Cliente" checked/> Cliente
										</label>
										<label class="btn btn-primary" for="venditore">
											<input type="radio" name="options_utente" id="venditore" value="Venditore"/> Venditore
										</label>
										<label class="btn btn-primary" for="corriere">
											<input type="radio" name="options_utente" id="corriere" value="Corriere"/> Corriere
										</label>
									</div>
								</fieldset>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6 p-0">
					        	<div class="form-group" id="Field_Nome">
						            <label for="nome" class="col-12">Nome:</label>
						            <input type="text" id="nome" name="nome" class="form-control mx-auto col-10" placeholder="Nome" maxlength="60" required/>
					        	</div>
					        </div>
					        <div class="col-12 col-md-6 p-0">
					        	<div class="form-group" id="Field_Cognome">
						            <label for="cognome" class="col-12">Cognome:</label>
						            <input type="text" id="cognome" name="cognome" class="form-control mx-auto col-10" placeholder="Cognome" maxlength="60" required/>
						        </div>
					        </div>
							<div class="col-12 col-md-6 p-0">
					        	<div class="form-group" id="Field_CF">
						            <label for="cf" class="col-12">CF:</label>
						            <input type="text" id="cf" name="cf" class="form-control mx-auto col-10" placeholder="CF" pattern=".{16,16}" title="servono 16 caratteri" maxlength="16" required/>
						        </div>
					        </div>
					        <div class="col-12 col-md-6 p-0">
					        	<div class="form-group" id="Field_PIVA">
						            <label for="P_IVA" class="col-12">P_IVA:</label>
						            <input type="text" id="P_IVA" name="P_IVA" class="form-control mx-auto col-10" placeholder="P_IVA" pattern=".{11,11}" title="servono 11 caratteri" maxlength="11" required/>
						        </div>
					        </div>
					        <div class="col-12 col-md-6 p-0">
					        	<div class="form-group" id="Field_Email">
						            <label for="email" class="col-12">Email:</label>
						            <input type="email" id="email" name="email" class="form-control mx-auto col-10" placeholder="Email" maxlength="140" required/>
						        </div>
					        </div>
					        <div class="col-12 col-md-6 p-0">
					        	<div class="form-group" id="Field_Telefono">
						            <label for="tel" class="col-12">Telefono:</label>
						            <input type="tel" id="tel" name="tel" class="form-control mx-auto col-10" placeholder="Telefono" maxlength="16" required />
						        </div>
					        </div>
					        <div class="col-12 col-md-6 p-0">
					        	<div class="form-group" id="Field_NomUtente">
						            <label for="nomeutente" class="col-12">Nome Utente:</label>
						            <input type="text" id="nomeutente" name="nomeutente" class="form-control mx-auto col-10" placeholder="Utente" maxlength="80" required/>
						        </div>
					        </div>
					        <div class="col-12 col-md-6 p-0">
					        	<div class="form-group" id="Field_Password">
						            <label for="password" class="col-12">Password:</label>
						            <div class="input-group mx-auto col-10 px-0">
							            <input type="password" id="password" name="password" class="form-control" placeholder="Password" aria-describedby="passwordHelpBlock" maxlength="30" pattern="(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*?[0-9])(?=.*[A-Z]).{8,30}" required/>
										<div class="input-group-prepend">
					                        <button type="button" id="showPassword" class="btn btn-light rounded">
					                        	<em aria-hidden="true" class="fas fa-eye" title="mostra password"></em>
					                        </button>
					                    </div>
					                </div>
									<div id="passwordHelpBlock" class="col-12 p-0 bg-light mt-3">
										<h3 class="p-2 text-center">Password contiene:</h3>
										<div class="col-12 p-0 pl-4">
											<p id="letter" class="invalid">Una lettera <strong>minuscola</strong> </p>
											<p id="capital" class="invalid">Una lettera <strong>MAIUSCOLA</strong> </p>
											<p id="number" class="invalid">Una <strong>numero</strong></p>
											<p id="specChar" class="invalid"><strong>Carattere speciale</strong> (!@#$%^&*)</p>
											<p id="length" class="invalid"><strong>8 - 30 caratteri</strong></p>
										</div>
									</div>
						        </div>
					        </div>
					        <div class="col-12 col-md-6 p-0">
					        	<div class="form-group" id="Field_ConfPassowrd">
						            <label for="Conf_password" class="col-12">Conferma Password:</label>
						            <div class="input-group mx-auto col-10 px-0">
							            <input type="password" id="Conf_password" name="Conf_password" class="form-control" placeholder="Password" aria-describedby="passwordHelpBlockCheck" pattern=".{8,30}" title="servono 8-30 caratteri" maxlength="30" required/>
							            <div class="input-group-prepend">
					                        <button type="button" id="showConfPassword" class="btn btn-light rounded">
					                        	<em aria-hidden="true" class="fas fa-eye" title="mostra conferma password"></em>
					                        </button>
					                    </div>
					                </div>
						            <div id="passwordHelpBlockCheck" class="form-text text-danger">
									  Le password devono corrispondere.
									</div>
						        </div>
					        </div>
					        <div class="col-12 p-0">
					        	<div class="form-group" id="Field_CasaAsta">
						            <label for="casa_asta" class="col-12">Casa d'asta?</label>
						            <input name="casa_asta" type="hidden" value="0" />
						            <input type="checkbox" id="casa_asta" name="casa_asta" value="1" class="form-control col-2 ml-3"/>
						        </div>
					        </div>
					        <div class="col-12 col-md-6 p-0">
					        	<div class="form-group" id="Field_Logo">
						            <label for="Logo_Azienda" class="col-12">Logo:</label>
						            <input type="file" name="Logo_Azienda" id="Logo_Azienda" class="form-control border-0 ml-2" required />
						        </div>
					        </div>
					        <div class="col-12 col-md-6 p-0">
					        	<div class="form-group" id="Field_ImmCopertina">
						            <label for="ImgCopertina_Azienda" class="col-12">Immagine Copertina:</label>
						            <input type="file" name="ImgCopertina_Azienda" id="ImgCopertina_Azienda" class="form-control border-0 ml-2" required />
						        </div>
					        </div>
					        <div class="form-group pt-2 col-12 text-right">
					            <input type="submit" name="submit" value="Registrati" class="btn btn-primary"/>
					        </div>
				        </div>
				    </form>
				</div>
			</div>
		</div>
<div class="container-fluid mx-auto resizeContainer">
    <h2> Inserisci il commento </h2>
    <form action="visualizzazione_auto.php" method="GET">
        <label for="numStelline">Seleziona il numero di stelle</label>
        <select class="form-select form-control mb-2" name="numStelline" id="numStelline" aria-label="Seleziona il numero di stelline">
            <option value="1">Una stella</option>
            <option value="2">Due stelle</option>
            <option value="3">Tre stelle</option>
            <option value="4">Quattro stelle</option>
            <option value="5">Cinque stelle</option>
        </select>
        <input type="hidden" aria-hidden="true" name="idAuto" id="idAuto" value="<?php echo $templateParams["idAuto"]; ?>"/>
        <div class="form-floating">
            <label for="commentoInviato">Recensione</label>
            <textarea class="form-control" rows="10" placeholder="Lascia qui il commento" id="commentoInviato" name="commentoInviato"></textarea>
        </div>
        <input type="submit" class="btn btn-primary" value="Invia"/>
    </form>
</div>
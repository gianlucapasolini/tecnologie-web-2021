<div class="container-fluid" id="containerCarrello">
<?php 
if(isset($templateParams["auto"]) && !isset($templateParams["carrelloVuoto"])):
    foreach($templateParams["auto"] as $autoConfigurata):
?>
<div class="container-fluid p-0 mb-2 overflow-hidden text-center" style="border: 2px solid black; border-radius: 5px; ">
    <h2> <?php echo $autoConfigurata["Modello"]; ?> </h2>
    <div class="row m-2">
        <div class="col col-sm-6 text-center">
            <p> Costruttore: </p>
            <p> Modello: </p>
            <p> Costo totale: </p>
        </div>
        <div class="col col-sm-6 text-center">
            <p> <?php echo $autoConfigurata["Nome_Utente"]; ?></p>
            <p> <?php echo $autoConfigurata["Modello"]; ?> </p>
            <p> <?php echo $autoConfigurata["PrezzoTotale"]; ?> </p>
        </div>
    </div>


    <ul class="nav nav-tabs" id="tab<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" role="tablist">
    <li class="nav-item">
        <a aria-hidden="false" class="nav-link active" id="optional-tab-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" data-toggle="tab" href="#optional-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" role="tab" aria-controls="optional-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" aria-selected="true">Optional</a>
    </li>
    <li class="nav-item">
        <a aria-hidden="false" class="nav-link" id="interni-tab-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" data-toggle="tab" href="#interni-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" role="tab" aria-controls="interni-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" aria-selected="false">Interni</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="esterni-tab-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" data-toggle="tab" href="#esterni-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" role="tab" aria-controls="esterni-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" aria-selected="false">Esterni</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="motore-tab-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" data-toggle="tab" href="#motore-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" role="tab" aria-controls="motore-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" aria-selected="false">Motore</a>
    </li>
    </ul>
    <div class="tab-content" id="tabContent<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>">
        <div class="tab-pane fade show active" id="optional-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" role="tabpanel" aria-labelledby="optional-tab-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Optional</th>
                <th scope="col">Costo</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($autoConfigurata["opt"] as $opt):?>
            <tr>
                <td> <?php echo $opt["Nome"]; ?> </td>
                <td> <?php echo $opt["Prezzo"]; ?> </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        </div>
        <div class="tab-pane fade" id="interni-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" role="tabpanel" aria-labelledby="interni-tab-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Interni</th>
                    <th scope="col">Costo</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($autoConfigurata["interni"] as $interni):?>
                <tr>
                    <td> <?php echo $interni["Nome"]; ?> </td>
                    <td> <?php echo $interni["Prezzo"]; ?> </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="esterni-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" role="tabpanel" aria-labelledby="esterni-tab-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Esterni</th>
                    <th scope="col">Costo</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($autoConfigurata["esterni"] as $esterni):?>
                <tr>
                    <td> <?php echo $esterni["Nome"]; ?> </td>
                    <td> <?php echo $esterni["Prezzo"]; ?> </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="motore-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>" role="tabpanel" aria-labelledby="motore-tab-<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>">
        <table class="table">
                <thead>
                <tr>
                    <th scope="col">Motore</th>
                    <th scope="col">Costo</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> <?php echo $autoConfigurata["motore"]["Nome"]; ?> </td>
                        <td> <?php echo $autoConfigurata["motore"]["Prezzo"]; ?> </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <?php if(isset($templateParams["carrello"]) && isset($_SESSION["CF"])): ?>
        <a class="btn btn-outline-danger mb-2" href="gestione_carrello.php?Elimina=<?php echo $autoConfigurata["IdAutoConfigurata"]; ?>"> Elimina dal carrello </a>
    <?php endif; ?>
</div>

<?php endforeach;
    else:?>

    <h2> Carrello vuoto </h2>

<?php
    endif;?>
    <?php if(isset($templateParams["idCarrello"]) && !isset($templateParams["carrelloVuoto"])): ?>
        <a href="pagamento.php?IdOrdine=<?php echo $templateParams["idCarrello"]; ?>" class="btn btn-primary"> Procedi col pagamento </a>
    <?php endif; ?>
    <?php if(!isset($_SESSION["CF"]) && !isset($templateParams["carrelloVuoto"])): ?>
        <a href="login.php" class="btn btn-primary"> Accedi per pagare </a>
    <?php endif; ?>
</div>
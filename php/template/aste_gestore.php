<div class="container-fluid" id="containerGestoreAste">
    <h2 class="mb-5"> Gestione aste </h2>
    <?php foreach($templateParams["aste"] as $asta): ?>
    <div class="row mb-2">
        <div class="col col-sm-6">
            <h3> <?php echo $asta["Modello"]; ?> </h3>
        </div>
        <div class="col col-sm-6 text-right">
            <form method="GET" action="aste.php">
                <input type="hidden" name="tipologia" value="Eliminazione">
                <input type="hidden" name="id" value="<?php echo $asta["IdAsta"]; ?>">
                <input type="submit" class="btn btn-outline-danger" value="Chiudi"/>
            </form>
        </div>
    </div>
    <?php endforeach; ?>
    

    <div class="row mx-auto">
        <form method="GET" action="aste.php">
            <input type="hidden" name="tipologia" value="Inserimento">
            <input type="submit" class="btn btn-primary" value="Aggiungi asta"/>
        </form>
    </div>

    <?php 
        if(isset($templateParams["msg"]) && $templateParams["color"] == "success"):
    ?>
    <div class="alert alert-success" role="alert">
        <?php echo $templateParams["msg"] ?>
    </div>
    <?php endif; ?>
    <?php 
        if(isset($templateParams["msg"]) && $templateParams["color"] == "failed"):
    ?>
        <div class="alert alert-primary" role="alert">
            <?php echo $templateParams["msg"] ?>
        </div>
    <?php endif; ?>
</div>
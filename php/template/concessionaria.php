        <!-- Page Content -->
        <div class="container-fluid p-0 overflow-hidden">
            <div class="col-12 col-md-10 col-xl-8 mx-auto">
                <div class="container-fluid p-0">
                    <h2 class="text-center my-3"> Le mie Auto </h2>
                    <div class="row">                            
                        <?php foreach($templateParams["mieAuto"] as $singola_auto): ?>
                        <div class="col-12 col-sm-6 col-lg-4 mx-auto text-center">
                            <form action="configuratore_venditore.php" method="GET" class="text-center">
                                <input type="hidden" aria-hidden="true" name="modello" value="<?php echo $singola_auto["IdAuto"]; ?>" />
                                <input type="image" id="auto_<?php echo $singola_auto["IdAuto"]; ?>" name="fotoModello" alt="auto <?php echo $singola_auto["Modello"]; ?>" src="<?php echo UPLOAD_DIR.$singola_auto['Link_immagine']; ?>"/>
                                <label for="auto_<?php echo $singola_auto["IdAuto"]; ?>" id="title_Model_<?php echo $singola_auto["IdAuto"]; ?>"> <?php echo $singola_auto["Modello"]; ?> </label>
                            </form>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>                    
            </div>
        </div>
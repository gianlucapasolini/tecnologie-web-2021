		<div class="container-fluid p-0 overflow-hidden">
			<div class="row" id="Sezione_Indirizzo">
			    <div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4 mx-auto">
			    	<h2 class="text-center" id="Titolo_Fatturazione">Recapiti</h2>
		            <form action="#" method="POST" class="bg-white border mx-4 p-4">
		            	<div class="form-group row">
		            		<label for="listSpedizioni">Indirizzo:</label>
			            	<select class="custom-select" id="listSpedizioni" name="listSpedizioni">
								<option value="Nuovo" selected>Nuovo</option>
								<?php foreach($templateParams["spedizioni"] as $spedizione): ?>
								<option value="<?php echo $spedizione["IdSpedizione"]; ?>"><?php echo $spedizione["Via"]."; ".$spedizione["N_Civico"]."; ".$spedizione["Citta"]."; ".$spedizione["Provincia"]."; ".$spedizione["CAP"]; ?></option>
				                <?php endforeach; ?>
							</select>
						</div>
		            	<?php if(isset($templateParams["erroreSpedizione"])): ?>
			            <p><?php echo $templateParams["erroreSpedizione"]; ?></p>
			            <?php endif; ?>
		                <div class="form-group row">
		                    <label for="via" class="col-12">Via:</label>
		                    <div class="col-1"></div>
		                    <input type="text" id="via" name="via" class="form-control col-10" maxlength="200" placeholder="Via" required/>
		                </div>
		                <div class="form-group row">
		                    <label for="N_Civico" class="col-12">N_Civico:</label>
		                    <div class="col-1"></div>
		                    <input type="number" id="N_Civico" name="N_Civico" class="form-control col-10" min="1" max="50000" placeholder="N_Civico" required/>
		                </div>
		                <div class="form-group row">
		                    <label for="Citta" class="col-12">Città:</label>
		                    <div class="col-1"></div>
		                    <input type="text" id="Citta" name="Citta" class="form-control col-10" maxlength="100" placeholder="Città" required/>
		                </div>
		                <div class="form-group row">
		                    <label for="Provincia" class="col-12">Provincia:</label>
		                    <div class="col-1"></div>
		                    <input type="text" id="Provincia" name="Provincia" class="form-control col-10" maxlength="100" placeholder="Provincia" required/>
		                </div>
		                <div class="form-group row">
		                    <label for="CAP" class="col-12">CAP:</label>
		                    <div class="col-1"></div>
		                    <input type="number" id="CAP" name="CAP" class="form-control col-10" min="1" max="99999" placeholder="CAP" required/>
		                </div>
		                <div class="form-group text-right mb-0">
		                    <input type="submit" name="submit" value="Aggiungi" class="btn btn-primary"/>
		                </div>
		            </form>
			    </div>
			</div>
		</div>
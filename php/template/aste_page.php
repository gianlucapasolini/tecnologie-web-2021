<div class="container-fluid text-center">
    <div class="row align-items-center">
      <div class="col col-sm-12">
        
          <?php
            if($templateParams["infoAsta"][0]["Venduto"] == 0):
          ?>
            <p style="color: green;"> Aperto </p> 
          <?php else: ?>
            <p style="color: red;"> Chiuso </p>
            <?php endif; ?>
      </div>
    </div>
    <div class="row align-items-center">
          <div class="col col-sm-12">
              <p id="prezzoAttuale" aria-hidden="false"> Offerta corrente: </p>
              <p id="pPrezzoAttuale"> <?php echo $templateParams["infoAsta"][0]["prezzoAttuale"]; ?> €</p>
          </div>
    </div>
    <form>
        <input type="hidden" id="cf" name="cf" value="<?php if(isset($_SESSION["CF"])){echo $_SESSION["CF"];}else{echo "";} ?>"/>
        <input type="hidden" id="id" name="id" value="<?php echo $templateParams["infoAsta"][0]["IdAsta"]; ?>"/>
      </form>
    <?php
            if($templateParams["infoAsta"][0]["Venduto"] == 0):
          ?>
    <div class="container-fluid mx-auto formFiltra">
      <div class="row">
          <div class="col-6 col-sm-6 col-lg-6 mb-2">
                <button class="btn bg-light" data-toggle="modal" data-target="#offerta" style="width: 150px;"> Fai un offerta </button>
            </div>
          <div class="col-6 col-sm-6 col-lg-6 mb-2">
          <button class="btn bg-light" id="btnRefresh" style="width: 150px;"> <em class="fas fa-sync" style="font-size: 27px;"></em> </button>
          </div>
      </div>
    </div>
    <?php endif;?>
  
  <div class="container-fluid" id="desc">
    <h2 class="mx-auto"> Descrizione </h2>
    <p class="mx-auto" id="descrizioneAuto">
        <?php echo $templateParams["infoAsta"][0]["Descrizione"]; ?>
    </p>
  </div>

  <div class="row">


    <div class="col-sm-12 col-md-6 col-lg-6">
      <div class="container-fluid" id="chart">
        <h2 class="mx-auto"> Andamento </h2>
        <canvas aria-label="Grafico sull'andamento dei rilanci" aria-hidden="false" id="graficoAndamento"></canvas>
      </div>
    </div>

    <div class="col-sm-12 col-md-6 col-lg-6">
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="container-fluid">
          <?php if($templateParams["infoAsta"][0]["Suono"] != NULL): ?>
            <h2 class="mx-auto"> Suono </h2>
            <audio controls>
                <source src="upload/<?php echo $templateParams["infoAsta"][0]["Suono"]; ?>" type="audio/mpeg"/>
                Il tuo browser non supporta la riproduzione audio
            </audio>
            <?php else: ?>
              <h2 class="mx-auto"> Suono non disponibile </h2>
            <?php endif; ?>
          </div>
        </div>
        <div class="col-sm-12 col-md-12">
          <div class="container-fluid">
          <?php if($templateParams["infoAsta"][0]["Link_video"] != NULL): ?>
            <h2 class="mx-auto"> Video </h2>
            <video controls id="videoPlayer">
                <source src="upload/<?php echo $templateParams["infoAsta"][0]["Link_video"]; ?>" type="video/mp4">
                Il tuo browser non supporta la riproduzione di video
            </video>
            <?php else: ?>
              <h2 class="mx-auto"> Video non disponibile </h2>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
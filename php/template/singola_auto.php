
<div class="container-fluid text-center">

    <img alt="Immagine auto <?php echo $templateParams["auto"]["Modello"]; ?>" class="resizeContainer" src="upload/<?php echo $templateParams["auto"]["Link_immagine"]; ?>"/>

    

    <div class="alert alert-primary resizeContainer mx-auto" role="alert">
    <p> <?php echo $templateParams["auto"]["Descrizione"]; ?>  </p>
        <p> Prezzo base </p>
        <p class="font-weight-bold text-info"> <?php echo $templateParams["auto"]["Prezzo_base"]; ?> € </p>

        <form method="GET" action="configuratore.php">
            <input type="hidden" name="idAuto" id="idAuto" value="<?php echo $templateParams["auto"]["IdAuto"]; ?>"/>
            <input type="submit" value="Configura" class="btn btn-primary"/>
         </form>
    </div>

    <div class="container-fluid mb-3 resizeContainer">
        <?php
            //$i = 0;
            //for($i=0; $i <4; $i++):
                $i = 0;
            foreach($templateParams["immagini"] as $immagine):
                if($i % 2 == 0):
        ?>
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mb-2">
                <img class="rounded-lg" alt="Immagine <?php echo $i; ?>" style="width:100%;" src="upload/<?php echo $immagine["LinkImmagine"]; ?>"/>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto mb-2">
                <p> <?php echo $immagine["Descrizione"]; ?>
                  </p>
            </div>

        </div>
        <?php
            else:
        ?>
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  my-auto mb-2">
                <p> <?php echo $immagine["Descrizione"]; ?></p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mb-2">
                <img class="rounded-lg" alt="Immagine <?php echo $i; ?>" style="width:100%;" src="upload/<?php echo $immagine["LinkImmagine"]; ?>"/>
            </div>

        </div>
        <?php
            endif;
            $i++;
        endforeach;
        ?>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6 my-auto">
            <div class="container-fluid mb-3">
            <?php if($templateParams["auto"]["Suono"] != NULL): ?>
                <h2> Audio </h2>
                <audio controls>
                    <source src="upload/<?php echo  $templateParams["auto"]["Suono"]; ?>" type="audio/mpeg"/>
                    Il tuo browser non supporta la riproduzione audio
                </audio>
            <?php else: ?>
                <h2> Audio non disponibile</h2>
            <?php endif; ?>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 my-auto">
            <div class="container-fluid mb-3">
            <?php if($templateParams["auto"]["Link_video"] != NULL): ?>
                <h2> Video </h2>
                <video controls id="videoPlayer">
                    <source src="upload/<?php echo $templateParams["auto"]["Link_video"]; ?>" type="video/mp4">
                    Il tuo browser non supporta la riproduzione di video
                </video>
                <?php else: ?>
                <h2> Video non disponibile</h2>
            <?php endif; ?>
            </div>
        </div>
    </div>


    
    





    <div class="container-fluid">
        <h2>
        <?php if(isset($templateParams["NoComments"])){
            echo "Non ci sono commenti";
        } else {
            echo "Commenti";
        }
        ?>
        </h2>
        <?php if(!isset($templateParams["NoComments"])):  ?>

        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-2 mx-auto">
                        <p>5</p>
                    </div>
                    <div class="col-10">
                        <div class="progress mb-2">
                        
                            <div class="progress-bar bg-success" role="progressbar" style="width:<?php echo $templateParams["5stelline"]["perc"]; ?>%" aria-valuenow="<?php echo $templateParams["5stelline"]["num"]; ?>" aria-valuemin="0" aria-valuemax="<?php echo $templateParams["totaleCommenti"]; ?>"></div>
                        
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2 mx-auto">
                        <p>4</p>
                    </div>
                    <div class="col-10">
                        <div class="progress mb-2">
                        
                            <div class="progress-bar bg-primary" role="progressbar" style="width:<?php echo $templateParams["4stelline"]["perc"]; ?>%" aria-valuenow="<?php echo $templateParams["4stelline"]["num"]; ?>" aria-valuemin="0" aria-valuemax="<?php echo $templateParams["totaleCommenti"]; ?>"></div>
                        
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2 mx-auto">
                        <p>3</p>
                    </div>
                    <div class="col-10">
                        <div class="progress mb-2">
                        
                            <div class="progress-bar bg-info" role="progressbar" style="width:<?php echo $templateParams["3stelline"]["perc"]; ?>%" aria-valuenow="<?php echo $templateParams["3stelline"]["num"]; ?>" aria-valuemin="0" aria-valuemax="<?php echo $templateParams["totaleCommenti"]; ?>"></div>
                        
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2 mx-auto">
                        <p>2</p>
                    </div>
                    <div class="col-10">
                        <div class="progress mb-2">
                        
                            <div class="progress-bar bg-warning" role="progressbar" style="width:<?php echo $templateParams["2stelline"]["perc"]; ?>%" aria-valuenow="<?php echo $templateParams["2stelline"]["num"]; ?>" aria-valuemin="0" aria-valuemax="<?php echo $templateParams["totaleCommenti"]; ?>"></div>
                        
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2 mx-auto">
                        <p>1</p>
                    </div>
                    <div class="col-10">
                        <div class="progress mb-2">
                        
                            <div class="progress-bar bg-danger" role="progressbar" style="width:<?php echo $templateParams["1stelline"]["perc"]; ?>%" aria-valuenow="<?php echo $templateParams["1stelline"]["num"]; ?>" aria-valuemin="0" aria-valuemax="<?php echo $templateParams["totaleCommenti"]; ?>"></div>
                        
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="container-fluid">

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="cinquestelle-tab" data-toggle="tab" href="#cinquestelle" role="tab" aria-controls="cinquestelle" aria-selected="true">5 stelle</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="quattrostelle-tab" data-toggle="tab" href="#quattrostelle" role="tab" aria-controls="quattrostelle" aria-selected="false">4 stelle</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="trestelle-tab" data-toggle="tab" href="#trestelle" role="tab" aria-controls="trestelle" aria-selected="false">3 stelle</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="duestelle-tab" data-toggle="tab" href="#duestelle" role="tab" aria-controls="duestelle" aria-selected="false">2 stelle</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="unastella-tab" data-toggle="tab" href="#unastella" role="tab" aria-controls="unastella" aria-selected="false">1 stelle</a>
                    </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">

                        <div class="tab-pane fade show active" id="cinquestelle" role="tabpanel" aria-labelledby="cinquestelle-tab">
                            <?php foreach($templateParams["5stelline"]["commenti"] as $commento): ?>
                            <div class="media">
                                <img src="upload/stig.png" style="width:20px; height:auto;" class="mr-1" alt="iconaCliente">
                                <div class="media-body">
                                    <h5 class="mt-0"><?php echo $commento["Nome_Utente"]; ?></h5>
                                    <?php echo $commento["Testo"]; ?>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>


                        <div class="tab-pane fade" id="quattrostelle" role="tabpanel" aria-labelledby="quattrostelle-tab">
                            <?php foreach($templateParams["4stelline"]["commenti"] as $commento): ?>
                            <div class="media">
                                <img src="upload/stig.png" style="width:20px; height:auto;" class="mr-1" alt="iconaCliente">
                                <div class="media-body">
                                    <h5 class="mt-0"><?php echo $commento["Nome_Utente"]; ?> </h5>
                                    <?php echo $commento["Testo"]; ?>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>

                        <div class="tab-pane fade" id="trestelle" role="tabpanel" aria-labelledby="trestelle-tab">
                        <?php foreach($templateParams["3stelline"]["commenti"] as $commento): ?>
                            <div class="media">
                                <img src="upload/stig.png" style="width:30px; height:auto;" class="mr-1" alt="iconaCliente">
                                <div class="media-body">
                                    <h5 class="mt-0"><?php echo $commento["Nome_Utente"]; ?></h5>
                                    <?php echo $commento["Testo"]; ?>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>

                        <div class="tab-pane fade" id="duestelle" role="tabpanel" aria-labelledby="duestelle-tab">
                            <?php foreach($templateParams["2stelline"]["commenti"] as $commento): ?>
                            <div class="media">
                                <img src="upload/stig.png" style="width:30px; height:auto;" class="mr-1" alt="iconaCliente">
                                <div class="media-body">
                                    <h5 class="mt-0"><?php echo $commento["Nome_Utente"]; ?> </h5>
                                    <?php echo $commento["Testo"]; ?>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>

                        <div class="tab-pane fade" id="unastella" role="tabpanel" aria-labelledby="unastella-tab">
                            <?php foreach($templateParams["1stelline"]["commenti"] as $commento): ?>
                            <div class="media">
                                <img src="upload/stig.png" style="width:30px; height:auto;" class="mr-1" alt="iconaCliente">
                                <div class="media-body">
                                    <h5 class="mt-0"><?php echo $commento["Nome_Utente"]; ?> </h5>
                                    <?php echo $commento["Testo"]; ?>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>

                    </div>

                    <?php endif; ?>
                </div>
            </div>


        </div>

        
    
    </div>
    <div class="container-fluid mt-3">
        <div class="row text-center">
            <div class="col-12">
        <?php if(isset($templateParams["haCommentato"])): ?>
            <?php if($templateParams["haCommentato"] == "no"): ?>
                <a class="btn btn-primary" href="visualizzazione_auto.php?commento=...&idAuto=<?php echo $templateParams["auto"]["IdAuto"]; ?>"> Inserisci un commento </a>
            <?php endif; ?>
        <?php endif; ?>
            </div>
        </div>
    </div>
</div>
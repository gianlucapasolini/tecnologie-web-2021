        <!-- Page Content -->
        <div class="container-fluid p-0 overflow-hidden">
            <div class="row">
                <div class="col-10 col-md-8 col-lg-6 col-xl-4 mt-3 mx-auto">
                    <form action="visualizzazione_auto.php" method="GET">
                        <div class="input-group m-0">
                            <label class="control-label " for="tableSearch" hidden>Ricerca</label>
                            <input class="form-control rounded" id="tableSearch" type="text" placeholder="Ricerca modello macchina..." autocomplete="off" />
                            <div class="input-group-prepend">
                                <button type="submit" name="submitSearch" class="btn btn-primary rounded" aria-label="Trova modello selezionato">
                                    <em aria-hidden="true" class="fas fa-search" title="Trova modello selezionato"></em>
                                </button>
                            </div>
                        </div>
                        <div class="form-group" id="Elenco_macchina_ricerca">
                            <label for="idAuto" hidden>Scelta Modello</label>
                            <select class="custom-select form-control bg-light rounded" name="idAuto" id="idAuto" data-livesearch="true">
                            </select>
                        </div>                    
                    </form>
                </div>
            </div>
            <div class="row col-12 col-md-10 mx-auto" id="main_content">
                <div class="col-12 col-xl-5 p-0 my-auto">
                    <div id="carouselGeneralCar" class="carousel slide p-2 my-2" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselGeneralCar" data-slide-to="0" class="active"></li>
                            <?php for ($i=1; $i < count($templateParams["slider_auto"]); $i++): ?>
                            <li data-target="#carouselGeneralCar" data-slide-to="<?php echo $i; ?>"></li>
                            <?php endfor; ?>
                        </ol>
                        <div class="carousel-inner">
                            <?php foreach($templateParams["slider_auto"] as $auto): ?>
                            <div class="carousel-item">
                                <a href="visualizzazione_auto.php?idAuto=<?php echo $auto['IdAuto']; ?>">
                                    <img src="<?php echo UPLOAD_DIR.$auto['Link_immagine']; ?>" class="rounded m-auto d-block w-100 macchinaCarouselHome" alt="Immagine modello macchina: <?php echo $auto['Modello']; ?>" />
                                </a>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <a class="carousel-control-prev removeOpacity" href="#carouselGeneralCar" role="button" data-slide="prev">
                            <em class="fas fa-arrow-alt-circle-left iconCarousel" aria-hidden="true"></em>
                            <span class="sr-only">Precedente</span>
                        </a>
                        <a class="carousel-control-next removeOpacity" href="#carouselGeneralCar" role="button" data-slide="next">
                            <em class="fas fa-arrow-alt-circle-right iconCarousel" aria-hidden="true"></em>
                            <span class="sr-only">Prossima</span>
                        </a>
                    </div> <!-- Carosel -->                     
                </div>
                <div class="col-12 col-xl-7 p-0 my-auto">
                    <div class="container-fluid p-0">
                        <h2 class="text-center my-3"> Venditori </h2>
                        <div class="row">                            
                            <?php foreach($templateParams["venditori"] as $venditore): ?>
                            <div class="col-12 col-sm-6 col-md-4 mx-auto">
                                <form action="venditore.php" method="GET" class="text-center">
                                    <input type="hidden" aria-hidden="true" name="venditore" value="<?php echo $venditore["Nome_Utente"]; ?>" />
                                    <input type="image" id="logo:<?php echo $venditore["Nome_Utente"]; ?>" name="logo" alt="Logo <?php echo $venditore["Nome_Utente"]; ?>" src="<?php echo UPLOAD_DIR.$venditore['Link_logo']; ?>"/>
                                    <label for="logo:<?php echo $venditore["Nome_Utente"]; ?>" id="<?php echo $venditore["Nome_Utente"]; ?>"> <?php echo $venditore["Nome_Utente"]; ?> </label>
                                </form>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
<div class="container-fluid mx-auto resizeContainer">
    <h2> Resoconto delle aste </h2>
    <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Asta</th>
            <th scope="col">Prezzo finale</th>
            <th scope="col">Vincitore</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 0; foreach($templateParams["asteChiuse"] as $asta): ?>
            <tr>
                <th scope="row"><?php echo $i; $i++;?></th>
                <td><?php echo $asta["Modello"]; ?></td>
                <td><?php echo $asta["prezzoAttuale"]; ?></td>
                <td><?php echo $asta["Vincitore"]; ?></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
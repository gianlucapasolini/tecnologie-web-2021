        <div class="container-fluid p-0 overflow-hidden">
	        <div class="row my-3">
	        	<span id="Tipologia_Utente" hidden><?php echo $_SESSION["tipo"] ?></span>
	        	<div class="col-6 text-right my-auto">
					<h2>Vuoi Uscire?</h2>
				</div>
			    <div class="col-6 my-auto">
			    	<form action="#" method="GET">
				    	<div class="form-group text-left">
		                    <input type="submit" name="LogOut" value="Logout" class="btn btn-danger"/>
		                </div>
	        		</form>
			    </div>
	        </div>
	        <div class="row">
		        <div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4 mx-auto">
			        <div class="my-3" id="Sezione_Indirizzo">
					    <div class="col-12">
					    	<h2 class="text-center" id="Titolo_Fatturazione">Fatturazione</h2>
					    	<h2 class="text-center" id="Titolo_IndirizzoNegozio">Indirizzo Negozio</h2>
				            <form action="#" method="POST" class="bg-white border mx-4 p-4">
				            	<?php if(isset($templateParams["erroreIndirizzo"])): ?>
					            <p><?php echo $templateParams["erroreIndirizzo"]; ?></p>
					            <?php endif; ?>
				                <div class="form-group row">
				                    <label for="via" class="col-12">Via:</label>
				                    <div class="col-1"></div>
				                    <input type="text" id="via" name="via" class="form-control col-10" maxlength="200" placeholder="<?php 
				                    	if ($_SESSION["tipo"] != "CORRIERE" && $templateParams["Indirizzo"]["Via"] != NULL){
				                    		echo $templateParams["Indirizzo"]["Via"];
				                    	} else{
				                    		echo "Via";
				                    	} ?>" required/>
				                </div>
				                <div class="form-group row">
				                    <label for="N_Civico" class="col-12">N_Civico:</label>
				                    <input type="number" id="N_Civico" name="N_Civico" class="form-control col-10 mx-auto" min="1" max="50000" placeholder="<?php 
				                    	if ($_SESSION["tipo"] != "CORRIERE" && $templateParams["Indirizzo"]["N_Civico"] != NULL){
				                    		echo $templateParams["Indirizzo"]["N_Civico"];
				                    	} else{
				                    		echo "N_Civico";
				                    	} ?>" required/>
				                </div>
				                <div class="form-group row">
				                    <label for="Citta" class="col-12">Città:</label>
				                    <input type="text" id="Citta" name="Citta" class="form-control col-10 mx-auto" maxlength="100" placeholder="<?php 
				                    	if ($_SESSION["tipo"] != "CORRIERE" && $templateParams["Indirizzo"]["Citta"] != NULL){
				                    		echo $templateParams["Indirizzo"]["Citta"];
				                    	} else{
				                    		echo "Città";
				                    	} ?>" required/>
				                </div>
				                <div class="form-group row">
				                    <label for="Provincia" class="col-12">Provincia:</label>
				                    <input type="text" id="Provincia" name="Provincia" class="form-control col-10 mx-auto" maxlength="100" placeholder="<?php 
				                    	if ($_SESSION["tipo"] != "CORRIERE" && $templateParams["Indirizzo"]["Provincia"] != NULL){
				                    		echo $templateParams["Indirizzo"]["Provincia"];
				                    	} else{
				                    		echo "Provincia";
				                    	} ?>" required/>
				                </div>
				                <div class="form-group row">
				                    <label for="CAP" class="col-12">CAP:</label>
				                    <input type="number" id="CAP" name="CAP" class="form-control col-10 mx-auto" min="1" max="99999" placeholder="<?php 
				                    	if ($_SESSION["tipo"] != "CORRIERE" && $templateParams["Indirizzo"]["CAP"] != NULL){
				                    		echo $templateParams["Indirizzo"]["CAP"];
				                    	} else{
				                    		echo "CAP";
				                    	} ?>" required/>
				                </div>
				                <div class="form-group text-right mb-0">
				                    <input type="submit" name="submit" value="Invia" class="btn btn-primary"/>
				                </div>
				            </form>
					    </div>
					</div>
				</div>
				<div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4 mx-auto">
					<div class="m-3" id="Gestione_Foto_Venditore">
						<div class="col-12">
							<div class="col-12 text-center">
								<h2>Gestione Foto Venditore</h2>				
							</div>
							<form action="#" method="POST" class="bg-white border p-4" enctype="multipart/form-data">
								<?php if(isset($templateParams["erroreSendImages"])): ?>
					            <p><?php echo $templateParams["erroreSendImages"]; ?></p>
					            <?php endif; ?>
				                <div class="form-group row">
						            <label for="Logo_Azienda" class="col-12">Logo:</label>
						            <input type="file" name="Logo_Azienda" id="Logo_Azienda" class="form-control border-0" />
						        </div>
						        <div class="form-group row">
						            <label for="ImgCopertina_Azienda" class="col-12">Immagine Copertina:</label>
						            <input type="file" name="ImgCopertina_Azienda" id="ImgCopertina_Azienda" class="form-control border-0" />
						        </div>
				                <div class="form-group text-right mb-0">
				                    <input type="submit" name="aggiorna" value="Aggiorna foto" class="btn btn-primary"/>
				                </div>
				            </form>
				        </div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4 mx-auto">
				<div class="row my-3" id="Gestione_Ordini_Corriere">
					<div class="col-12">
						<div class="col-12 mb-3 text-center">
							<h2>Gestione stato ordini</h2>				
						</div>
						<form action="#" method="POST" class="bg-white border p-4 col-12">
							<?php if(isset($templateParams["erroreUpdateOrdini"])): ?>
				            <p><?php echo $templateParams["erroreUpdateOrdini"]; ?></p>
				            <?php endif; ?>
				            <div class="form-group row">
				            	<label for="OrdineAssociato" class="col-12">Numero Ordine:</label>
				            	<select class="custom-select col-10 mx-auto" id="OrdineAssociato" name="OrdineAssociato">
									<option value="Nessuno" selected>Seleziona</option>
									<?php if(isset($templateParams["ordini"])): ?>		            
									<?php foreach($templateParams["ordini"] as $ordine): ?>
									<option value="<?php echo $ordine["IdOrdine"].";".$ordine["IdStato"]; ?>"><?php echo $ordine["IdOrdine"]; ?></option>
					                <?php endforeach; ?>
					                <?php endif; ?>
								</select>
							</div>
							<div class="form-group row">
								<label for="StatoOrdine" class="col-12">Stato Ordine:</label>
								<select class="custom-select col-10 mx-auto" id="StatoOrdine" name="StatoOrdine">
									<option value="0" selected>NO</option>
									<?php if(isset($templateParams["statoOrdini"])): ?>	
									<?php foreach($templateParams["statoOrdini"] as $stato): ?>
									<option value="<?php echo $stato["IdStato"]; ?>"><?php echo $stato["Nome_Stato"]; ?></option>
					                <?php endforeach; ?>
					                <?php endif; ?>
								</select>
							</div>
							<div class="form-group text-right mb-0">
			                    <input type="submit" name="aggiornaOrdine" value="Aggiorna ordine" class="btn btn-primary"/>
			                </div>
			            </form>
		            </div>
				</div>
			</div>
		</div>
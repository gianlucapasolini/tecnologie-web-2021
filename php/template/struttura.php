<!DOCTYPE html>
<html lang="it">
    <head>
		<title> <?php echo $templateParams["titolo"]; ?> </title>
		<link rel="icon" href="upload/logo.jpg">
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous"/>
		<link rel="stylesheet" href="css/style.css"/>
		<?php
	    if(isset($templateParams["css"])):
	        foreach($templateParams["css"] as $css):
	    ?>
	    	<link rel="stylesheet" href="<?php echo $css; ?>"/>
	    <?php
	        endforeach;
	    endif;
		?>
		 <script  src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<?php
    if(isset($templateParams["prejs"])):
        foreach($templateParams["prejs"] as $script):
    ?>
        <script src="<?php echo $script; ?>"></script>
    <?php
        endforeach;
    endif;
    ?>
		<script src=" https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" type="text/javascript"></script>
	</head>
    <body class="pt-3">
		<?php
			if(isset($_SESSION["RIPRISTINO"])){
				header("nuovaPassword.php");
			}
		?>
			<nav>
			
				<div class="nav bg-light" id="barraDesktop">
					<img src="upload/logo.png" alt="Logo del sito" class="d-inline-block align-top my-auto ml-2" style="width:100px; height:38px;"/>
					<h1 aria-hidden="false" class="navbar-brand ml-5 my-auto"> <?php echo $templateParams["titoloPagina"]; ?> </h1>
					<?php
					if(!isset($_SESSION["tipo"]) || (isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "CLIENTE")):
					?>
					<a class="nav-link active my-auto" href="index.php">Home</a>
					<a class="nav-link my-auto" href="aste.php">Aste</a>
					<a class="nav-link my-auto" href="gestione_carrello.php?tipologia=ordini">Ordini</a>					
					<a class="nav-link my-auto" href="desideri.php">Desideri</a>
					<?php
					else:
						if($_SESSION["tipo"] == "VENDITORE"):
					?>
					<?php if(!isset($_SESSION["Casa_Asta"]) || (isset($_SESSION["Casa_Asta"]) && $_SESSION["Casa_Asta"] == 0)): ?>
					<a class="nav-link active my-auto" href="mia_concessionaria.php">Home</a>
					<?php else: ?>
						<a class="nav-link active my-auto" href="aste.php?resoconto=...">Home</a>
					<?php endif;?>
					<?php if(!isset($_SESSION["Casa_Asta"]) || (isset($_SESSION["Casa_Asta"]) && $_SESSION["Casa_Asta"] == 0)): ?>
						<a class="nav-link my-auto" href="configuratore_venditore.php">Aggiungi</a>
					<?php else: ?>
						<a class="nav-link my-auto" href="aste.php">Aste</a>
					<?php endif;?>

					<?php endif;		
						endif;
					?>
				</div>

				<div id="barraSmartphone" class="navbar navbar-light bg-light">
					<?php
					if(isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "CLIENTE"):
					?>
					<a href="login.php" aria-label="Pagina profilo">
						<em aria-hidden="true" class="fas fa-user-alt"> </em>
					</a>

					<h1 class="navbar-brand mx-auto" aria-hidden="false"> <?php echo $templateParams["titoloPagina"]; ?> </h1>
						
					<span>
						<span class="badge rounded-pill bg-secondary" aria-hidden="true">
							<a aria-label="Pagina notifiche" href="gestione_notifiche.php" class='fas fa-bell text-light'> 
							<?php if(isset($templateParams["numNotifiche"])): ?>
							<?php echo $templateParams["numNotifiche"]; ?> <?php endif; ?></a>
						</span>
						<a href="gestione_carrello.php?tipologia=carrello" aria-label="Pagina carrello">
							<em aria-hidden="true" class="fa fa-shopping-cart"> </em>
						</a>
					</span>
					
					
					<?php
					else:
						if(isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "VENDITORE" || isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "CORRIERE"):
							
					?>
					<a href="login.php" aria-label="Pagina del profilo">
						<em aria-hidden="true" class="fas fa-user-alt"> </em>
					</a>
					
					<h1 aria-hidden="false" class="navbar-brand"> <?php echo $templateParams["titoloPagina"]; ?> </h1>
						<span>
							<span class="badge rounded-pill bg-secondary" aria-hidden="true">
								<a aria-label="Pagina notifiche" href="gestione_notifiche.php" class='fas fa-bell text-light'> 
								<?php if(isset($templateParams["numNotifiche"])): ?>
								<?php echo $templateParams["numNotifiche"]; ?> <?php endif; ?></a>
							</span>
						<a href="manage_account.php?LogOut=..." aria-label="Pagina di logout">
							<em aria-hidden="true" class="fas fa-sign-out-alt"> </em>
						</a>
					</span>
					<?php else: ?>

						<a href="login.php" aria-label="Pagina di login">
							<em aria-hidden="true" class="fas fa-sign-in-alt"> </em>
						</a>
					
						<h1 aria-hidden="false" class="navbar-brand"> <?php echo $templateParams["titoloPagina"]; ?> </h1>
		
						<a href="gestione_carrello.php?tipologia=carrello" aria-label="Pagina del carrello">
							<em aria-hidden="true" class="fa fa-shopping-cart"> </em>
						</a>

					<?php endif;
						endif;
					?>
				</div>
				<ul id="barraInferiore" class="nav nav-pills nav-fill bg-light lower-nav pb-1 pt-1">
				<?php
					if(!isset($_SESSION["tipo"]) || (isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "CLIENTE")):
					?>
						<li class="nav-item my-auto">
							<a href="desideri.php" id="iconDesideri" name="iconDesideri" aria-label="Vai alla lista dei desideri">
								<em aria-hidden="true" class="fas fa-grin-hearts" style="margin-bottom:0;"> </em>
								<p aria-hidden="true" style="font-size:16px; margin-top:0;"> Desideri </p>
							</a>
						</li>
						<li class="nav-item my-auto">
							<a href="gestione_carrello.php?tipologia=ordini" id="iconOrdini" name="iconOrdini" aria-label="Vai alla pagina degli ordini">
								<em aria-hidden="true" class="fas fa-list" style="margin-bottom:0;"> </em>
								<p aria-hidden="true" style="font-size:16px; margin-top:0;"> Ordini </p>
							</a>
						</li>
						<li class="nav-item my-auto">
							<a href="aste.php" id="iconAste" name="iconAste" aria-label="Vai alla pagina delle aste">
								<em aria-hidden="true" class="fa fa-gavel" style="margin-bottom:0;"> </em>
								<p aria-hidden="true" style="font-size:16px; margin-top:0;"> Aste </p>
							</a>
						</li>
						<li class="nav-item my-auto">
							<a href="index.php" id="iconHome" name="iconHome" aria-label="Vai alla home">
								<em aria-hidden="true" class="fa fa-home" style="margin-bottom:0;"> </em>
								<p aria-hidden="true" style="font-size:16px; margin-top:0;"> Home </p>
							</a>
						</li>
					<?php
					else:
						if($_SESSION["tipo"] == "VENDITORE"):
					?>
						<?php if(!isset($_SESSION["Casa_Asta"]) || (isset($_SESSION["Casa_Asta"]) && $_SESSION["Casa_Asta"] == 0)): ?>
						<li class="nav-item my-auto">
							<a href="configuratore_venditore.php" aria-label="Aggiungi nuova auto" id="iconAggiungi" name="iconAggiungi">
								<em class="fa fa-plus" style="margin-bottom:0;"> </em>
								<p aria-hidden="true" style="font-size:16px; margin-top:0;"> Aggiungi </p>
							</a>
						</li>
						<?php else: ?>
							<li class="nav-item my-auto">
							<a href="aste.php" aria-label="Vai alle aste" id="iconAste" name="iconAste">
								<em class="fa fa-gavel" style="margin-bottom:0;"> </em>
								<p aria-hidden="true" style="font-size:16px; margin-top:0;"> Aste </p>
							</a>
						</li>
						<?php endif; ?>
						<?php if(!isset($_SESSION["Casa_Asta"]) || (isset($_SESSION["Casa_Asta"]) && $_SESSION["Casa_Asta"] == 0)): ?>
						<li class="nav-item my-auto">
							<a href="mia_concessionaria.php" aria-label="Vai alla home" id="iconHome" name="iconHome">
								<em class="fa fa-home" style="margin-bottom:0;"> </em>
								<p aria-hidden="true" style="font-size:16px; margin-top:0;"> Home </p>
							</a>
						</li>
					<?php else: ?>
						<li class="nav-item my-auto">
							<a href="aste.php?resoconto=..." aria-label="Vai alla home" id="iconHome" name="iconHome">
								<em class="fa fa-home" style="margin-bottom:0;"> </em>
								<p aria-hidden="true" style="font-size:16px; margin-top:0;"> Home </p>
							</a>
						</li>
					<?php endif;?>
						
					<?php endif;
						endif;
					?>
					
				  </ul>
			  </nav>

			<div class="icon-bar" id="iconBar">
				<span class="" aria-hidden="true">
					<a aria-label="Pagina notifiche" href="gestione_notifiche.php" class='fas fa-bell'> 
					<?php if(isset($templateParams["numNotifiche"])): ?>
					<?php echo $templateParams["numNotifiche"]; ?> <?php endif; ?></a>
				</span>
				<?php if(isset($_SESSION["tipo"])):?>
				<a aria-label="Vai alla pagina del profilo" href="login.php"><em class="fas fa-user-alt"></em></a>
				<?php else: ?>
				<a aria-label="Vai alla pagina di login" href="login.php"><em class="fas fa-sign-in-alt"></em></a>
				<?php endif; ?>
				<?php if(isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "VENDITORE"): ?>
					<a aria-label="Vai alla pagina di logout" href="manage_account.php?LogOut=..."><em class="fas fa-sign-out-alt"></em></a>
				<?php else: ?>
					<a aria-label="Vai al carrello" href="gestione_carrello.php?tipologia=carrello"><em class="fa fa-shopping-cart"></em></a>
				<?php endif; ?>
			</div>
			
			<main class="mt-5 pb-2 mr-1 ml-1">
				<?php
				
					if(isset($templateParams["section"])){
						require($templateParams["section"]);
					}
				?>
	            <?php
	            if(isset($templateParams["nome"])){
					if(!isset($_SESSION["ASTE"])){
						require($templateParams["nome"]);
					} else {
						require("template/resoconto_aste.php");
					}
	                
	            }
	            ?>
			</main>
			
			<?php
	            if(isset($templateParams["modal"])){
	                require($templateParams["modal"]);
	            }
			?>

			

	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <?php
    if(isset($templateParams["js"])):
        foreach($templateParams["js"] as $script):
    ?>
        <script src="<?php echo $script; ?>"></script>
    <?php
        endforeach;
    endif;
    ?>
    </body>
</html>
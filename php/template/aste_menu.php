<div class="container-fluid text-center">
	<form method="GET" action="aste.php" class="mb-1 mx-auto formFiltra">
		<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#filtroAste" aria-expanded="false" aria-controls="filtroAste">
			Filtri
		</button>
		<a href="visualizzazione_aste_vinte.php" class="btn btn-primary"> Vai alle aste vinte </a>
		<div class="collapse" id="filtroAste">
			<input type="hidden" name="tipologia" value="ricercaAvanzata"/>
			<div class="row mb-2">
				<div class="col col-sm-6">
					<label for="modello"> Modello </label>
					<input id="modello" name="modello" class="form-control" type="search" placeholder="Nome" aria-label="Inserisci modello auto"/>
				</div>
				<div class="col col-sm-6">
					<label for="casaAsta"> Casa asta </label>
					<select name="casaAsta" aria-label="Seleziona la casa d'asta" id="casaAsta" class="form-control form-select w-100">
						<option value="">Nessuno</option>
						<?php foreach($templateParams["caseAsta"] as $casa): ?>
							<option><?php echo $casa["Nome_Utente"]; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="row mb-2">
				<div class="col col-sm-6 my-auto">
					<div class="form-check">
						<input type="checkbox" value="1" class="form-check-input" name="checkBattuta" id="checkBattuta"/>
						<label class="form-check-label" for="checkBattuta">Già battuta</label>
					</div>
				</div>
				<div class="col col-sm-6">
					<div class="form-check">
						<label for="data">Data immatricolazione</label>
						<input type="date" id="data" name="data"/>
					</div>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Cerca</button>
		</div>
	</form>


	<div id="caroselloImmaginiAste" class="carousel slide mx-auto" data-ride="carousel">
		<div class="carousel-inner">

			<?php $i = 0; ?>
			<?php foreach($templateParams["asteRandom"] as $asta):?>
			<?php if($i == 0): ?>
			<div class="carousel-item active">
				<img src="upload/<?php echo $asta["Link_Immagine1"] ?>" class="d-block w-100 caroselloAstaImmagine" alt="Slide <?php echo $i; ?>">
				<p>
				<?php echo $asta["Modello"]." - ".$asta["prezzoAttuale"]." €"; ?>
				</p>
			</div>
			<?php else: ?>
			<div class="carousel-item">
				<img src="upload/<?php echo $asta["Link_Immagine1"] ?>" class="d-block w-100 caroselloAstaImmagine" alt="Slide <?php echo $i; ?>">
				<p>
				<?php echo $asta["Modello"]." - ".$asta["prezzoAttuale"]." €"; ?>
				</p>
			</div>
			<?php endif; ?>
			<?php $i++; ?>
			<?php endforeach; ?>
			
			<a class="carousel-control-prev noOpacity" href="#caroselloImmaginiAste" role="button" data-slide="prev">
				<em class="fas fa-arrow-alt-circle-left iconCarousel" aria-hidden="true"></em>
				<span class="sr-only">Immagine precedente</span>
			</a>
			<a class="carousel-control-next noOpacity" href="#caroselloImmaginiAste" role="button" data-slide="next">
				<em class="fas fa-arrow-alt-circle-right iconCarousel" aria-hidden="true"></em>
				<span class="sr-only">Immagine successiva</span>
			</a>
		</div>
	</div>

	<div id="elencoCaseAsta" class="container-fluid">
		<h2> Case d'asta </h2>
		<div class="row">
			<?php 
			$i = 0;
			foreach($templateParams["caseAsta"] as $casa):
			?>

			<div class="col-sm-6 col-xs-12 mx-auto">
				<form method="GET" action="aste.php" class="text-center">
					<input type="hidden" aria-hidden="true" name="tipologia" value="ricerca">
					<input type="hidden" aria-hidden="true" name="casaAsta" value="<?php echo $casa["Nome_Utente"]; ?>">
					<input style="height:20vh;" type="image" id="<?php echo $casa["P_IVA"]; ?>" name="p_iva" alt="Link casa asta <?php echo $casa["Nome_Utente"]; ?>" src="upload/<?php echo $casa["Link_Logo"]; ?>"/>
					<label style="display:block;" for="<?php echo $casa["P_IVA"]; ?>"> <?php echo $casa["Nome_Utente"]; ?> </label>
				</form>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
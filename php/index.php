<?php
require_once 'gianluca_bootstrap.php';

if(!isUserLoggedIn() || $_SESSION["tipo"] == "CLIENTE") {
	$templateParams["titolo"] = "Car Shop - Home";
	$templateParams["titoloPagina"] = "Home";
	$templateParams["nome"] = "home.php";
	$templateParams["slider_auto"] = $dbh_gianluca->getRandomFotoAuto(4);
	$templateParams["venditori"] = $dbh_gianluca->getAllVenditori();
	//DA FINIRE VALE PER TUTTI!
	if(isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "CLIENTE"){
		$templateParams["numNotifiche"] = $dbh_gianluca->getNumeroNotifiche($_SESSION["CF"]);
	}
	$templateParams["js"][0] = "./js/gestione_home.js";
	$templateParams["css"][0] = "./css/gianluca_style.css";
} else {
	header("location: manage_account.php");
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>
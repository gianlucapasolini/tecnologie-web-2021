function convertPlaceholderIntext($input){
    if($input.attr("placeholder") != "N_Civico" && $input.attr("placeholder") != "CAP"){
        $input.val($input.attr("placeholder"));
    }    
}

$(document).ready(function () {
    const tipo = $("div > span[id='Tipologia_Utente']").text();
    switch (tipo) {
        case "CLIENTE":
            $("div[id='Gestione_Foto_Venditore']").parent().hide();
            $("h2[id='Titolo_Fatturazione']").show();
            $("h2[id='Titolo_IndirizzoNegozio']").hide();
            $("div[id='Gestione_Ordini_Corriere']").parent().hide();
            break;
        case "VENDITORE":
            $("div[id='Gestione_Foto_Venditore']").parent().show();
            $("h2[id='Titolo_Fatturazione']").hide();
            $("h2[id='Titolo_IndirizzoNegozio']").show();
            $("div[id='Gestione_Ordini_Corriere']").parent().hide();
            break;
        case "CORRIERE":
            $("div[id='Gestione_Foto_Venditore']").parent().hide();
            $("div[id='Sezione_Indirizzo']").parent().hide();
            $("div[id='Gestione_Ordini_Corriere']").parent().show();
            break;    
        default:
            alert("ERRORE!!!!");
            break;
    }

    convertPlaceholderIntext($("form input[id='via']"));
    convertPlaceholderIntext($("form input[id='N_Civico']"));
    convertPlaceholderIntext($("form input[id='Citta']"));
    convertPlaceholderIntext($("form input[id='Provincia']"));
    convertPlaceholderIntext($("form input[id='CAP']"));

    if ($("form input[id='via']").val() != "Via"){
        $("form input[type='submit'][value='Invia']").prop('value', 'Aggiorna');
    }

    //default
    $("#StatoOrdine option[value!=0]").hide();
    $("#OrdineAssociato").change(function (e) { 
        $("#StatoOrdine option:selected").prop("selected", false);
        $("#StatoOrdine option").hide();
        if ($("#OrdineAssociato").val() == "Nessuno"){
            $("#StatoOrdine option[value=0]").show();
            $("#StatoOrdine option[value=0]").prop("selected", true);
        } else {            
            StatusOrder = $("#OrdineAssociato").val().split(";")[1];
            $("#StatoOrdine option").filter(function (i, e) { return parseInt($(e).val()) >= StatusOrder }).show();
            $("#StatoOrdine option[value="+StatusOrder+"]").prop("selected", true);
        }
    });
});
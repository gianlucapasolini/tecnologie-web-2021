$(document).ready(function () {
    $("form div > div[id=passwordHelpBlockCheck]").hide();
    $("form div > div[id=passwordHelpBlock]").hide();
    
    const passw = $("input[name='password']");
    const letter = $("div[id='passwordHelpBlock'] > div > p[id='letter']");
    const capital = $("div[id='passwordHelpBlock'] > div > p[id='capital']");
    const number = $("div[id='passwordHelpBlock'] > div > p[id='number']");
    const speccarater = $("div[id='passwordHelpBlock'] > div > p[id='specChar']");
    const length = $("div[id='passwordHelpBlock'] > div > p[id='length']");

    $("#showPassword").click(function () {
        if ($("#password").attr("type") == "password"){
            $("#password").attr("type", "text");
            $("#showPassword > em").removeClass("fa-eye").addClass("fa-eye-slash")
        } else {
            $("#password").attr("type", "password");
            $("#showPassword > em").removeClass("fa-eye-slash").addClass("fa-eye")
        }
    });

    $("#showConfPassword").click(function () {
        if ($("#Conf_password").attr("type") == "password") {
            $("#Conf_password").attr("type", "text");
            $("#showConfPassword > em").removeClass("fa-eye").addClass("fa-eye-slash")
        } else {
            $("#Conf_password").attr("type", "password");
            $("#showConfPassword > em").removeClass("fa-eye-slash").addClass("fa-eye")
        }
    });

    $("input[name='password']").focus(function () { 
        $("form div > div[id=passwordHelpBlock]").show();
    });

    $("input[name='password']").blur(function () { 
        $("form div > div[id=passwordHelpBlock]").hide();        
    });

    $("input[name='password']").keyup(function () { 
        // Validate lowercase letters
        const lowerCaseLetters = /[a-z]/g;
        if (passw.val().match(lowerCaseLetters)) {
            letter.removeClass("invalid");
            letter.addClass("valid");
        } else {
            letter.removeClass("valid");
            letter.addClass("invalid");
        }

        // Validate capital letters
        const upperCaseLetters = /[A-Z]/g;
        if (passw.val().match(upperCaseLetters)) {
            capital.removeClass("invalid");
            capital.addClass("valid");
        } else {
            capital.removeClass("valid");
            capital.addClass("invalid");
        }

        // Validate numbers
        const numbers = /[0-9]/g;
        if (passw.val().match(numbers)) {
            number.removeClass("invalid");
            number.addClass("valid");
        } else {
            number.removeClass("valid");
            number.addClass("invalid");
        }

        // Validate specialCaracter
        const speccaraters = /[!@#$%^&*]/g;
        if (passw.val().match(speccaraters)) {
            speccarater.removeClass("invalid");
            speccarater.addClass("valid");
        } else {
            speccarater.removeClass("valid");
            speccarater.addClass("invalid");
        }

        // Validate length
        if (passw.val().length >= 8 && passw.val().length <=30) {
            length.removeClass("invalid");
            length.addClass("valid");
        } else {
            length.removeClass("valid");
            length.addClass("invalid");
        }
    });

    $("form").submit(function (e) { 
        //CHECK password!
        const passw = $(this).find("input[name=password]");
        const confpassw = $(this).find("input[name=Conf_password]");
        const regex = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*?[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,30}$/;
        if (regex.test(passw.val())){
            if (isNaN($("div[id='passwordHelpBlock'] > p[class='invalid']"))) {
                if (passw.val() === confpassw.val()) {
                    passw.val(sha3_512(passw.val()));
                    confpassw.val(sha3_512(confpassw.val()));
                    $("form div > div[id=passwordHelpBlockCheck]").hide();
                } else {
                    $("form div > div[id=passwordHelpBlockCheck]").show().focus();
                    e.preventDefault();
                }
            } else {
                $("form div > div[id=passwordHelpBlockCheck]").hide();
                e.preventDefault();
            }
        } else {
            $("form div > div[id=passwordHelpBlockCheck]").hide();
            e.preventDefault();
        }
    });
});
const configurazione = { modello: {}, motore: {}, esterni: {}, interni: {}, optional: {} };
const fields = { modello: "", motore: "", esterni: [], interni: [] };

function draw() {
    let total = parseInt(configurazione.modello.prezzo);
    table = `<tr>
                <th id = "selezione" > Selezione</ th>
                <th id="prezzo">Prezzo</th>
            </tr>`;
    for (const [key, value] of Object.entries(configurazione)) {
        if (key == "modello") {
            table += `<tr>
                        <th id='base' headers='selezione'>${value.desc}</th> 
                        <th id="prezzo_base" headers="prezzo base"><em>${value.prezzo}</em>€</th>
                    </tr >`;
        }
        else {
            let sum = 0;
            if (value.prezzo == undefined) {
                for (const [titolo, val] of Object.entries(value)) {
                    sum += parseInt(val.prezzo);

                }
            }
            else {
                sum = parseInt(value.prezzo);
            }
            total += sum;
            table += `<tr>
                        <th id='${key}' headers='selezione'>${key}</th> 
                        <th id="prezzo_${key}" headers="prezzo ${key}"><em>${sum}</em>€</th>
                    </tr>`;
            if (value.prezzo == undefined) {
                for (const [titolo, val] of Object.entries(value)) {
                    if (key == "interni" || key == "esterni") {
                        table += `<tr >
                            <th id='${val.id}' headers='selezione ${key}'>${titolo} ${val.desc}</th> 
                            <td id="prezzo_${val.id}" headers="prezzo ${val.id} prezzo_${key}"><em>${val.prezzo}</em>€</td>
                        </tr> `;
                    }
                    else {
                        table += `<tr >
                            <th id='${val.id}' headers='selezione ${key}'>${val.desc}</th> 
                            <td id="prezzo_${val.id}" headers="prezzo ${val.id} prezzo_${key}"><em>${val.prezzo}</em>€</td>
                        </tr> `;
                    }
                };
            }
            else {
                table += `< tr >
                        <th id='${value.id}' headers='selezione ${key}'>${value.desc}</th> 
                        <td id="prezzo_${value.id}" headers="prezzo ${value.id} prezzo_${key}"><em>${value.prezzo}</em>€</td>
                    </ tr> `;
            }
        }
    }
    table += `<tr>
    <th id="totale" headers="selezione">Totale
    </th>
    <th id="prezzoTotale" headers="prezzo totale">
        <em>${total}</em>€
    </th>
</tr>`;
    $("aside > table").html(table);
}

function updateRadio(element) {
    const id = element;
    const title = $(`#${element} `).parents("div")[0].id.replace("-tipo", "");
    const tipo = $(`#${element} `).parents("div[role=tabpanel]")[0].id.replace("pills-", "");
    const desc = $(`#${element} + label span:first-of-type`).text();
    const prezzo = $(`#${element} + label span:last-of-type`).children("em")[0].innerHTML;
    if (tipo === "motore") {
        configurazione[tipo] = { id: id, desc: desc, prezzo: prezzo };
    } else {
        configurazione[tipo][title] = { id: id, desc: desc, prezzo: prezzo };
    }
    draw();
}

function updateCheckbox(element) {
    const id = element;
    const tipo = $(`#${element} `).parents("div[role=tabpanel]")[0].id.replace("pills-", "");
    if ($(`#${element} `).is(':checked')) {
        const desc = $(`#${element} + label span:first-of-type`).text();
        const prezzo = $(`#${element} + label span:last-of-type`).children("em")[0].innerHTML;
        configurazione[tipo][id] = { id: id, desc: desc, prezzo: prezzo };
    } else {
        delete configurazione[tipo][id];
    }
    draw();
}

$(document).ready(function () {
    const modello = $("#base").text();
    const prezzoBase = $("#prezzo_base>em").text();
    configurazione.modello = { desc: modello, prezzo: prezzoBase };
    const elements = $("div[id$='-tipo']");
    elements.each(function (i, el) {
        const title = el.id.replace("-tipo", "");
        const tipo = $(this).parents("div[role=tabpanel]")[0].id.replace("pills-", "");
        if (tipo != "motore" && tipo != "optional") {
            fields[tipo].push(title);
        }
    });
    $("form input:radio:checked").each(function (index, el) {
        updateRadio(el.id);
    });
    $("form input:checkbox:checked").each(function (index, el) {
        updateCheckbox(el.id);
    });
    $("form input:radio").change(function (e) {
        updateRadio(e.currentTarget.id);
        $(".error").remove();

    });
    $("form input:checkbox").change(function (e) {
        updateCheckbox(e.currentTarget.id);
        $(".error").remove();
    });

    $("form input[type=submit]").click(function (e) {
        e.preventDefault();
        let configOk = true;
        let error = "";
        for (const [key, value] of Object.entries(fields)) {
            for (const [titolo, val] of Object.entries(value)) {
                if (!configurazione[key].hasOwnProperty(val)) {
                    configOk = false;
                    error += `<li>non selezionato ${val} in ${key}</li>`;
                }
            }
        }
        if (configOk) {
            $("form").submit();
        }
        else {
            if ($(".error").length == 1) {
                $(".error").html(error);
            } else {
                $(this).after(`<ul>${error}</ul>`);
                $(this).next().addClass("error");
            }

        }
    });

});
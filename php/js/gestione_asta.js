var el;
var ctx;
var success = 0;
function creaGrafico(){
    let info = [];
    
    for(let i = 0; i < 10; i++){
        info[i] = i;
    }
    $.ajax({
        url: "utils/aste_rilanci.php", 
        dataType: "json",
        data: {"id": $( "#id" ).val()},  
        success:function(val)
                {
                    var myLineChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                          labels: info,
                          datasets: [{
                              label: '# di rilanci',
                              data: val,
                              backgroundColor:"rgba(66, 135, 245, 0.5)"
                          }]},
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: false
                                    }
                                }]
                            }
                        }
                    });
                },
        error: function(){
            console.log("errore");
        }
    });
}

function getPrezzoAttuale(){
    $.ajax({
        url: "utils/aste_rilanci.php", 
        dataType: "json",
        data: {"id": $( "#id" ).val(),
                "prezzo": 1},  
        success:function(val)
                {
                    $("#pPrezzoAttuale").text(val + " €");
                    $("#prezzoAttualeModal").text("Offerta attuale: " + val + " €");
                    return val;
                },
        error: function(){
            return 0;
        }
    });
}

$(document).ready(function(){
    ctx = document.getElementById('graficoAndamento').getContext('2d');
    creaGrafico();
    $( "#btnEsegui" ).click(function() {
        let importo;
        let importoTxt = $( "#txtImporto" ).val();
        var numbers = /^[0-9]+$/;
        if(importoTxt.match(numbers))
        {
            importo = parseInt(importoTxt);
            let cf = $( "#cf" ).val();
            if(cf == ""){
                $("#risultato").text("Errore nel rilancio! Non sei loggato");
                return;
            }
            let id = $( "#id" ).val();
            $.ajax({
                url: "utils/aste_rilanci.php", 
                dataType: "json",
                data: {"id": id,
                        "CF": cf,
                        "rilancio": importo},  
                success:function(val)
                        {
                            if(val == "ok"){
                                $("#risultato").text("Rilancio avvenuto con successo");
                                creaGrafico();
                                getPrezzoAttuale();
                                success = 1;
                            } else {
                                $("#risultato").text("Errore nel rilancio");
                            }
                        },
                error: function(){
                    $("#risultato").text("Errore nel rilancio");
                }
            });
        }
        else
        {
            $("#risultato").text('Inserire solo numeri');
        }
        
        
    });

    $(" #ChidiAsta ").click(function(){
        if(success == 1){
            location.reload();
        }
    });

    $(" #closeModal ").click(function(){
        if(success == 1){
            location.reload();
        }
    });

    $(" #btnRefresh ").click(function(){
        creaGrafico();
        getPrezzoAttuale();
    });
});
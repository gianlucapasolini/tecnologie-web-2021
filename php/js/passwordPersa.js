$(document).ready(function () {
    //default
    $("#Field_PIVA").hide().find("input").prop('required', false);
    
    $("input[name='options_utente']").change(function(){
    	const tipo = $("input[type='radio'][name='options_utente']:checked").val();
        switch(tipo){
            case "Cliente":
                $("#Field_CF").show().find("input").prop('required', true);
                $("#Field_PIVA").hide().find("input").prop('required', false);
                break;
            case "Venditore":
                $("#Field_PIVA").show().find("input").prop('required', true);
                $("#Field_CF").hide().find("input").prop('required', false);
                break;
            case "Corriere":
                $("#Field_PIVA").show().find("input").prop('required', true);
                $("#Field_CF").hide().find("input").prop('required', false);
                break;
            default:
                alert("Errore!");
                break;
        }
    });
});
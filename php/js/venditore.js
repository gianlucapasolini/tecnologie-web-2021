$(document).ready(function () {
    const a = $("header a");
    const mybutton = $("#top");
    const header = $("#header");

    const yShow = header.height() / 2;
    const cf = $("#CF").val();
    let login;
    if (cf != "") {
        login = true;
    } else {
        login = false;
    }

    $("#nav-modello button").click(function (e) {
        e.preventDefault();
        if (login) {
            $.post("utils/ajaxPreferiti.php",
                {
                    cf: cf,
                    modello: e.currentTarget.id
                },
                function (data, status) {
                    const icon = $(`#${e.currentTarget.id}>em`).toggleClass("red");
                    if (icon.text() == "favorite") {
                        icon.text("favorite_border");
                    } else {
                        icon.text("favorite");
                    }

                });
        }
    });

    $("#nav-recensione button").click(function (e) {
        e.preventDefault();
        const divId = e.currentTarget.id.replace("button", "div");
        const button = $(`#${divId} button`);
        if (button.hasClass("less")) {
            $(`#${divId} span`).show();
            $(`#${divId} span+span`).hide();
            button.text("Read more")
        } else {
            $(`#${divId} span`).hide();
            $(`#${divId} span+span`).show();
            button.text("Read less")
        }
        button.toggleClass("less");

    });

    window.onscroll = function () {
        if (document.body.scrollTop >= yShow || document.documentElement.scrollTop >= yShow) {
            mybutton.show();
        } else {
            mybutton.hide();
        }

        if (document.body.scrollTop >= yShow || document.documentElement.scrollTop >= yShow) {
            a.hide();
        } else {
            a.show();
        }

    }
});

function initMap() {
    const indirizzo = $("#indirizzo").text().replaceAll(" ", "%20");
    $.get(`https://us1.locationiq.com/v1/search.php?key=pk.5318f7f38d279e35746fd285b9a853cd&q=${indirizzo}&accept-language=it&countrycodes=it&format=json`, function (data, status) {
        const center = { lat: parseFloat(data[0].lat), lng: parseFloat(data[0].lon) };
        const map = new google.maps.Map(document.getElementById("googleMap"), {
            zoom: 20,
            center: center,
        });
        const marker = new google.maps.Marker({
            position: center,
            map: map,
        });
    });
}
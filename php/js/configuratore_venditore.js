$(document).ready(function () {
    const esNuovo = $("#es-nuovo");
    const inNuovo = $("#in-nuovo");

    $("#es-nuovoBotton").click(function (event) {
        event.preventDefault();
        const text = esNuovo.val().trim().toLowerCase();
        if (text != "" && $(`#e-${text}`).length == 0) {
            $("#esterni fieldset ul").append(`<li>
                <input type="radio" id="e-${text}" name="tipologia" value="${text}"required />
                <label for="e-${text}">${text}</label>
            </li>`);
        }
        else {
            esNuovo.after(`<li>${text} non ammisibile</li>`);
            esNuovo.next().addClass("error");
        }
    });

    esNuovo.on('input', function () {
        if ($(this).next().hasClass("error")) {
            $(this).next().remove();
        }
    });

    $("#in-nuovoBotton").click(function (event) {
        event.preventDefault();
        const text = inNuovo.val().trim().toLowerCase();
        if (text != "" && $(`#i-${text}`).length == 0) {
            $("#interni fieldset ul").append(`<li>
                <input type="radio" id="i-${text}" name="tipologia" value="${text}"required />
                <label for="i-${text}">${text}</label>
            </li>`);
        }
        else {
            inNuovo.after(`<li>${text} non ammisibile</li>`);
            inNuovo.next().addClass("error");
        }
    });

    inNuovo.on('input', function () {
        if ($(this).next().hasClass("error")) {
            $(this).next().remove();
        }
    });
});
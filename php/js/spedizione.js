function ripristinoNuovo() {
    changeValori("Via; N_Civico; Città; Provincia; CAP");
}

function changeValori(string) {
    const valori = string.split(";");
    $("#via").val(valori[0]);
    if(Number.isInteger(parseInt(valori[1]))){
        $("#N_Civico").val(parseInt(valori[1]));
    } else {
        $("#N_Civico").val("");
    }
    $("#Citta").val(valori[2]);
    $("#Provincia").val(valori[3]);
    if(Number.isInteger(parseInt(valori[4]))){
        $("#CAP").val(parseInt(valori[4]));
    } else {
        $("#CAP").val("");
    }
}

$(document).ready(function () {
    $("#listSpedizioni").change(function (e) { 
        //cambia completamento placeholder e testo
        if ($("#listSpedizioni").val() == "Nuovo"){
            ripristinoNuovo();
            $("input[name='submit']").val("Aggiungi");
        } else {
            changeValori($("#listSpedizioni Option:selected").html());
            $("input[name='submit']").val("Aggiorna");
        }
    });
});
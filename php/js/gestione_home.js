$(document).ready(function () {
    $("div[id='carouselGeneralCar'] > div[class='carousel-inner'] > div[class='carousel-item']:first").addClass("active");

    $("#Elenco_macchina_ricerca").hide();

    $('#tableSearch').on('input', function (e) {
        const search = $("#tableSearch").val();
        if (search.length >= 1){
            load_data(search);
            $("#Elenco_macchina_ricerca").show();
        }
        else{
            $("#Elenco_macchina_ricerca").hide();
        }
    });

    function load_data(search) {
        $.ajax({
            method: "GET",
            url: "utils/load_model_ajax.php",
            data: {search: search},
            dataType: "json",
            success: function (data) {
                //Risultato dei dati!!
                let html = '';
                for(let count = 0; count < data.length; count++){
                    html += '<option value="' + data[count].IdAuto + '">' + data[count].Modello + '; prezzo : ' + data[count].Prezzo_base +'€</option>';
                }
                if(data.length == 0){
                    html += '<option value="NO">No Corrispondenze</option>';
                    $("#idAuto").html(html);
                } else {
                    $("#idAuto").html(html);
                }                
            },
            error: function (xhr, status, error) {
                const html = '<option value="NO">No Corrispondenze</option>';
                $("#idAuto").html(html);
            }
        });
    }
});
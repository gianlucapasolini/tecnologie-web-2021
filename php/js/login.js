$(document).ready(function () {
    $("form[name='Login']").submit(function (e) { 
        //cripta password!!! prima di spedirla!
        const passw = $(this).find("input[name=password]");
        const regex = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*?[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,30}$/;
        if (regex.test(passw.val())) {
            passw.val(sha3_512(passw.val()));
        } else {
            e.preventDefault();
        }
    });

    $("#showPassword").click(function () {
        if ($("#password").attr("type") == "password"){
            $("#password").attr("type", "text");
            $("#showPassword > em").removeClass("fa-eye").addClass("fa-eye-slash")
        } else {
            $("#password").attr("type", "password");
            $("#showPassword > em").removeClass("fa-eye-slash").addClass("fa-eye")
        }
    });
});
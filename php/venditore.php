<?php
require_once 'brighi_bootstrap.php';

if(!isset($_GET["venditore"]) || isUserLoggedIn() && $_SESSION["tipo"] != "CLIENTE" ){
		header("location: index.php");
} else {
    $templateParams["titolo"] = "Car Shop - ".$_GET["venditore"];
    $templateParams["titoloPagina"] = $_GET["venditore"];
    $templateParams["nome"] = "template/venditore.php";
    $templateParams["venditore"]=$dbh_brighi->getVenditore($_GET["venditore"]);
    if(isUserLoggedIn() && $_SESSION["CF"] != ""){
        $templateParams["modelli"]=$dbh_brighi->getPreferiti($_SESSION["CF"], $_GET["venditore"]);
        $templateParams["numNotifiche"] = $dbh_brighi->getNumeroNotifiche($_SESSION["CF"]);
    }
    else
    {
        $templateParams["modelli"]=$dbh_brighi->getPreferiti("", $_GET["venditore"]);
    }
    $templateParams["recensioni"]=$dbh_brighi->getRecensioni($_GET["venditore"]);
    $templateParams["media"]=$dbh_brighi->getRecensioniMedia($_GET["venditore"]);
    $templateParams["prejs"][0] = "./js/venditore.js";
    $templateParams["js"][0] = "https://kit.fontawesome.com/a076d05399.js";
    $templateParams["css"][0] = "./css/venditore.css";
    $templateParams["css"][1] ="https://fonts.googleapis.com/icon?family=Material+Icons";
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>
# Car Shop #

Elaborato per il corso di Tecnologie Web. Abbiamo creato un sito di acquisto di automobili nuove (configurabili mediante l'apposito configuratore) e un sistema d'aste, gestendo rilanci, ordini e notifiche.

---
### Configurazione ###

1.	https://bitbucket.org/andreaBrighi/tecnologie-web/src/master/db/, da questo repo è possibile, tramite il file di creazione_db.sql, creare il database. Tramite il file creazione_db_safeuser.sql è possibile creare l'utente dedicato per le query. Tramite il file inserimento_db_definitivo.sql è possibile popolare il database.
2.	Scaricare il repository sulla cartella htdocs  di xampp.
3.	Tramite il repo https://bitbucket.org/andreaBrighi/tecnologie-web/src/master/php/ cambiare dentro i file *.bootstrap.php il numero della porta usato da Xampp nella Vostra configurazione.
4.	Settare e configurare Xampp per abilitare le e-mail seguendo il successivo tutorial https://www.phpflow.com/php/how-to-send-email-from-localhost-using-php/. Non inviamo la nostra mail e password per riservatezza.
5.	Spendere più soldi possibili configurando le auto dei sogni "pagando" virtualmente.

### Funzionalità principali ###
*	Configuratore auto
*	Accesso sicuro
*	Carrello gestito anche tramite cookie se non registrato
*	Aste auto antiche
*	Notifiche in-app e via mail
*	3 tipi di utenti: cliente, venditore e corriere
*	Lista dei desideri

---
## Credits:

* Brighi Andrea
* Di Lillo Daniele
* Pasolini Gianluca